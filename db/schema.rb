# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151022143730) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appstore_checks", force: :cascade do |t|
    t.string   "name",                      null: false
    t.text     "detail"
    t.text     "expected"
    t.integer  "sort",          default: 1, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "internal_name",             null: false
    t.string   "klass",                     null: false
  end

  add_index "appstore_checks", ["internal_name"], name: "index_appstore_checks_on_internal_name", unique: true, using: :btree

  create_table "infrastructure_logs", force: :cascade do |t|
    t.integer  "infrastructure_id",                 null: false
    t.boolean  "available",         default: false, null: false
    t.datetime "datetime",                          null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "infrastructures", force: :cascade do |t|
    t.string   "name",           null: false
    t.text     "url",            null: false
    t.text     "success_result", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.boolean  "primitive",      null: false
  end

  create_table "jenkins_jobs", force: :cascade do |t|
    t.integer  "testrun_id",       null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "current_job_name"
    t.datetime "start"
    t.integer  "scenario_id"
    t.datetime "stop"
    t.integer  "status_id",        null: false
    t.integer  "node_id"
  end

  add_index "jenkins_jobs", ["testrun_id", "scenario_id"], name: "index_jenkins_jobs_on_testrun_id_and_scenario_id", unique: true, using: :btree

  create_table "job_statuses", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "job_statuses", ["internal_name"], name: "index_job_statuses_on_internal_name", unique: true, using: :btree

  create_table "nodes", force: :cascade do |t|
    t.string   "name",                       null: false
    t.string   "host",                       null: false
    t.string   "login",                      null: false
    t.string   "password",                   null: false
    t.text     "upload_dir",                 null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "home_dir",                   null: false
    t.boolean  "disabled",   default: false, null: false
  end

  create_table "scenarios", force: :cascade do |t|
    t.integer  "testrun_id",       null: false
    t.string   "allure_id",        null: false
    t.string   "name",             null: false
    t.boolean  "failed_by_appium", null: false
    t.datetime "start",            null: false
    t.datetime "stop",             null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "job_id",           null: false
  end

  create_table "step_files", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "version"
    t.string   "local_path"
    t.string   "original_name"
  end

  create_table "testcase_statuses", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "testcase_statuses", ["internal_name"], name: "index_testcase_statuses_on_internal_name", unique: true, using: :btree

  create_table "testcases", force: :cascade do |t|
    t.string   "allure_id",   null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "scenario_id", null: false
    t.integer  "status_id",   null: false
    t.jsonb    "data"
  end

  create_table "testrun_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "internal_name", null: false
  end

  add_index "testrun_types", ["internal_name"], name: "index_testrun_types_on_internal_name", unique: true, using: :btree
  add_index "testrun_types", ["name"], name: "index_testrun_types_on_name", unique: true, using: :btree

  create_table "testruns", force: :cascade do |t|
    t.string   "name",                                    null: false
    t.text     "description"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "type_id"
    t.integer  "sort",                    default: 1
    t.boolean  "disabled",                default: false, null: false
    t.datetime "start"
    t.datetime "stop"
    t.integer  "failed_scenarios_count",  default: 0,     null: false
    t.integer  "passed_scenarios_count",  default: 0,     null: false
    t.integer  "pending_scenarios_count", default: 0,     null: false
  end

end
