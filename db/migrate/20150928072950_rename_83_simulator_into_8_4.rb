# -*- encoding : utf-8 -*-
class Rename83SimulatorInto84 < ActiveRecord::Migration
  def up
    type_8_x = TestrunType.simulator_ios_8_x
    type_8_x.name = 'iOS 8.4 (Simulator)'
    type_8_x.save!
  end
end
