# -*- encoding : utf-8 -*-
class AddStep7IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check XCode platform version',
        detail: 'defaults read %{app}/Info.plist DTPlatformVersion',
        expected: "8.1\n",
        sort: 7,
        internal_name: 'step_7',
        klass: 'Steps::Step_7'
    )
  end
end
