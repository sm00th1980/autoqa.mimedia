# -*- encoding : utf-8 -*-
class AddTestrunTypeWith9xIos < ActiveRecord::Migration
  def up
    TestrunType.create!(name: 'iOS 9.0 (Simulator)', internal_name: 'simulator_ios_9_x')
    TestrunType.create!(name: 'iOS 9.x (Device)', internal_name: 'device_ios_9_x')
  end
end
