# -*- encoding : utf-8 -*-
class CreateTestrunTypes < ActiveRecord::Migration
  def change
    create_table :testrun_types do |t|
      t.string "name", null: false

      t.timestamps null: false
    end
  end
end
