# -*- encoding : utf-8 -*-
class AddInfrastructureLogin < ActiveRecord::Migration
  def up
    Infrastructure.create!(name: 'api.dev.mimedia.com/login', url: 'api.dev.mimedia.com/login', success_result: '', primitive: false)
  end

  def down
    Infrastructure.find_by(name: 'api.dev.mimedia.com/login').delete
  end
end
