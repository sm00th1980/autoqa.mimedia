# -*- encoding : utf-8 -*-
class AddScenarioIdIntoJenkinsJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :scenario_id, :integer
  end
end
