# -*- encoding : utf-8 -*-
class AddUniqIndexToTestrunTypes < ActiveRecord::Migration
  def change
    add_index :testrun_types, :name, unique: true
  end
end
