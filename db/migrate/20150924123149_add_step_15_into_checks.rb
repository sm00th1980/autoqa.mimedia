# -*- encoding : utf-8 -*-
class AddStep15IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPad launch images',
        detail: '-',
        expected: "LaunchImage-Portrait~ipad.png (MD5 = dcf60180b258defb566de5042ce79bbd)\nLaunchImage-Landscape~ipad.png (MD5 = 36047b1203297e4c7288092ff3e9aa62)\nLaunchImage-Portrait@2x~ipad.png (MD5 = 2217217bac18a20935025fe19c27be93)\nLaunchImage-Landscape@2x~ipad.png (MD5 = bf91364ae71b4dab8a0c79e64903b220)",
        sort: 15,
        internal_name: 'step_15',
        klass: 'Steps::Step_15'
    )
  end
end
