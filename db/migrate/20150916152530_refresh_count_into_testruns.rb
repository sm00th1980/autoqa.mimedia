# -*- encoding : utf-8 -*-
class RefreshCountIntoTestruns < ActiveRecord::Migration
  def up
    Testrun.mobile.find_each do |testrun|
      testrun.failed_scenarios_count = testrun.scenarios.select { |scenario| scenario.failed? }.count
      testrun.passed_scenarios_count = testrun.scenarios.select { |scenario| scenario.passed? }.count
      testrun.pending_scenarios_count = testrun.scenarios.select { |scenario| scenario.pending? }.count
      testrun.save!
    end
  end
end
