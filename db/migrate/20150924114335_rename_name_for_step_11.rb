# -*- encoding : utf-8 -*-
class RenameNameForStep11 < ActiveRecord::Migration
  def up
    step_11 = AppstoreCheck.step_11
    step_11.name = 'Check iPhone/iPod Icons'
    step_11.save!
  end
end
