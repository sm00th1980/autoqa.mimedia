# -*- encoding : utf-8 -*-
class CleanOldScenarios < ActiveRecord::Migration
  def up
    Scenario.where('stop < ?', (Time.now - 1.week)).find_each do |scenario|
      scenario.testcases.destroy_all
      scenario.delete
    end
  end
end
