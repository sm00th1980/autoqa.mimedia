# -*- encoding : utf-8 -*-
class FillTestcaseStatuses < ActiveRecord::Migration
  def up
    TestcaseStatus.create!(name: 'Passed', internal_name: 'passed')
    TestcaseStatus.create!(name: 'Pending', internal_name: 'pending')
    TestcaseStatus.create!(name: 'Broken', internal_name: 'broken')
    TestcaseStatus.create!(name: 'Unknown', internal_name: 'unknown')
    TestcaseStatus.create!(name: 'Broken by infrastructure', internal_name: 'broken_by_infrastructure')

    add_index :testcase_statuses, :internal_name, unique: true
  end

  def down
    TestcaseStatus.delete_all
    remove_index :testcase_statuses, :internal_name
  end
end
