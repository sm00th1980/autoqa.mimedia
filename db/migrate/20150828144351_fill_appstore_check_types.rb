# -*- encoding : utf-8 -*-
class FillAppstoreCheckTypes < ActiveRecord::Migration
  def up
    AppstoreCheckType.create(name: 'Remote', internal_name: 'remote')
    AppstoreCheckType.create(name: 'Local', internal_name: 'local')
  end

  def down
    AppstoreCheckType.delete_all
  end
end
