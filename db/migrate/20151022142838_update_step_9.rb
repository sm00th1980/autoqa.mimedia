# -*- encoding : utf-8 -*-
class UpdateStep9 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_9
    step.expected = "0700\n"
    step.save!
  end
end
