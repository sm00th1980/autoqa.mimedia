# -*- encoding : utf-8 -*-
class UpdateHashForStep13 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_13
    step.expected = "AppIcon29x29.png (MD5 = 7d8929a5c8a67517cda1a6ac904e6e39, SIZE = 29x29)\nAppIcon29x29@2x.png (MD5 = fe1627baf4758dc8b012a9412587de44, SIZE = 58x58)\nAppIcon29x29~ipad.png (MD5 = 7ee252ac9b82d606767fffb927bb2d9e, SIZE = 29x29)\nAppIcon29x29@2x~ipad.png (MD5 = 7ef4e4032b2ddfdc3787a243725ac734, SIZE = 58x58)"
    step.save!
  end

  def down
    step = AppstoreCheck.step_13
    step.expected = "AppIcon29x29.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29, SIZE = 29x29)\nAppIcon29x29@2x.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9, SIZE = 58x58)\nAppIcon29x29~ipad.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29, SIZE = 29x29)\nAppIcon29x29@2x~ipad.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9, SIZE = 58x58)"
    step.save!
  end
end
