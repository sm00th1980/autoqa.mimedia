# -*- encoding : utf-8 -*-
class AddKlassIntoAppstoreChecks < ActiveRecord::Migration
  def change
    add_column :appstore_checks, :klass, :string
  end
end
