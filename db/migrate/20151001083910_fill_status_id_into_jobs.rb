# -*- encoding : utf-8 -*-
class FillStatusIdIntoJobs < ActiveRecord::Migration
  def up
    JenkinsJob.find_each do |job|
      job.status_id = job.start.present? ? JobStatus.running.id : JobStatus.scheduled.id
      job.save!
    end

    change_column :jenkins_jobs, :status_id, :integer, null: false
  end
end
