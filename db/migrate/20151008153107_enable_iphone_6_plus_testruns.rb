# -*- encoding : utf-8 -*-
class EnableIphone6PlusTestruns < ActiveRecord::Migration
  def up
    testrun = Testrun.find_by(name: "iPhone 6+", type: TestrunType.simulator_ios_8_x)
    testrun.disabled = false
    testrun.save!

    testrun = Testrun.find_by(name: "iPhone 6+", type: TestrunType.simulator_ios_9_x)
    testrun.disabled = false
    testrun.save!
  end

  def down
    testrun = Testrun.find_by(name: "iPhone 6+", type: TestrunType.simulator_ios_8_x)
    testrun.disabled = true
    testrun.save!

    testrun = Testrun.find_by(name: "iPhone 6+", type: TestrunType.simulator_ios_9_x)
    testrun.disabled = true
    testrun.save!
  end
end
