# -*- encoding : utf-8 -*-
class RenameTempJobNameInCurrentJobNameIntoTestruns < ActiveRecord::Migration
  def change
    rename_column :testruns, :temp_jenkins_name, :current_job_name
  end
end
