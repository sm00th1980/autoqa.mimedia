# -*- encoding : utf-8 -*-
class AddLocalPathIntoStepFiles < ActiveRecord::Migration
  def change
    add_column :step_files, :local_path, :string
  end
end
