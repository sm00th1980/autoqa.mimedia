# -*- encoding : utf-8 -*-
class AddOsVersionIntoDevices < ActiveRecord::Migration
  def change
    add_column :devices, :os_version_id, :integer
    add_index :devices, :os_version_id
  end
end
