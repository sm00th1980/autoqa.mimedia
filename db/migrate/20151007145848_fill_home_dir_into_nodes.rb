# -*- encoding : utf-8 -*-
class FillHomeDirIntoNodes < ActiveRecord::Migration
  def up
    node1 = Node.find_by(name: 'node1')
    node1.home_dir = '/Users/mimedia-atest'
    node1.save!

    node2 = Node.find_by(name: 'node2')
    node2.home_dir = '/Users/user'
    node2.save!

    change_column :nodes, :home_dir, :string, null: false
  end
end
