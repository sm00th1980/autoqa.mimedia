# -*- encoding : utf-8 -*-
class CreateAppstoreCheckTypes < ActiveRecord::Migration
  def change
    create_table :appstore_check_types do |t|
      t.string "name", null: false
      t.text "internal_name", null: false

      t.timestamps null: false
    end

    add_index :appstore_check_types, :internal_name, unique: true
  end
end
