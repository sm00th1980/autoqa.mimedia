# -*- encoding : utf-8 -*-
class DropScenarioIdsFromJenkinsJobs < ActiveRecord::Migration
  def up
    remove_column :jenkins_jobs, :scenario_ids
  end
end
