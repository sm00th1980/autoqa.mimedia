# -*- encoding : utf-8 -*-
class AddStep2IntoAppstoreChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check code sign entitlements',
        detail: 'codesign -dvvv --entitlements - %{app}',
        expected: 'Executable=%{executable}
Identifier=com.mimedia.iOSv2
Format=bundle with Mach-O universal (armv7 arm64)
CodeDirectory v=20200 size=117161 flags=0x0(none) hashes=5849+5 location=embedded
Hash type=sha1 size=20
CDHash=7ee6c7e17dc788f8740ea06647cf47be7b20a727
Signature size=4333
Authority=iPhone Distribution: Mimedia Inc. (958B23VCBE)
Authority=Apple Worldwide Developer Relations Certification Authority
Authority=Apple Root CA
Signed Time=26 Aug 2015 12:42:04
Info.plist entries=38
TeamIdentifier=958B23VCBE
Sealed Resources version=2 rules=12 files=904
Internal requirements count=1 size=184
??qq<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>application-identifier</key>
        <string>958B23VCBE.com.mimedia.iOSv2</string>
        <key>beta-reports-active</key>
        <true/>
        <key>com.apple.developer.team-identifier</key>
        <string>958B23VCBE</string>
        <key>get-task-allow</key>
        <false/>
        <key>keychain-access-groups</key>
        <array>
                <string>958B23VCBE.com.mimedia.iOSv2</string>
        </array>
</dict>
</plist>
',
        sort: 2,
        internal_name: 'step_2',
        klass: 'Steps::Step_2'
    )
  end
end
