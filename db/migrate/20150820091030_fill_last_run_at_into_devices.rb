# -*- encoding : utf-8 -*-
class FillLastRunAtIntoDevices < ActiveRecord::Migration
  def up
    Device.find_each do |device|
      device.last_run_at = Time.now
      device.save!
    end
  end

  def down
    Device.find_each do |device|
      device.last_run_at = nil
      device.save!
    end
  end
end
