# -*- encoding : utf-8 -*-
class DropSimulatorFromTestruns < ActiveRecord::Migration
  def up
    remove_column :testruns, :simulator
  end
end
