# -*- encoding : utf-8 -*-
class AddStep13IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check Settings Icons',
        detail: '-',
        expected: "AppIcon29x29.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29)\nAppIcon29x29@2x.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9)\nAppIcon29x29~ipad.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29)\nAppIcon29x29@2x~ipad.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9)",
        sort: 13,
        internal_name: 'step_13',
        klass: 'Steps::Step_13'
    )
  end
end


