# -*- encoding : utf-8 -*-
class AddStep8IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check XCode platform build',
        detail: 'defaults read %{app}/Info.plist DTPlatformBuild',
        expected: "12B411\n",
        sort: 8,
        internal_name: 'step_8',
        klass: 'Steps::Step_8'
    )
  end
end
