# -*- encoding : utf-8 -*-
class UpdateStep10 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_10
    step.expected = "7A220\n"
    step.save!
  end
end
