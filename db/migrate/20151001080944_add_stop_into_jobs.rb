# -*- encoding : utf-8 -*-
class AddStopIntoJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :stop, :timestamp
  end
end
