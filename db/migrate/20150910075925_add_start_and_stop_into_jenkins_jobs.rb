# -*- encoding : utf-8 -*-
class AddStartAndStopIntoJenkinsJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :start, :datetime
    add_column :jenkins_jobs, :stop, :datetime
  end
end
