# -*- encoding : utf-8 -*-
class FillJobStatuses < ActiveRecord::Migration
  def up
    JobStatus.create!(name: 'Scheduled', internal_name: 'scheduled')
    JobStatus.create!(name: 'Running', internal_name: 'running')
    JobStatus.create!(name: 'Finished with success', internal_name: 'finished_with_success')
    JobStatus.create!(name: 'Finished with fail', internal_name: 'finished_with_fail')
  end
end
