# -*- encoding : utf-8 -*-
class CreateScenarios < ActiveRecord::Migration
  def change
    create_table :scenarios do |t|
      t.integer :testrun_id, null: false
      t.string :allure_id, null: false
      t.string :name, null: false
      t.boolean :failed_by_appium, null: false
      t.datetime :start, null: false
      t.datetime :stop, null: false

      t.timestamps null: false
    end
  end
end
