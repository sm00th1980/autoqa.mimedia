# -*- encoding : utf-8 -*-
class FixDataFullpathsForReadyTestruns < ActiveRecord::Migration
  def change
    testrun = Testrun.find_by(name: 'iPhone 6')
    testrun.jenkins_name = 'iphone_6 on 8.3'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 6 Plus')
    testrun.jenkins_name = 'iphone_6_Plus on 8.3'
    testrun.save!
  end
end
