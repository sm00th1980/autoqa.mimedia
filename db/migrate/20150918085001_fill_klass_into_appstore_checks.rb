# -*- encoding : utf-8 -*-
class FillKlassIntoAppstoreChecks < ActiveRecord::Migration
  def up
    prepare = AppstoreCheck.find_by(internal_name: 'prepare')
    prepare.klass = 'Steps::Prepare'
    prepare.save!

    puts prepare.inspect

    step_1 = AppstoreCheck.find_by(internal_name: 'step_1')
    step_1.klass = 'Steps::Step_1'
    step_1.save!

    change_column :appstore_checks, :klass, :string, null: false
  end
end
