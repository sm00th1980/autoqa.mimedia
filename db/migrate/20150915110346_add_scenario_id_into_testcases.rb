# -*- encoding : utf-8 -*-
class AddScenarioIdIntoTestcases < ActiveRecord::Migration
  def change
    add_column :testcases, :scenario_id, :integer
  end
end
