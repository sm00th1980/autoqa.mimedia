# -*- encoding : utf-8 -*-
class UpdateStep7 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_7
    step.expected = "9.0\n"
    step.save!
  end
end
