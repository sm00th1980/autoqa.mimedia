# -*- encoding : utf-8 -*-
class UpdatePathForDeviceToAllure < ActiveRecord::Migration
  def up
    Device.find_each do |device|
      device.jenkins_name = 'allure'
      device.save!
    end
  end

  def down
    Device.find_each do |device|
      device.jenkins_name = 'iphone_4s on 7.1'
      device.save!
    end
  end
end
