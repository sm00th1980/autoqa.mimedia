# -*- encoding : utf-8 -*-
class ChangeHostForNode2 < ActiveRecord::Migration
  def up
    node2 = Node.find_by(name: 'node2')
    node2.host = '192.168.162.203'
    node2.save!
  end
end
