# -*- encoding : utf-8 -*-
class RenameStep7AndStep8 < ActiveRecord::Migration
  def up
    step_7 = AppstoreCheck.step_7
    step_7.name = 'Check platform version'
    step_7.save!

    step_8 = AppstoreCheck.step_8
    step_8.name = 'Check platform build'
    step_8.save!
  end
end
