# -*- encoding : utf-8 -*-
class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string "name", null: false
      t.text "description"
      t.boolean "simulator", default: true

      t.timestamps null: false
    end
  end
end
