# -*- encoding : utf-8 -*-
class AddPrepareIntoAppstoreChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(name: 'Unzipping and prepare for checking', type: AppstoreCheckType.local, internal_name: 'prepare', sort: 0)
  end

  def down
    AppstoreCheck.find_by(internal_name: 'prepare').delete
  end
end
