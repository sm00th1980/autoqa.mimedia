# -*- encoding : utf-8 -*-
class AddStep10IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check DTXcodeBuild',
        detail: 'defaults read %{app}/Info.plist DTXcodeBuild',
        expected: "6A2008a\n",
        sort: 10,
        internal_name: 'step_10',
        klass: 'Steps::Step_10'
    )
  end
end
