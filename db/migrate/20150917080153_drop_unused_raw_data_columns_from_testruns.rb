# -*- encoding : utf-8 -*-
class DropUnusedRawDataColumnsFromTestruns < ActiveRecord::Migration
  def up
    remove_column :testruns, :environment
    remove_column :testruns, :report
    remove_column :testruns, :xunit
    remove_column :testruns, :defects
    remove_column :testruns, :graph
    remove_column :testruns, :behavior
  end
end
