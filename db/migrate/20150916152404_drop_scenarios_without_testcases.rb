# -*- encoding : utf-8 -*-
class DropScenariosWithoutTestcases < ActiveRecord::Migration
  def up
    Scenario.find_each do |scenario|
      if scenario.testcases.count == 0
        scenario.delete
      end
    end
  end
end
