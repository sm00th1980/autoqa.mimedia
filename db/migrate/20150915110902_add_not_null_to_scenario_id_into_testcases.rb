# -*- encoding : utf-8 -*-
class AddNotNullToScenarioIdIntoTestcases < ActiveRecord::Migration
  def up
    change_column :testcases, :scenario_id, :integer, null: false
  end
end
