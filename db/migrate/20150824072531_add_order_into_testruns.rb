# -*- encoding : utf-8 -*-
class AddOrderIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :sort, :integer, default: 1
  end
end
