# -*- encoding : utf-8 -*-
class FillNodeInIntoJenkinsJobs < ActiveRecord::Migration
  def up
    JenkinsJob.where(status: [JobStatus.running, JobStatus.finished_with_success, JobStatus.finished_with_fail]).find_each do |job|
      job.node_id = Node.first.id
      job.save!
    end
  end

  def down
    JenkinsJob.find_each do |job|
      job.node_id = nil
      job.save!
    end
  end
end
