# -*- encoding : utf-8 -*-
class AddJenkinsNameIntoDevices < ActiveRecord::Migration
  def change
    add_column :devices, :jenkins_name, :text
  end
end
