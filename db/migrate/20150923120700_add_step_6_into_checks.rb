# -*- encoding : utf-8 -*-
class AddStep6IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check version',
        detail: 'defaults read %{app}/Info.plist CFBundleVersion',
        expected: "%{version}\n",
        sort: 6,
        internal_name: 'step_6',
        klass: 'Steps::Step_6'
    )
  end
end
