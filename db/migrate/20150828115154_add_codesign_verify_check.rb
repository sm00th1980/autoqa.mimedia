# -*- encoding : utf-8 -*-
class AddCodesignVerifyCheck < ActiveRecord::Migration
  def up
    execute("INSERT INTO appstore_checks (name, detail, expected, created_at, updated_at) VALUES ('Check code sign verify', 'codesign --verify --no-strict -vvvv %{app}', '%{app}: valid on disk\\n%{app}: satisfies its Designated Requirement', '2015-09-21 12:48:58.394977', '2015-09-21 12:48:58.394977')")
  end

  def down
    AppstoreCheck.find_by(name: 'Check code sign verify').delete
  end
end
