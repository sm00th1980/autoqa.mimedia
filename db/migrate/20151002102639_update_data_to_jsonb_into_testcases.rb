# -*- encoding : utf-8 -*-
class UpdateDataToJsonbIntoTestcases < ActiveRecord::Migration
  def up
    remove_column :testcases, :data
    add_column :testcases, :data, :jsonb
  end
end
