# -*- encoding : utf-8 -*-
class AddStatusIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :status, :string
  end
end
