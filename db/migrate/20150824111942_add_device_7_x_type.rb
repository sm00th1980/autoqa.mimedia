# -*- encoding : utf-8 -*-
class AddDevice7XType < ActiveRecord::Migration
  def up
    TestrunType.create!(name: 'iOS 7.x (Device)', internal_name: 'device_ios_7_x')
  end
end
