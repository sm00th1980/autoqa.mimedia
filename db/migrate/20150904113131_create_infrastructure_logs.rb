# -*- encoding : utf-8 -*-
class CreateInfrastructureLogs < ActiveRecord::Migration
  def change
    create_table :infrastructure_logs do |t|
      t.integer :infrastructure_id, null: false
      t.boolean :available, null: false, default: false
      t.datetime :datetime, null: false

      t.timestamps null: false
    end
  end
end
