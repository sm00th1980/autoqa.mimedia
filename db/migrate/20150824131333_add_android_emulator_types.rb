# -*- encoding : utf-8 -*-
class AddAndroidEmulatorTypes < ActiveRecord::Migration
  def up
    TestrunType.create!(name: 'Android 4.3 - Jelly Bean (Emulator)', internal_name: 'emulator_android_4_3')
    TestrunType.create!(name: 'Android 4.4 - KitKat (Emulator)', internal_name: 'emulator_android_4_4')
    TestrunType.create!(name: 'Android 5.0 - Lollipop (Emulator)', internal_name: 'emulator_android_5_0')
    TestrunType.create!(name: 'Android 5.1 - Lollipop (Emulator)', internal_name: 'emulator_android_5_1')
  end
end
