# -*- encoding : utf-8 -*-
class AddTypeIntoAppstoreChecks < ActiveRecord::Migration
  def up
    add_column :appstore_checks, :type_id, :integer

    AppstoreCheck.find_each do |check|
      check.type_id = AppstoreCheckType.remote.id
      check.save!
    end

    change_column :appstore_checks, :type_id, :integer, null: false
  end

  def down
    add_column :appstore_checks, :type_id
  end
end
