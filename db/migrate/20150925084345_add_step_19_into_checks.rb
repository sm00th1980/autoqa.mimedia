# -*- encoding : utf-8 -*-
class AddStep19IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check architecture #3',
        detail: 'otool -vh %{executable}',
        expected: "arm64 has been supported",
        sort: 19,
        internal_name: 'step_19',
        klass: 'Steps::Step_19'
    )
  end
end


