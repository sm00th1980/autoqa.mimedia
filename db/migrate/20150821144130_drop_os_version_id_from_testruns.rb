# -*- encoding : utf-8 -*-
class DropOsVersionIdFromTestruns < ActiveRecord::Migration
  def up
    remove_column :testruns, :os_version_id
  end
end
