# -*- encoding : utf-8 -*-
class CleanDataForNotReadySimulators20150824 < ActiveRecord::Migration
  def up
    testrun = Testrun.find_by(name: 'iPhone 5', type: TestrunType.ios_8_3)
    testrun.jenkins_name = nil
    testrun.behavior = nil
    testrun.defects = nil
    testrun.environment = nil
    testrun.graph = nil
    testrun.report = nil
    testrun.xunit = nil

    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_7_1)
    testrun.jenkins_name = nil
    testrun.behavior = nil
    testrun.defects = nil
    testrun.environment = nil
    testrun.graph = nil
    testrun.report = nil
    testrun.xunit = nil

    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_8_3)
    testrun.jenkins_name = nil
    testrun.behavior = nil
    testrun.defects = nil
    testrun.environment = nil
    testrun.graph = nil
    testrun.report = nil
    testrun.xunit = nil

    testrun.save!
  end
end
