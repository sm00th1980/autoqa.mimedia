# -*- encoding : utf-8 -*-
class AddHomeDirIntoNodes < ActiveRecord::Migration
  def change
    add_column :nodes, :home_dir, :string
  end
end
