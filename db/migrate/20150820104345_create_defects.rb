# -*- encoding : utf-8 -*-
class CreateDefects < ActiveRecord::Migration
  def change
    create_table :defects do |t|
      t.json :data
      t.integer :device_id, null: false

      t.timestamps null: false
    end
  end
end
