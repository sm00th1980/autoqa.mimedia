# -*- encoding : utf-8 -*-
class UpdateExpectedForStep11To15 < ActiveRecord::Migration
  def up
    AppstoreCheck.step_11.update_attribute(:expected, "AppIcon60x60@2x.png (MD5 = e73ebed50450ba4b742c23bfbdcfd49c, SIZE = 120x120)\nAppIcon60x60@3x.png (MD5 = d676993636dd01b57deb8f8d18df3299, SIZE = 180x180)")
    AppstoreCheck.step_12.update_attribute(:expected, "AppIcon76x76~ipad.png (MD5 = efadc0a3dcfe7d8243ebaed6c05e3ef4, SIZE = 76x76)\nAppIcon76x76@2x~ipad.png (MD5 = 73533b632acafccbd51505c506085a76, SIZE = 152x152)")
    AppstoreCheck.step_13.update_attribute(:expected, "AppIcon29x29.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29, SIZE = 29x29)\nAppIcon29x29@2x.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9, SIZE = 58x58)\nAppIcon29x29~ipad.png (MD5 = 6270381645d8a22adab8af5a5f8f7c29, SIZE = 29x29)\nAppIcon29x29@2x~ipad.png (MD5 = bb39a9fbdee6ed15eccf4183ea64cea9, SIZE = 58x58)")
    AppstoreCheck.step_14.update_attribute(:expected, "LaunchImage.png (MD5 = 89817fb95ab12d69b0f010450ad29428, SIZE = 320x480)\nLaunchImage@2x.png (MD5 = 278d5a9802193ae28d8698ef5dc7240a, SIZE = 640x960)\nLaunchImage-568h@2x.png (MD5 = 6e41359534fd8a60f4a419c5e2caa429, SIZE = 640x1136)\nLaunchImage-800-667h@2x.png (MD5 = b7cf9b3955f9b49ff4ecd39577947861, SIZE = 750x1334)\nLaunchImage-800-Portrait-736h@3x.png (MD5 = 45e1109fedc69abd44a8f3452659feb1, SIZE = 1242x2208)\nLaunchImage-800-Landscape-736h@3x.png (MD5 = 3a26e026cf7000a50d98f6ab54310b19, SIZE = 2208x1242)")
    AppstoreCheck.step_15.update_attribute(:expected, "LaunchImage-Portrait~ipad.png (MD5 = dcf60180b258defb566de5042ce79bbd, SIZE = 768x1024)\nLaunchImage-Landscape~ipad.png (MD5 = 36047b1203297e4c7288092ff3e9aa62, SIZE = 1024x768)\nLaunchImage-Portrait@2x~ipad.png (MD5 = 2217217bac18a20935025fe19c27be93, SIZE = 1536x2048)\nLaunchImage-Landscape@2x~ipad.png (MD5 = bf91364ae71b4dab8a0c79e64903b220, SIZE = 2048x1536)")
  end
end
