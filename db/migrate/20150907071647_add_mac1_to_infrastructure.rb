# -*- encoding : utf-8 -*-
class AddMac1ToInfrastructure < ActiveRecord::Migration
  def up
    Infrastructure.create!(name: 'node1', url: 'http://192.168.25.7:4723//wd/hub/session', success_result: {"status":0,"value":{"webStorageEnabled":false,"locationContextEnabled":false,"browserName":"iOS","platform":"MAC","javascriptEnabled":true,"databaseEnabled":false,"takesScreenshot":true,"networkConnectionEnabled":false,"warnings":{},"desired":{"waitForAppScript":"$.delay(7500); true","newCommandTimeout":600,"platformVersion":"8.3","autoAcceptAlerts":false,"app":"/Users/deyarov/projects/Mobile_AutomatedTest/ios/prebuilds/MiMedia_i386.app.zip","platformName":"iOS","deviceName":"iPhone 6","appium-version":"1.4.10"},"waitForAppScript":"$.delay(7500); true","newCommandTimeout":600,"platformVersion":"8.3","autoAcceptAlerts":false,"app":"/Users/deyarov/projects/Mobile_AutomatedTest/ios/prebuilds/MiMedia_i386.app.zip","platformName":"iOS","deviceName":"iPhone 6","appium-version":"1.4.10"},"sessionId":"321da26e-2121-4c27-a34b-6a5b8da3ff8e"}.to_json)
  end

  def down
    Infrastructure.find_by(name: 'node1').delete
  end
end
