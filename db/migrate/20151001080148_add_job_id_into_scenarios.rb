# -*- encoding : utf-8 -*-
class AddJobIdIntoScenarios < ActiveRecord::Migration
  def change
    add_column :scenarios, :job_id, :integer
  end
end
