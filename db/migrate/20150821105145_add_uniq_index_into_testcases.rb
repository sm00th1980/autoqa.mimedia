# -*- encoding : utf-8 -*-
class AddUniqIndexIntoTestcases < ActiveRecord::Migration
  def change
    add_index :testcases, [:allure_id, :device_id], unique: true
  end
end
