# -*- encoding : utf-8 -*-
class AddStep14IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check iPhone/iPod launch images',
        detail: '-',
        expected: "LaunchImage.png (MD5 = 89817fb95ab12d69b0f010450ad29428)\nLaunchImage@2x.png (MD5 = 278d5a9802193ae28d8698ef5dc7240a)\nLaunchImage-568h@2x.png (MD5 = 6e41359534fd8a60f4a419c5e2caa429)\nLaunchImage-800-667h@2x.png (MD5 = b7cf9b3955f9b49ff4ecd39577947861)\nLaunchImage-800-Portrait-736h@3x.png (MD5 = 45e1109fedc69abd44a8f3452659feb1)\nLaunchImage-800-Landscape-736h@3x.png (MD5 = 3a26e026cf7000a50d98f6ab54310b19)",
        sort: 14,
        internal_name: 'step_14',
        klass: 'Steps::Step_14'
    )
  end
end


