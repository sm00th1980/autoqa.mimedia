# -*- encoding : utf-8 -*-
class CreateOsVersions < ActiveRecord::Migration
  def change
    create_table :os_versions do |t|
      t.string "name", null: false
      t.text "description"

      t.timestamps null: false
    end
  end
end
