# -*- encoding : utf-8 -*-
class DropUnusedColumnsIntoAppstoreChecks < ActiveRecord::Migration
  def up
    remove_column :appstore_checks, :got
    remove_column :appstore_checks, :type_id
  end
end
