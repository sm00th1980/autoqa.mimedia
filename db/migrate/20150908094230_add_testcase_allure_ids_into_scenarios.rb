# -*- encoding : utf-8 -*-
class AddTestcaseAllureIdsIntoScenarios < ActiveRecord::Migration
  def change
    add_column :scenarios, :testcase_allure_ids, :string, array: true
  end
end
