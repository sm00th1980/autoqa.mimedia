# -*- encoding : utf-8 -*-
class FillTypeIdIntoTestruns < ActiveRecord::Migration
  def up

    Testrun.where(os_version: OsVersion.version_7_1).find_each do |testrun|
      testrun.type_id = TestrunType.ios_7_1.id
      testrun.save!
    end

    Testrun.where(os_version: OsVersion.version_8_3).find_each do |testrun|
      testrun.type_id = TestrunType.ios_8_3.id
      testrun.save!
    end

    Testrun.where(name: 'API').find_each do |testrun|
      testrun.type_id = TestrunType.api.id
      testrun.save!
    end
  end

  def down
    Testrun.find_each do |testrun|
      testrun.type_id = nil
      testrun.save!
    end
  end
end
