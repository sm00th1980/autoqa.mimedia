# -*- encoding : utf-8 -*-
class UpdateExpectedForStep1 < ActiveRecord::Migration
  def up
    execute("update appstore_checks set expected='%{app}: valid on disk\n%{app}: satisfies its Designated Requirement\n'")
  end
end
