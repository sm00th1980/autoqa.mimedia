# -*- encoding : utf-8 -*-
class AddInternalTypeIntoTestrunTypes < ActiveRecord::Migration
  def change
    add_column :testrun_types, :internal_name, :string
  end
end
