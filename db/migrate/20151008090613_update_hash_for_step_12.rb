# -*- encoding : utf-8 -*-
class UpdateHashForStep12 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_12
    step.expected = "AppIcon76x76~ipad.png (MD5 = 55afe79a201f7598a6b522a9ae0164fb, SIZE = 76x76)\nAppIcon76x76@2x~ipad.png (MD5 = 3e2cdbea632f477fcec314d843776486, SIZE = 152x152)"
    step.save!
  end

  def down
    step = AppstoreCheck.step_12
    step.expected = "AppIcon76x76~ipad.png (MD5 = efadc0a3dcfe7d8243ebaed6c05e3ef4, SIZE = 76x76)\nAppIcon76x76@2x~ipad.png (MD5 = 73533b632acafccbd51505c506085a76, SIZE = 152x152)"
    step.save!
  end
end
