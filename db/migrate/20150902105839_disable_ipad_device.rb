# -*- encoding : utf-8 -*-
class DisableIpadDevice < ActiveRecord::Migration
  def up
    ipad = Testrun.find_by(name: 'iPad Mini 3 #185 on iOS 8.1.1<br>UDID: 705025e2c3702bcb0f44865a371f479aa3233eec')
    ipad.disabled = true
    ipad.save!
  end

  def down
    ipad = Testrun.find_by(name: 'iPad Mini 3 #185 on iOS 8.1.1<br>UDID: 705025e2c3702bcb0f44865a371f479aa3233eec')
    ipad.disabled = false
    ipad.save!
  end
end
