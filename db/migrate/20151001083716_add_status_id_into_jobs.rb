# -*- encoding : utf-8 -*-
class AddStatusIdIntoJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :status_id, :integer
  end
end
