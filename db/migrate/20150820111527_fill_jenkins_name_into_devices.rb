# -*- encoding : utf-8 -*-
class FillJenkinsNameIntoDevices < ActiveRecord::Migration
  def up
    Device.find_each do |device|
      device.jenkins_name = 'iphone_4s on 7.1'
      device.save!
    end
  end

  def down
    Device.find_each do |device|
      device.jenkins_name = nil
      device.save!
    end
  end
end
