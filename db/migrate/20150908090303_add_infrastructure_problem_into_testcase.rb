# -*- encoding : utf-8 -*-
class AddInfrastructureProblemIntoTestcase < ActiveRecord::Migration
  def change
    add_column :testcases, :infrastructure_problem, :boolean
  end
end
