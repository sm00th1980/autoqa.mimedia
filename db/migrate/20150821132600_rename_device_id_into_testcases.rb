# -*- encoding : utf-8 -*-
class RenameDeviceIdIntoTestcases < ActiveRecord::Migration
  def change
    rename_column :testcases, :device_id, :testrun_id
  end
end
