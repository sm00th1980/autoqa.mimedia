# -*- encoding : utf-8 -*-
class FillInternalNameIntoAppstoreChecks < ActiveRecord::Migration
  def change
    AppstoreCheck.find_each do |check|
      check.internal_name = 'step_1'
      check.save!
    end

    change_column :appstore_checks, :internal_name, :string, null: false
    add_index :appstore_checks, :internal_name, unique: true
  end
end
