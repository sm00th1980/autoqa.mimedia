# -*- encoding : utf-8 -*-
class CreateInfrastructures < ActiveRecord::Migration
  def change
    create_table :infrastructures do |t|
      t.string "name", null: false
      t.text "url", null: false
      t.text "success_result", null: false

      t.timestamps null: false
    end
  end
end
