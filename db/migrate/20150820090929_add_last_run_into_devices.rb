# -*- encoding : utf-8 -*-
class AddLastRunIntoDevices < ActiveRecord::Migration
  def change
    add_column :devices, :last_run_at, :timestamp
  end
end
