# -*- encoding : utf-8 -*-
class UpdateHashForStep14 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_14
    step.expected = "LaunchImage.png (MD5 = b327f96c529f29daa169a591b0dfcd8e, SIZE = 320x480)\nLaunchImage@2x.png (MD5 = b8a62b3cf58a668e79c7ab0defa6df3a, SIZE = 640x960)\nLaunchImage-568h@2x.png (MD5 = 30951d5509a3341d61350c337575108a, SIZE = 640x1136)\nLaunchImage-800-667h@2x.png (MD5 = 9e395ad42d7a18b80eb6b3277fda2568, SIZE = 750x1334)\nLaunchImage-800-Portrait-736h@3x.png (MD5 = 98eb2eabf0b28f7b7cd2bf0461703705, SIZE = 1242x2208)\nLaunchImage-800-Landscape-736h@3x.png (MD5 = 044d1a4cfdecb9f902a05f35a0c3d3a4, SIZE = 2208x1242)"
    step.save!
  end

  def down
    step = AppstoreCheck.step_14
    step.expected = "LaunchImage.png (MD5 = 89817fb95ab12d69b0f010450ad29428, SIZE = 320x480)\nLaunchImage@2x.png (MD5 = 278d5a9802193ae28d8698ef5dc7240a, SIZE = 640x960)\nLaunchImage-568h@2x.png (MD5 = 6e41359534fd8a60f4a419c5e2caa429, SIZE = 640x1136)\nLaunchImage-800-667h@2x.png (MD5 = b7cf9b3955f9b49ff4ecd39577947861, SIZE = 750x1334)\nLaunchImage-800-Portrait-736h@3x.png (MD5 = 45e1109fedc69abd44a8f3452659feb1, SIZE = 1242x2208)\nLaunchImage-800-Landscape-736h@3x.png (MD5 = 3a26e026cf7000a50d98f6ab54310b19, SIZE = 2208x1242)"
    step.save!
  end
end
