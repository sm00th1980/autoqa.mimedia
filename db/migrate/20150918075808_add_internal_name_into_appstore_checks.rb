# -*- encoding : utf-8 -*-
class AddInternalNameIntoAppstoreChecks < ActiveRecord::Migration
  def change
    add_column :appstore_checks, :internal_name, :string
  end
end
