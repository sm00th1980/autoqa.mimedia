# -*- encoding : utf-8 -*-
class FixNameWithApostropheInScenarioNames < ActiveRecord::Migration
  def up
    Scenario.where("name like '%i''m%'").find_each do |scenario|
      scenario.name = scenario.name.sub("i'm", "I am")
      scenario.save!
    end
  end
end
