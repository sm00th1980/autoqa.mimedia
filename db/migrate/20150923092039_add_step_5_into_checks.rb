# -*- encoding : utf-8 -*-
class AddStep5IntoChecks < ActiveRecord::Migration
  def up
    AppstoreCheck.create!(
        name: 'Check Bundle ID',
        detail: 'defaults read %{app}/Info.plist CFBundleIdentifier',
        expected: "com.mimedia.iOSv2\n",
        sort: 5,
        internal_name: 'step_5',
        klass: 'Steps::Step_5'
    )
  end
end
