# -*- encoding : utf-8 -*-
class AddStatsColumnsIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :start, :datetime
    add_column :testruns, :stop, :datetime
    add_column :testruns, :failed_scenarios_count, :integer, null: false, default: 0
    add_column :testruns, :passed_scenarios_count, :integer, null: false, default: 0
    add_column :testruns, :pending_scenarios_count, :integer, null: false, default: 0
  end
end
