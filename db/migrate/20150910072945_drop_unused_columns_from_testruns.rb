# -*- encoding : utf-8 -*-
class DropUnusedColumnsFromTestruns < ActiveRecord::Migration
  def up
    remove_column :testruns, :jenkins_name
    remove_column :testruns, :status
    remove_column :testruns, :current_job_name
  end
end
