# -*- encoding : utf-8 -*-
class AddApiTestrun < ActiveRecord::Migration
  def up
    Testrun.create!(name: 'API', type: TestrunType.api)
  end

  def down
    Testrun.find_by(name: 'API').delete
  end
end
