# -*- encoding : utf-8 -*-
class UpdateStep8 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_8
    step.expected = "13A340\n"
    step.save!
  end
end
