# -*- encoding : utf-8 -*-
class CreateEnvironments < ActiveRecord::Migration
  def change
    create_table :environments do |t|
      t.json :data
      t.integer :device_id, null: false
      
      t.timestamps null: false
    end
  end
end
