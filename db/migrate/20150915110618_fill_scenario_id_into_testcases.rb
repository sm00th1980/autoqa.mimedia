# -*- encoding : utf-8 -*-
class FillScenarioIdIntoTestcases < ActiveRecord::Migration
  def up
    Scenario.find_each do |scenario|
      scenario.testcase_allure_ids.each do |allure_id|
        testcase = Testcase.find_by(allure_id: allure_id)
        if testcase
          testcase.scenario_id = scenario.id
          testcase.save!
        end
      end
    end
    Testcase.where(scenario_id: nil).delete_all
  end

  def down
    Testcase.find_each do |testcase|
      testcase.scenario_id = nil
      testcase.save!
    end
  end
end
