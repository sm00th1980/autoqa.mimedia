# -*- encoding : utf-8 -*-
class RenameTableToTestruns < ActiveRecord::Migration
  def change
    rename_table :devices, :testruns
  end
end
