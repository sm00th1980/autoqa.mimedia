# -*- encoding : utf-8 -*-
class DropTableDefects < ActiveRecord::Migration
  def up
    drop_table :defects
    drop_table :environments
  end
end
