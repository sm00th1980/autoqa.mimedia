# -*- encoding : utf-8 -*-
class AddCurrentJobNameIntoJenkinsJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :current_job_name, :string
  end
end
