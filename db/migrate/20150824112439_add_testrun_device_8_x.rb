# -*- encoding : utf-8 -*-
class AddTestrunDevice8X < ActiveRecord::Migration
  def up
    Testrun.create!(name: 'iPad Mini 3 (UDID:705025e2c3702bcb0f44865a371f479aa3233eec)', simulator: false, type: TestrunType.device_ios_8_x, jenkins_name: 'device iPad_3_1 on 8.2')
  end

  def down
    Testrun.find_by(name: 'iPad Mini 3 (UDID:705025e2c3702bcb0f44865a371f479aa3233eec)').delete
  end
end
