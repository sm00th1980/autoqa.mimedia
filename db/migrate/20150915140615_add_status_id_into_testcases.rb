# -*- encoding : utf-8 -*-
class AddStatusIdIntoTestcases < ActiveRecord::Migration
  def change
    add_column :testcases, :status_id, :integer
  end
end
