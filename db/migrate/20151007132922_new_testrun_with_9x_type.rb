# -*- encoding : utf-8 -*-
class NewTestrunWith9xType < ActiveRecord::Migration
  def up
    Testrun.create!(name: 'iPhone 4s', type: TestrunType.simulator_ios_9_x, sort: 1, disabled: false)
    Testrun.create!(name: 'iPhone 5', type: TestrunType.simulator_ios_9_x, sort: 2, disabled: false)
    Testrun.create!(name: 'iPhone 5s', type: TestrunType.simulator_ios_9_x, sort: 3, disabled: false)
    Testrun.create!(name: 'iPhone 6', type: TestrunType.simulator_ios_9_x, sort: 4, disabled: false)
    Testrun.create!(name: 'iPhone 6+', type: TestrunType.simulator_ios_9_x, sort: 5, disabled: false)
    Testrun.create!(name: 'iPad 2', type: TestrunType.simulator_ios_9_x, sort: 6, disabled: false)
  end
end
