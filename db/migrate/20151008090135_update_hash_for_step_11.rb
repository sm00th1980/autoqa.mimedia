# -*- encoding : utf-8 -*-
class UpdateHashForStep11 < ActiveRecord::Migration
  def up
    step_11 = AppstoreCheck.step_11
    step_11.expected = "AppIcon60x60@2x.png (MD5 = c2ac7beedc47df00f1706ed8ea49c9b6, SIZE = 120x120)\nAppIcon60x60@3x.png (MD5 = 77e6e87e39f0fc66cc7b88092515ccf7, SIZE = 180x180)"
    step_11.save!
  end

  def down
    step_11 = AppstoreCheck.step_11
    step_11.expected = "AppIcon60x60@2x.png (MD5 = e73ebed50450ba4b742c23bfbdcfd49c, SIZE = 120x120)\nAppIcon60x60@3x.png (MD5 = d676993636dd01b57deb8f8d18df3299, SIZE = 180x180)"
    step_11.save!
  end
end
