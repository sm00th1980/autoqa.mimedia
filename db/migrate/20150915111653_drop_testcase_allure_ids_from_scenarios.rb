# -*- encoding : utf-8 -*-
class DropTestcaseAllureIdsFromScenarios < ActiveRecord::Migration
  def up
    remove_column :scenarios, :testcase_allure_ids
  end
end
