# -*- encoding : utf-8 -*-
class FillTestrunTypes < ActiveRecord::Migration
  def up
    TestrunType.create!(name: 'iOS 7.1 (Simulator)')
    TestrunType.create!(name: 'iOS 8.3 (Simulator)')
    TestrunType.create!(name: 'API')
  end

  def down
    TestrunType.delete_all
  end
end
