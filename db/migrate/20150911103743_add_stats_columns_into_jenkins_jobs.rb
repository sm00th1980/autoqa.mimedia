# -*- encoding : utf-8 -*-
class AddStatsColumnsIntoJenkinsJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :failed_scenarios_count, :integer, null: false, default: 0
    add_column :jenkins_jobs, :passed_scenarios_count, :integer, null: false, default: 0
    add_column :jenkins_jobs, :pending_scenarios_count, :integer, null: false, default: 0
  end
end
