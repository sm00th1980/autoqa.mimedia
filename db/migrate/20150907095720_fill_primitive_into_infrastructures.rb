# -*- encoding : utf-8 -*-
class FillPrimitiveIntoInfrastructures < ActiveRecord::Migration
  def up
    Infrastructure.find_each do |inf|
      inf.primitive = true
      inf.save!
    end

    change_column :infrastructures, :primitive, :boolean, null: false
  end
end
