# -*- encoding : utf-8 -*-
class AddTempJenkinsNameIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :temp_jenkins_name, :string
  end
end
