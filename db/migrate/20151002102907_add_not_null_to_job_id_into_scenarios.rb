# -*- encoding : utf-8 -*-
class AddNotNullToJobIdIntoScenarios < ActiveRecord::Migration
  def up
    change_column :scenarios, :job_id, :integer, null: false
  end

  def down
    change_column :scenarios, :job_id, :integer, null: true
  end
end
