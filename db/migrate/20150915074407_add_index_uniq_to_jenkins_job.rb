# -*- encoding : utf-8 -*-
class AddIndexUniqToJenkinsJob < ActiveRecord::Migration
  def change
    add_index :jenkins_jobs, [:testrun_id, :scenario_id], unique: true
  end
end
