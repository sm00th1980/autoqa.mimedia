# -*- encoding : utf-8 -*-
class AddNodeIdIntoJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :node_id, :integer
  end
end
