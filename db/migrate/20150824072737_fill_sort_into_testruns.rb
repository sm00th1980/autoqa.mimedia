# -*- encoding : utf-8 -*-
class FillSortIntoTestruns < ActiveRecord::Migration
  def up
    #7.1
    testrun = Testrun.find_by(name: 'iPhone 4s', type: TestrunType.ios_7_1)
    testrun.sort = 1
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5', type: TestrunType.ios_7_1)
    testrun.sort = 2
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5s', type: TestrunType.ios_7_1)
    testrun.sort = 3
    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_7_1)
    testrun.sort = 4
    testrun.save!


    #8.3
    testrun = Testrun.find_by(name: 'iPhone 4s', type: TestrunType.ios_8_3)
    testrun.sort = 1
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5', type: TestrunType.ios_8_3)
    testrun.sort = 2
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5s', type: TestrunType.ios_8_3)
    testrun.sort = 3
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 6', type: TestrunType.ios_8_3)
    testrun.sort = 4
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 6 Plus', type: TestrunType.ios_8_3)
    testrun.sort = 5
    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_8_3)
    testrun.sort = 6
    testrun.save!
  end
end
