# -*- encoding : utf-8 -*-
class RenameStep19 < ActiveRecord::Migration
  def up
    step_19 = AppstoreCheck.step_19
    step_19.name = 'Check ARM64 support'
    step_19.save!
  end
end
