# -*- encoding : utf-8 -*-
class AddTypeIdIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :type_id, :integer
  end
end
