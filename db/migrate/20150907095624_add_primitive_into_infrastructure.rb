# -*- encoding : utf-8 -*-
class AddPrimitiveIntoInfrastructure < ActiveRecord::Migration
  def change
    add_column :infrastructures, :primitive, :boolean
  end
end
