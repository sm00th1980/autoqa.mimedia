# -*- encoding : utf-8 -*-
class AddAllureColumnsIntoDevice < ActiveRecord::Migration
  def change
    add_column :devices, :behavior, :json
    add_column :devices, :defects, :json
    add_column :devices, :environment, :json
    add_column :devices, :graph, :json
    add_column :devices, :report, :json
    add_column :devices, :xunit, :json
  end
end
