# -*- encoding : utf-8 -*-
class AddNewDevices < ActiveRecord::Migration
  def up
    os_version_7_1 = OsVersion.find_by(name: 'iOS 7.1').id
    os_version_8_3 = OsVersion.find_by(name: 'iOS 8.3').id

    Device.create!(name: 'iPhone 4s', simulator: true, os_version_id: os_version_7_1)
    Device.create!(name: 'iPhone 5', simulator: true, os_version_id: os_version_7_1)
    Device.create!(name: 'iPhone 5s', simulator: true, os_version_id: os_version_7_1)
    Device.create!(name: 'iPad 2', simulator: true, os_version_id: os_version_7_1)

    Device.create!(name: 'iPhone 4s', simulator: true, os_version_id: os_version_8_3)
    Device.create!(name: 'iPhone 5', simulator: true, os_version_id: os_version_8_3)
    Device.create!(name: 'iPhone 5s', simulator: true, os_version_id: os_version_8_3)
    Device.create!(name: 'iPhone 6', simulator: true, os_version_id: os_version_8_3)
    Device.create!(name: 'iPhone 6 Plus', simulator: true, os_version_id: os_version_8_3)
    Device.create!(name: 'iPad 2', simulator: true, os_version_id: os_version_8_3)
  end

  def down
    Device.delete_all
  end
end
