# -*- encoding : utf-8 -*-
class DropUnusedColumnsFromJenkinsJobs < ActiveRecord::Migration
  def up
    remove_column :jenkins_jobs, :running
    remove_column :jenkins_jobs, :stop
    remove_column :jenkins_jobs, :failed_scenarios_count
    remove_column :jenkins_jobs, :passed_scenarios_count
    remove_column :jenkins_jobs, :pending_scenarios_count
  end
end
