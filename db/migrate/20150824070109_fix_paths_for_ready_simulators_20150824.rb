# -*- encoding : utf-8 -*-
class FixPathsForReadySimulators20150824 < ActiveRecord::Migration
  def up
    testrun = Testrun.find_by(name: 'iPhone 4s', type: TestrunType.ios_7_1)
    testrun.jenkins_name = 'iphone_4s on 7.1'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 4s', type: TestrunType.ios_8_3)
    testrun.jenkins_name = 'iphone_4s on 8.3'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5', type: TestrunType.ios_7_1)
    testrun.jenkins_name = 'iphone_5 on 7.1'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5s', type: TestrunType.ios_7_1)
    testrun.jenkins_name = 'iphone_5s on 7.1'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPhone 5s', type: TestrunType.ios_8_3)
    testrun.jenkins_name = 'iphone_5s on 8.3'
    testrun.save!
  end
end
