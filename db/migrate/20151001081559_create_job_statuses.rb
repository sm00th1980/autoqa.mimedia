# -*- encoding : utf-8 -*-
class CreateJobStatuses < ActiveRecord::Migration
  def change
    create_table :job_statuses do |t|
      t.string "name", null: false
      t.string "internal_name", null: false
      
      t.timestamps null: false
    end

    add_index :job_statuses, :internal_name, unique: true
  end
end
