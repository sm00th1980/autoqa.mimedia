# -*- encoding : utf-8 -*-
class AddPendingIntoTestcases < ActiveRecord::Migration
  def change
    add_column :testcases, :pending, :boolean
  end
end
