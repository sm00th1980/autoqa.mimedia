# -*- encoding : utf-8 -*-
class AddPathsForReadySimulators < ActiveRecord::Migration
  def up
    testrun = Testrun.find_by(name: 'iPhone 5', type: TestrunType.ios_8_3)
    testrun.jenkins_name = 'iphone_5 on 8.3'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_7_1)
    testrun.jenkins_name = 'ipad_2 on 7.1'
    testrun.save!

    testrun = Testrun.find_by(name: 'iPad 2', type: TestrunType.ios_8_3)
    testrun.jenkins_name = 'ipad_2 on 8.3'
    testrun.save!
  end
end
