# -*- encoding : utf-8 -*-
class AddScenarioIdsIntoJenkinsJobs < ActiveRecord::Migration
  def change
    add_column :jenkins_jobs, :scenario_ids, :integer, array: true, default: [], null: false
  end
end
