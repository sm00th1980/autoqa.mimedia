# -*- encoding : utf-8 -*-
class CleanAllData < ActiveRecord::Migration
  def up
    Testcase.delete_all
    Scenario.delete_all
    JenkinsJob.delete_all

    Testrun.find_each do |testrun|
      testrun.failed_scenarios_count = 0
      testrun.passed_scenarios_count = 0
      testrun.pending_scenarios_count = 0
      testrun.save!
    end
  end
end
