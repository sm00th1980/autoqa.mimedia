# -*- encoding : utf-8 -*-
class DropTestrunIdFromTestcases < ActiveRecord::Migration
  def change
    remove_column :testcases, :testrun_id
  end
end
