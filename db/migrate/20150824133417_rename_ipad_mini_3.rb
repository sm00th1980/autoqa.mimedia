# -*- encoding : utf-8 -*-
class RenameIpadMini3 < ActiveRecord::Migration
  def up
    testrun = Testrun.find_by(name: 'iPad Mini 3 (UDID:705025e2c3702bcb0f44865a371f479aa3233eec)')
    testrun.name = 'iPad Mini 3 #185 on iOS 8.1.1<br>UDID: 705025e2c3702bcb0f44865a371f479aa3233eec'
    testrun.save!
  end
end
