# -*- encoding : utf-8 -*-
class CreateTestcases < ActiveRecord::Migration
  def change
    create_table :testcases do |t|
      t.integer :device_id, null: false
      t.string :allure_id, null: false
      t.json :data

      t.timestamps null: false
    end
  end
end
