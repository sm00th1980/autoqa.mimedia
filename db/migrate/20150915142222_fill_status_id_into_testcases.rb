# -*- encoding : utf-8 -*-
class FillStatusIdIntoTestcases < ActiveRecord::Migration
  def up
    Testcase.find_each do |testcase|
      testcase.status_id = Parser.testcase_status(testcase.data.to_json).id
      testcase.save!
    end

    change_column :testcases, :status_id, :integer, null: false
  end

  def down
    Testcase.find_each do |testcase|
      testcase.status_id = nil
      testcase.save!
    end
  end
end
