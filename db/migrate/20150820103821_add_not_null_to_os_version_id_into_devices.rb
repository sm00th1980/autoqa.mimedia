# -*- encoding : utf-8 -*-
class AddNotNullToOsVersionIdIntoDevices < ActiveRecord::Migration
  def up
    change_column :devices, :os_version_id, :integer, null: false
    change_column :devices, :simulator, :boolean, null: false, default: true
  end

  def down
    change_column :devices, :os_version_id, :integer, null: true
    change_column :devices, :simulator, :boolean, null: true, default: true
  end
end
