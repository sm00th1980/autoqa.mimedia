# -*- encoding : utf-8 -*-
class UpdateHashForStep15 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_15
    step.expected = "LaunchImage-Portrait~ipad.png (MD5 = 8914bc71fea59318dca3b09890c4ff30, SIZE = 768x1024)\nLaunchImage-Landscape~ipad.png (MD5 = 67023476a6dd24d8ee4d86243a6bac56, SIZE = 1024x768)\nLaunchImage-Portrait@2x~ipad.png (MD5 = 092ccfb6b9fbdbd7b0a5703e1d954d32, SIZE = 1536x2048)\nLaunchImage-Landscape@2x~ipad.png (MD5 = 24cee3bf1e2cc6c1797410334030addc, SIZE = 2048x1536)"
    step.save!
  end

  def down
    step = AppstoreCheck.step_15
    step.expected = "LaunchImage-Portrait~ipad.png (MD5 = dcf60180b258defb566de5042ce79bbd, SIZE = 768x1024)\nLaunchImage-Landscape~ipad.png (MD5 = 36047b1203297e4c7288092ff3e9aa62, SIZE = 1024x768)\nLaunchImage-Portrait@2x~ipad.png (MD5 = 2217217bac18a20935025fe19c27be93, SIZE = 1536x2048)\nLaunchImage-Landscape@2x~ipad.png (MD5 = bf91364ae71b4dab8a0c79e64903b220, SIZE = 2048x1536)"
    step.save!
  end
end
