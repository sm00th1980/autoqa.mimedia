# -*- encoding : utf-8 -*-
class CreateJenkinsJobs < ActiveRecord::Migration
  def change
    create_table :jenkins_jobs do |t|
      t.integer :testrun_id, null: false
      t.boolean :running, null: false

      t.timestamps null: false
    end
  end
end
