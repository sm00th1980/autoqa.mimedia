# -*- encoding : utf-8 -*-
class CleanDataForApiTestruns < ActiveRecord::Migration
  def change
    Testrun.api.find_each do |testrun|
      testrun.jenkins_name = 'tmp'
      testrun.behavior = nil
      testrun.defects = nil
      testrun.environment = nil
      testrun.graph = nil
      testrun.report = nil
      testrun.xunit = nil

      testrun.save!
    end
  end
end
