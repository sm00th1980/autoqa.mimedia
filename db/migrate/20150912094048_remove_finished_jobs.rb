# -*- encoding : utf-8 -*-
class RemoveFinishedJobs < ActiveRecord::Migration
  def up
    JenkinsJob.where('start is not null and stop is not null').delete_all
  end
end
