# -*- encoding : utf-8 -*-
class RenameStep18 < ActiveRecord::Migration
  def up
    step_18 = AppstoreCheck.step_18
    step_18.name = 'Check architecture'
    step_18.save!
  end
end
