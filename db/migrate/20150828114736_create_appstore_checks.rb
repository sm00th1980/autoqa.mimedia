# -*- encoding : utf-8 -*-
class CreateAppstoreChecks < ActiveRecord::Migration
  def change
    create_table :appstore_checks do |t|
      t.string "name", null: false
      t.text "detail"
      t.text "expected"
      t.text "got"
      t.integer "sort", default: 1, null: false

      t.timestamps null: false
    end
  end
end
