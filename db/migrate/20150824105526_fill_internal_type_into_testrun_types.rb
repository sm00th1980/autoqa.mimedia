# -*- encoding : utf-8 -*-
class FillInternalTypeIntoTestrunTypes < ActiveRecord::Migration
  def up
    testrun = TestrunType.find_by(name: "iOS 7.1 (Simulator)")
    testrun.internal_name = 'simulator_ios_7_x'
    testrun.save!

    testrun = TestrunType.find_by(name: "iOS 8.3 (Simulator)")
    testrun.internal_name = 'simulator_ios_8_x'
    testrun.save!

    testrun = TestrunType.find_by(name: "iOS 8.x (Device)")
    testrun.internal_name = 'device_ios_8_x'
    testrun.save!

    testrun = TestrunType.find_by(name: "API")
    testrun.internal_name = 'api'
    testrun.save!

    change_column :testrun_types, :internal_name, :string, null: false
    add_index :testrun_types, :internal_name, unique: true
  end
end
