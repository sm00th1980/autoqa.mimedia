# -*- encoding : utf-8 -*-
class DropOsVersionTable < ActiveRecord::Migration
  def up
    drop_table :os_versions
  end
end
