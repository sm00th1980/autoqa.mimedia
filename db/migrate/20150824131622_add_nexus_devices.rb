# -*- encoding : utf-8 -*-
class AddNexusDevices < ActiveRecord::Migration
  def up
    Testrun.create!(name: 'Nexus 5', type: TestrunType.emulator_android_4_3, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 5', type: TestrunType.emulator_android_4_4, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 5', type: TestrunType.emulator_android_5_0, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 5', type: TestrunType.emulator_android_5_1, jenkins_name: nil)

    Testrun.create!(name: 'Nexus 7', type: TestrunType.emulator_android_4_3, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 7', type: TestrunType.emulator_android_4_4, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 7', type: TestrunType.emulator_android_5_0, jenkins_name: nil)
    Testrun.create!(name: 'Nexus 7', type: TestrunType.emulator_android_5_1, jenkins_name: nil)
  end
end
