# -*- encoding : utf-8 -*-
class UpdateStep18 < ActiveRecord::Migration
  def up
    step = AppstoreCheck.step_18
    step.expected = "%{executable} (architecture armv7):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\n   MH_MAGIC     ARM         V7  0x00     EXECUTE    51       5504   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n%{executable} (architecture arm64):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\nMH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    51       6208   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n"
    step.save!
  end
end
