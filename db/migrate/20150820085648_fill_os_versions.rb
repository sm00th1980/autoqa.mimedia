# -*- encoding : utf-8 -*-
class FillOsVersions < ActiveRecord::Migration
  def up
    OsVersion.create!(name: 'iOS 7.1')
    OsVersion.create!(name: 'iOS 8.3')
  end

  def down
    OsVersion.delete_all
  end
end
