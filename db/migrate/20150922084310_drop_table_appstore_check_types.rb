# -*- encoding : utf-8 -*-
class DropTableAppstoreCheckTypes < ActiveRecord::Migration
  def up
    drop_table :appstore_check_types
  end
end
