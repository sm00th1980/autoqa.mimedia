# -*- encoding : utf-8 -*-
class FixNode1 < ActiveRecord::Migration
  def up
    node1 = Infrastructure.find_by(name: 'node1')
    node1.url = "http://192.168.25.7:4723"
    node1.success_result = "That URL did not map to a valid JSONWP resource"
    node1.save!
  end
end
