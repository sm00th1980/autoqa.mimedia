# -*- encoding : utf-8 -*-
class AddDisabledIntoTestruns < ActiveRecord::Migration
  def change
    add_column :testruns, :disabled, :boolean, default: false, null: false
  end
end
