# -*- encoding : utf-8 -*-
class DropUnusedPendingAndBrokenFromTestcases < ActiveRecord::Migration
  def up
    remove_column :testcases, :pending
    remove_column :testcases, :infrastructure_problem
  end
end
