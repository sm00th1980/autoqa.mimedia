# -*- encoding : utf-8 -*-
class DisableAndroidTestruns < ActiveRecord::Migration
  def up
    Testrun.where('id >= 11').find_each do |testrun|
      testrun.disabled = true
      testrun.save!
    end
  end
end
