# -*- encoding : utf-8 -*-
class AddApiDevIntoInfrastructure < ActiveRecord::Migration
  def up
    Infrastructure.create!(name: 'api.dev.mimedia.com', url: 'https://api.dev.mimedia.com/2.0', success_result: {"session_expired":true}.to_json)
  end

  def down
    Infrastructure.find_by(name: 'api.dev.mimedia.com').delete
  end
end
