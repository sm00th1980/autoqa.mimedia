# -*- encoding : utf-8 -*-
class DropLastRunAtFromTestruns < ActiveRecord::Migration
  def up
    remove_column :testruns, :last_run_at
  end
end
