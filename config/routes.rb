# -*- encoding : utf-8 -*-
Rails.application.routes.draw do

  #api/v1
  get 'api/v1/restart_appium_for_simulator' => 'api/v1#restart_appium_for_simulator'
  get 'api/v1/restart_appium_for_mobile' => 'api/v1#restart_appium_for_mobile'
  get 'api/v1/restart_appium_for_tablet' => 'api/v1#restart_appium_for_tablet'

  get 'api/v1/job_finished' => 'api/v1#job_finished'

  post 'api/v1/job_unschedule' => 'api/v1#job_unschedule'

  #data
  get 'data/behavior.json' => 'data#behavior'
  get 'data/graph.json' => 'data#graph'
  get 'data/xunit.json' => 'data#xunit'
  get 'data/report.json' => 'data#report'
  get 'data/environment.json' => 'data#environment'
  get 'data/defects.json' => 'data#defects'
  get 'data/:id-testcase.json' => 'data#testcase'

  get 'testrun/:id' => 'testrun#index', as: :testrun
  post 'testrun/:id/schedule' => 'testrun#schedule', as: :testrun_schedule
  post 'testrun/:id/unschedule' => 'testrun#unschedule', as: :testrun_unschedule
  post 'testrun/:id/schedule_failed_or_pending' => 'testrun#schedule_failed_or_pending', as: :testrun_schedule_failed_or_pending

  get 'android' => 'android#index'
  get 'api' => 'api#index'

  get 'ios' => 'ios#index'
  post 'upload' => 'upload#upload'

  get 'messaging' => 'step#send_message'

  root :to => 'root#index'
end
