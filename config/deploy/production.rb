# -*- encoding : utf-8 -*-
server "192.168.25.38", user: 'mimedia-atest', port: 22, roles: %w{web app db}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
