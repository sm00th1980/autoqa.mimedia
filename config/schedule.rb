# -*- encoding : utf-8 -*-
# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

#poll jenkins for running job is still running
every 5.minutes do
  runner "Jenkins::Poller.perform_async"
end

#try to run scheduled jobs
every 1.minute do
  runner "Jenkins::Scheduler.run"
end

every 1.minute do
  runner "InfrastructurePoller.perform_async"
end

every 1.minute do
  runner "InfrastructureLoginPoller.perform_async"
end

every :day, at: '09:05pm' do
  runner "NightRun.perform"
end

every :day, at: '03:00pm' do
  runner "InfrastructureLog.clean"
  runner "Scenario.clean"
end

# every 1.hour do
#   runner "Build::Distributor.new.distribute"
# end
