# -*- encoding : utf-8 -*-
Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  config.serve_static_files = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like
  # NGINX, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.serve_static_files = true

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address => 'smtp.mercury.office',
      :port => 25,
      :authentication => 'plain',
      :user_name => 'atest-mimedia',
      :password => 'BG-Vrsik9_Ut',
      :enable_starttls_auto => false
  }

  config.middleware.use ExceptionNotification::Rack,
                        :email => {
                            :email_prefix => "[autoqa.mimedia] ",
                            :sender_address => %{"autoqa.mimedia notifier" <support@autoqa.mimedia>},
                            :exception_recipients => %w{deyarov@mercurydevelopment.com}
                        }

  config.data_fullpath = "/var/lib/jenkins/jobs/%s/workspace/ios/target/site/allure-maven-plugin/data"

  config.api = {
      host: '10.52.0.10',
      username: 'mercdev',
      start_path: '/var/lib/jenkins/jobs/API testing/builds',
      end_path: 'allure-report/data'
  }

  config.jenkins = {
      host: '192.168.25.38',
      port: 8080,
      username: 'deyarov',
      password: 'post@nuclear'
  }

  config.api_login = {
      host: 'https://api.dev.mimedia.com',
      session_postfix: '2.0/session',
      login_postfix: '2.0/accounts/login',
      api_key: 'Anybody seen Godot?',
      login: 'jane@mimedia.com',
      password: '123456'
  }

  config.hostname = 'http://192.168.25.38:80'

  config.git = {
      host: 'localhost',
      path: '/home/mimedia-atest/project',
      branch: 'night_testing'
  }

  config.mac = {
      app_name: 'MiMedia'
  }

end
