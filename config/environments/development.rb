# -*- encoding : utf-8 -*-
Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.action_mailer.default_url_options = {host: 'autoqa-mimedia.mercury.office', port: 80}
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address => 'smtp.mercury.office',
      :port => 25,
      :authentication => 'plain',
      :user_name => 'atest-mimedia',
      :password => 'BG-Vrsik9_Ut',
      :enable_starttls_auto => false
  }

  config.api = {
      host: '10.52.0.10',
      username: 'mercdev',
      start_path: '/var/lib/jenkins/jobs/API\ testing/builds',
      end_path: 'allure-report/data'
  }

  config.data_fullpath = "spec/fixtures"

  config.report_team = ['deyarov@mercurydevelopment.com']
  config.mail_sender = 'do-not-reply@autoqa-mimedia.mercury.office'

  config.jenkins = {
      host: '192.168.25.38',
      port: 8080,
      username: 'deyarov',
      password: 'post@nuclear'
  }

  config.api_login = {
      host: 'https://api.dev.mimedia.com',
      session_postfix: '2.0/session',
      login_postfix: '2.0/accounts/login',
      api_key: 'Anybody seen Godot?',
      login: 'jane@mimedia.com',
      password: '123456'
  }

  config.hostname = 'http://localhost:3000'

  config.git = {
      host: 'localhost',
      path: '/Users/deyarov/projects/Mobile_AutomatedTest',
      branch: 'night_testing'
  }

  config.mac = {
      app_name: 'MiMedia'
  }

end
