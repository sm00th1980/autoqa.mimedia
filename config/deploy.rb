# -*- encoding : utf-8 -*-
# config valid only for current version of Capistrano
lock '3.4.0'

app = 'autoqa.mimedia'

set :application, app
set :repo_url, 'git@bitbucket.org:sm00th1980/autoqa.mimedia.git'

set :deploy_to, "/srv/www/#{app}"

set :scm, :git
set :scm_passphrase, ""

set :stages, ["production"]
set :default_stage, "production"

set :unicorn_pid, "/srv/www/#{app}/shared/pids/unicorn.pid"
set :unicorn_config_path, "/srv/www/#{app}/current/config/unicorn.rb"

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do

    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do

    end
  end

end
