# -*- encoding : utf-8 -*-
require 'rails_helper'

describe UploadController, type: :controller do
  render_views

  describe "upload file" do
    before(:all) do
      @filename = 'MiMedia-iOS-AppStore-1.0.24-2015-09-15.zip'

      @params = {
          build: fixture_file_upload("appstore_checks/#{@filename}"),
          version: '1.0.24'
      }
    end

    it "should upload file with success" do
      assert_difference "StepFile.count" do
        post :upload, @params

        expect(response).to have_http_status(:success)

        file = assigns(:temp_filename)

        expect(file).to_not be_nil
        expect(File.exist?(file)).to be true

        step_file = StepFile.last
        expect(step_file.name).to eq file
        expect(step_file.version).to eq @params[:version]
        expect(step_file.original_name).to eq @filename

        expect(JSON.parse(response.body)).to eq ({"success" => true, "file_id" => step_file.id})

        FileUtils.rm(file)
      end
    end

  end

end
