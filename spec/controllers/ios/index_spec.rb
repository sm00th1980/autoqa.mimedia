# -*- encoding : utf-8 -*-
require 'rails_helper'

describe IosController, type: :controller do

  render_views

  describe "GET index" do
    before(:all) do
      create(:testrun_passed)
      @job = create(:running_jenkins_job)
    end

    it "should show index" do
      get :index
      expect(response).to have_http_status(:success)

      expect(assigns(:testruns_simulator_ios_8_x)).to eq TestrunType.simulator_ios_8_x.testruns.order(:sort).reject{|t| t.disabled?}
      expect(assigns(:testruns_simulator_ios_9_x)).to eq TestrunType.simulator_ios_9_x.testruns.order(:sort).reject{|t| t.disabled?}

      # expect(assigns(:testruns_device_ios_7_x)).to eq Testrun.where(type: TestrunType.device_ios_7_x)
      # expect(assigns(:testruns_device_ios_8_x)).to eq Testrun.where(type: TestrunType.device_ios_8_x)

      expect(assigns(:testruns_device_ios_8_x)).to eq []
      expect(assigns(:testruns_device_ios_9_x)).to eq []

      expect(assigns(:testruns_ios)).to eq Testrun.where(type: [TestrunType.simulator_ios_8_x, TestrunType.simulator_ios_9_x], disabled: false)

      expect(assigns(:jobs)).to eq JenkinsJob.where(status: [JobStatus.scheduled, JobStatus.running]).order(:created_at)
      expect(assigns(:jobs)).to eq [@job]
    end
  end

end
