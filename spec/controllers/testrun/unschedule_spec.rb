# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunController, type: :controller do

  render_views

  describe "unschedulling with success" do
    before(:all) do
      @scheduled_testrun = create(:scheduled_testrun)
      expect(@scheduled_testrun.scheduled?).to eq true
      expect(@scheduled_testrun.running?).to eq false
    end

    it "should unschedule testrun" do
      post :unschedule, id: @scheduled_testrun
      expect(assigns(:testrun)).to eq @scheduled_testrun

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('testrun.success.unschedulled')

      expect(@scheduled_testrun.reload.scheduled?).to eq false
    end
  end

  describe "unschedulling with failure" do
    before(:all) do
      JenkinsJob.delete_all
      @running_testrun = create(:running_testrun)
      expect(@running_testrun.scheduled?).to eq false
      expect(@running_testrun.running?).to eq true
    end

    it "should show error for running testrun" do
      post :unschedule, id: @running_testrun

      expect(assigns(:testrun)).to eq @running_testrun

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to eq I18n.t('testrun.failure.unschedulled')
      expect(flash[:notice]).to be_nil

      expect(@running_testrun.reload.scheduled?).to eq false
      expect(@running_testrun.reload.running?).to eq true
    end

    it "should show error for not exist testrun" do
      post :unschedule, id: -1

      expect(assigns(:testrun)).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to eq I18n.t('testrun.failure.not_found') % -1
      expect(flash[:notice]).to be_nil
    end
  end


end
