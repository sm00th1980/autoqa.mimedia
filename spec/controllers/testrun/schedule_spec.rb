# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunController, type: :controller do

  render_views

  describe "schedulling" do
    before(:all) do
      @testrun = create(:testrun)
    end

    it "should schedule testrun" do
      post :schedule, id: @testrun
      expect(assigns(:testrun)).to eq @testrun

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('testrun.success.schedulled')
    end

    it "should show error for not exist testrun" do
      post :schedule, id: -1

      expect(assigns(:testrun)).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to eq I18n.t('testrun.failure.not_found') % -1
      expect(flash[:notice]).to be_nil
    end

  end

end
