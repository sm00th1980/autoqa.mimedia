# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunController, type: :controller do

  render_views

  describe "schedulling" do
    before(:all) do
      @testrun = create(:testrun_failed)
      @scenario_ids = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.id }.sort

      expect(@testrun).to_not be_nil
      expect(@scenario_ids.empty?).to eq false
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
    end

    it "should schedule only failed or pending testrun" do
      post :schedule_failed_or_pending, id: @testrun
      expect(assigns(:testrun)).to eq @testrun

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('testrun.success.schedulled')

      expect(@testrun.reload.scheduled?).to eq true
      expect(@testrun.reload.running?).to eq false
    end

    it "should show error for not exist testrun" do
      assert_difference "JenkinsJob.count", 0 do
        post :schedule_failed_or_pending, id: -1

        expect(assigns(:testrun)).to be_nil

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(ios_path)

        expect(flash[:alert]).to eq I18n.t('testrun.failure.not_found') % -1
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
