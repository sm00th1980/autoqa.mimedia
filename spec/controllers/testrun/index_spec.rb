# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunController, type: :controller do

  render_views

  describe "GET index" do
    before(:all) do
      @testrun = create(:testrun)
    end

    it "should show index" do
      get :index, id: @testrun
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
    end

    it "should show error for not exist testrun" do
      get :index, id: -1
      expect(assigns(:testrun)).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)

      expect(flash[:alert]).to eq I18n.t('testrun.failure.not_found') % -1
      expect(flash[:notice]).to be_nil
    end

  end

end
