# -*- encoding : utf-8 -*-
require 'rails_helper'

RSpec.describe RootController, type: :controller do

  render_views

  describe "GET index" do
    it "should redirect to ios#index" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(ios_path)
    end
  end

end
