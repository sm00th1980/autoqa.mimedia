# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Api::V1Controller, type: :controller do

  render_views

  describe "GET restart_appium_for_simulator" do
    before(:all) do
      @running_job = create(:running_jenkins_job)
      check_running_job(@running_job)

      @scheduled_job = create(:jenkins_job)
      check_scheduled_job(@scheduled_job)
    end

    it "should show restart_appium_for_simulator" do
      get :restart_appium_for_simulator, {id: @running_job.id}
      expect(response).to have_http_status(:success)

      node = Node.find_by(id: @running_job.node_id)

      check_response_with_message(response, true, I18n.t('job.success.appium_restarted') % node.name)
    end

    it "should not restart appium with unknown job_id" do
      id = '-1'

      get :restart_appium_for_simulator, {id: id}
      expect(response).to have_http_status(:success)
      check_response_with_message(response, false, I18n.t('job.failure.not_found') % id)
    end

    it "should not restart appium with not running job" do
      get :restart_appium_for_simulator, {id: @scheduled_job.id}
      expect(response).to have_http_status(:success)
      check_response_with_message(response, false, I18n.t('job.failure.not_running'))
    end
  end

  private
  def check_response_with_message(response, result, message)
    expect(response).to have_http_status(:success)
    json = JSON.parse(response.body)
    expect(json['success']).to eq result
    expect(json['message']).to eq message
  end

end
