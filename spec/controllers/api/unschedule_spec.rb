# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Api::V1Controller, type: :controller do

  render_views

  describe "unschedule job with success" do
    before(:all) do
      @job = create(:jenkins_job)
      check_scheduled_job(@job)
    end

    it "should unschedule job with success" do
      assert_difference "JenkinsJob.count", -1 do
        post :job_unschedule, {id: @job.id}
        check_response_with_message(response, true, I18n.t('job.success.unschedulled'))
        expect(JenkinsJob.exists?(id: @job.id)).to eq false
      end

    end

    it "should not unschedule unknown job_id" do
      id = '-1'
      assert_difference "JenkinsJob.count", 0 do
        post :job_unschedule, {id: id}
        check_response_with_message(response, false, I18n.t('job.failure.not_found') % id)
      end
    end
  end

  describe "unschedule job with fail" do
    before(:all) do
      @job = create(:running_jenkins_job)
      check_running_job(@job)
    end

    it "should not unschedule running job" do
      assert_difference "JenkinsJob.count", 0 do
        post :job_unschedule, {id: @job.id}
        check_response_with_message(response, false, I18n.t('job.failure.not_scheduled'))
        check_running_job(@job)
      end
    end
  end

  describe "unschedule job with fail" do
    before(:all) do
      @job = create(:jenkins_job_finished_with_success)
      check_finished_with_success_job(@job)
    end

    it "should not unschedule finished with success job" do
      assert_difference "JenkinsJob.count", 0 do
        post :job_unschedule, {id: @job.id}
        check_response_with_message(response, false, I18n.t('job.failure.not_scheduled'))
        check_finished_with_success_job(@job)
      end
    end
  end

  describe "unschedule job with fail" do
    before(:all) do
      @job = create(:jenkins_job_finished_with_fail)
      check_finished_with_fail_job(@job)
    end

    it "should not unschedule finished with fail job" do
      assert_difference "JenkinsJob.count", 0 do
        post :job_unschedule, {id: @job.id}
        check_response_with_message(response, false, I18n.t('job.failure.not_scheduled'))
        check_finished_with_fail_job(@job)
      end
    end
  end

  private
  def check_response_with_message(response, result, message)
    expect(response).to have_http_status(:success)
    json = JSON.parse(response.body)
    expect(json['success']).to eq result
    expect(json['message']).to eq message
  end

end
