# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Api::V1Controller, type: :controller do

  render_views

  describe "GET specific scenario_finished" do
    before(:all) do
      @job = create(:running_jenkins_job_with_failed_testrun)
      check_running_job(@job)
      expect(@job.scenario_id).to_not be_nil
      @manager = Jenkins::Manager.new(@job, @job.current_job_name)

      @scheduled_job = create(:jenkins_job)
      check_scheduled_job(@scheduled_job)
    end

    it "should show start collect data for finished job" do
      stubbing(@manager.current_job_name, @manager.config) do
        Sidekiq::Testing.inline! do
          get :job_finished, {id: @job.id}
          expect(response).to have_http_status(:success)
          expect(assigns(:job)).to eq @job

          check_response_with_message(response, true, "starting collect data for testrun:<#{@job.testrun.name}>")

          expect(JenkinsJob.exists?(id: @job.id)).to eq true
          expect(@job.reload.status).to eq JobStatus.finished_with_success
          expect(@job.reload.stop).to_not be_nil
        end
      end
    end

    it "should not start collect data for not existing testrun id" do
      id = '-1'

      get :job_finished, {id: id}
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to be_nil

      check_response_with_message(response, false, I18n.t('job.failure.not_found') % id)
    end

    it "should not start collect data for not running job" do
      get :restart_appium_for_simulator, {id: @scheduled_job.id}
      expect(response).to have_http_status(:success)
      check_response_with_message(response, false, I18n.t('job.failure.not_running'))
    end
  end

  private
  def check_response_with_message(response, result, message)
    expect(response).to have_http_status(:success)
    json = JSON.parse(response.body)
    expect(json['success']).to eq result
    expect(json['message']).to eq message
  end
end
