# -*- encoding : utf-8 -*-
require 'rails_helper'

describe DataController, type: :controller do

  render_views

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_without_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.environment_view).to_not be_nil
    end

    it "should show environment" do
      get :environment
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.environment_view
    end
  end

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_without_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.report_view).to_not be_nil
    end

    it "should show report" do
      get :report
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.report_view
    end
  end

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_without_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.xunit_view).to_not be_nil
    end

    it "should show xunit" do
      get :xunit
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.xunit_view
    end
  end

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_with_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.defects_view).to_not be_nil
    end

    it "should show defects" do
      get :defects
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.defects_view
    end
  end

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_without_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.graph_view).to_not be_nil
    end

    it "should show graph" do
      get :graph
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.graph_view
    end
  end

  describe "GET json environment" do
    before(:each) do
      @testrun = create(:testrun_without_defects)
      expect(@testrun).to_not be_nil
      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
      expect(@testrun.behavior_view).to_not be_nil
    end

    it "should show behavior" do
      get :behavior
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to eq @testrun
      expect(from_json(response.body)).to eq @testrun.behavior_view
    end
  end

  describe "GET json data for invalid request" do
    before(:each) do
      expect(controller.request).to receive(:referer).and_return("vfmdkvnfnvjdf")
    end

    it "should show environment" do
      get :environment
      expect(response).to have_http_status(:success)
      expect(assigns(:testrun)).to be_nil

      expect(from_json(response.body)["success"]).to eq false
      expect(from_json(response.body)["error"]).to eq I18n.t('testrun.not_found')
    end
  end

  describe "GET json data for testcase" do
    before(:each) do
      @testrun = create(:testrun_with_testcases)
      expect(@testrun).to_not be_nil

      @testcase = @testrun.scenarios.sample.testcases.sample
      expect(@testcase).to_not be_nil

      expect(controller.request).to receive(:referer).and_return("http://jenkins/testrun/#{@testrun.id}")
    end

    it "should show testcase data for exists allure_id" do
      get :testcase, id: @testcase.allure_id
      expect(response).to have_http_status(:success)
      expect(assigns(:testcase)).to eq @testcase
      expect(from_json(response.body)).to eq @testcase.data
    end

    it "should show error for not exists allure_id" do
      get :testcase, id: -1
      expect(response).to have_http_status(:success)
      expect(assigns(:testcase)).to be_nil

      expect(from_json(response.body)["success"]).to eq false
      expect(from_json(response.body)["error"]).to eq I18n.t('testcase.not_found')
    end
  end

  def from_json(response)
    JSON.load(response)
  end

end
