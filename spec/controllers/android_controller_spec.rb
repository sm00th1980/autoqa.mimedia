# -*- encoding : utf-8 -*-
require 'rails_helper'

RSpec.describe AndroidController, type: :controller do

  render_views

  describe "GET index" do
    it "should show index" do
      get :index
      expect(response).to have_http_status(:success)

      expect(assigns(:testruns_emulator_android_4_3)).to eq Testrun.where(type: TestrunType.emulator_android_4_3)
      expect(assigns(:testruns_emulator_android_4_4)).to eq Testrun.where(type: TestrunType.emulator_android_4_4)
      expect(assigns(:testruns_emulator_android_5_0)).to eq Testrun.where(type: TestrunType.emulator_android_5_0)
      expect(assigns(:testruns_emulator_android_5_1)).to eq Testrun.where(type: TestrunType.emulator_android_5_1)
    end
  end

end
