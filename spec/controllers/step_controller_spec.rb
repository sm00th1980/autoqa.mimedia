# -*- encoding : utf-8 -*-
require 'rails_helper'

describe StepController, type: :controller do

  render_views

  describe "GET check steps" do
    before(:all) do
      @step_file = create(:step_file, version: '1.0.27', name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.zip').to_s, original_name: 'MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.zip')
      expect(@step_file).to_not be_nil
    end

    it "should show checking steps" do
      get :send_message, {id: @step_file.id}
      expect(response).to have_http_status(:success)
      expect(response.headers['Content-Type']).to eq 'text/event-stream'
      expect(response.stream.closed?).to be true
      expect(parse(response.body)).to eq result
    end

    private
    def result
      step_1_expected = AppstoreCheck.step_1.read_attribute_before_type_cast('expected') % {app: AppstoreCheck.app_path}
      step_1_got = AppstoreCheck.step_1.read_attribute_before_type_cast('expected') % {app: AppstoreCheck.app_path}
      step_1_command = AppstoreCheck.step_1.read_attribute_before_type_cast('detail') % {app: AppstoreCheck.app_path}

      step_3_expected = AppstoreCheck.step_3.read_attribute_before_type_cast('expected') % {path: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_3_got = AppstoreCheck.step_3.read_attribute_before_type_cast('expected') % {path: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_3_command = AppstoreCheck.step_3.read_attribute_before_type_cast('detail') % {path: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}

      step_4_expected = AppstoreCheck.step_4.read_attribute_before_type_cast('expected') % {provision: "#{AppstoreCheck.app_path}/embedded.mobileprovision"}
      step_4_got = AppstoreCheck.step_4.read_attribute_before_type_cast('expected') % {provision: "#{AppstoreCheck.app_path}/embedded.mobileprovision"}
      step_4_command = AppstoreCheck.step_4.read_attribute_before_type_cast('detail') % {provision: "#{AppstoreCheck.app_path}/embedded.mobileprovision"}

      step_5_expected = AppstoreCheck.step_5.read_attribute_before_type_cast('expected') % {app: AppstoreCheck.app_path}
      step_5_got = AppstoreCheck.step_5.read_attribute_before_type_cast('expected') % {app: AppstoreCheck.app_path}
      step_5_command = AppstoreCheck.step_5.read_attribute_before_type_cast('detail') % {app: AppstoreCheck.app_path}

      step_6_expected = AppstoreCheck.step_6.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}", version: @step_file.version}
      step_6_got = AppstoreCheck.step_6.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}", version: @step_file.version}
      step_6_command = AppstoreCheck.step_6.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}", version: @step_file.version}

      step_7_expected = AppstoreCheck.step_7.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_7_got = AppstoreCheck.step_7.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_7_command = AppstoreCheck.step_7.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}"}

      step_8_expected = AppstoreCheck.step_8.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_8_got = AppstoreCheck.step_8.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_8_command = AppstoreCheck.step_8.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}"}

      step_9_expected = AppstoreCheck.step_9.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_9_got = AppstoreCheck.step_9.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_9_command = AppstoreCheck.step_9.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}"}

      step_10_expected = AppstoreCheck.step_10.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_10_got = AppstoreCheck.step_10.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}"}
      step_10_command = AppstoreCheck.step_10.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}"}

      step_11_expected = AppstoreCheck.step_11.read_attribute_before_type_cast('expected')
      step_11_got = AppstoreCheck.step_11.read_attribute_before_type_cast('expected')
      step_11_command = AppstoreCheck.step_11.read_attribute_before_type_cast('detail')

      step_12_expected = AppstoreCheck.step_12.read_attribute_before_type_cast('expected')
      step_12_got = AppstoreCheck.step_12.read_attribute_before_type_cast('expected')
      step_12_command = AppstoreCheck.step_12.read_attribute_before_type_cast('detail')

      step_13_expected = AppstoreCheck.step_13.read_attribute_before_type_cast('expected')
      step_13_got = AppstoreCheck.step_13.read_attribute_before_type_cast('expected')
      step_13_command = AppstoreCheck.step_13.read_attribute_before_type_cast('detail')

      step_14_expected = AppstoreCheck.step_14.read_attribute_before_type_cast('expected')
      step_14_got = AppstoreCheck.step_14.read_attribute_before_type_cast('expected')
      step_14_command = AppstoreCheck.step_14.read_attribute_before_type_cast('detail')

      step_15_expected = AppstoreCheck.step_15.read_attribute_before_type_cast('expected')
      step_15_got = AppstoreCheck.step_15.read_attribute_before_type_cast('expected')
      step_15_command = AppstoreCheck.step_15.read_attribute_before_type_cast('detail')

      step_16_expected = AppstoreCheck.step_16.read_attribute_before_type_cast('expected')
      step_16_got = AppstoreCheck.step_16.read_attribute_before_type_cast('expected')
      step_16_command = AppstoreCheck.step_16.read_attribute_before_type_cast('detail')

      step_18_expected = AppstoreCheck.step_18.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_18_got = AppstoreCheck.step_18.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_18_command = AppstoreCheck.step_18.read_attribute_before_type_cast('detail') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}

      step_19_expected = AppstoreCheck.step_19.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_19_got = AppstoreCheck.step_19.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_19_command = AppstoreCheck.step_19.read_attribute_before_type_cast('detail') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}

      step_20_expected = AppstoreCheck.step_20.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_20_got = AppstoreCheck.step_20.read_attribute_before_type_cast('expected') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
      step_20_command = AppstoreCheck.step_20.read_attribute_before_type_cast('detail') % {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}

      step_21_expected = AppstoreCheck.step_21.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}", short_version: @step_file.short_version}
      step_21_got = AppstoreCheck.step_21.read_attribute_before_type_cast('expected') % {app: "#{AppstoreCheck.app_path}", short_version: @step_file.short_version}
      step_21_command = AppstoreCheck.step_21.read_attribute_before_type_cast('detail') % {app: "#{AppstoreCheck.app_path}", short_version: @step_file.short_version}

      [
          {"id" => AppstoreCheck.prepare.id, "status" => "running", "name" => AppstoreCheck.prepare.name, "expected" => nil, "command" => "-", "got" => nil},
          {"id" => AppstoreCheck.prepare.id, "status" => "passed", "name" => AppstoreCheck.prepare.name, "expected" => "-", "command" => "-", "got" => "-"},
          {"id" => AppstoreCheck.step_1.id, "status" => "running", "name" => AppstoreCheck.step_1.name, "expected" => nil, "command" => step_1_command, "got" => nil},
          {"id" => AppstoreCheck.step_1.id, "status" => "passed", "name" => AppstoreCheck.step_1.name, "expected" => step_1_expected, "command" => step_1_command, "got" => step_1_got},
          {"id" => AppstoreCheck.step_3.id, "status" => "running", "name" => AppstoreCheck.step_3.name, "expected" => nil, "command" => step_3_command, "got" => nil},
          {"id" => AppstoreCheck.step_3.id, "status" => "passed", "name" => AppstoreCheck.step_3.name, "expected" => step_3_expected, "command" => step_3_command, "got" => step_3_got},
          {"id" => AppstoreCheck.step_4.id, "status" => "running", "name" => AppstoreCheck.step_4.name, "expected" => nil, "command" => step_4_command, "got" => nil},
          {"id" => AppstoreCheck.step_4.id, "status" => "passed", "name" => AppstoreCheck.step_4.name, "expected" => step_4_expected, "command" => step_4_command, "got" => step_4_got},
          {"id" => AppstoreCheck.step_5.id, "status" => "running", "name" => AppstoreCheck.step_5.name, "expected" => nil, "command" => step_5_command, "got" => nil},
          {"id" => AppstoreCheck.step_5.id, "status" => "passed", "name" => AppstoreCheck.step_5.name, "expected" => step_5_expected, "command" => step_5_command, "got" => step_5_got},
          {"id" => AppstoreCheck.step_6.id, "status" => "running", "name" => AppstoreCheck.step_6.name, "expected" => nil, "command" => step_6_command, "got" => nil},
          {"id" => AppstoreCheck.step_6.id, "status" => "passed", "name" => AppstoreCheck.step_6.name, "expected" => step_6_expected, "command" => step_6_command, "got" => step_6_got},
          {"id" => AppstoreCheck.step_7.id, "status" => "running", "name" => AppstoreCheck.step_7.name, "expected" => nil, "command" => step_7_command, "got" => nil},
          {"id" => AppstoreCheck.step_7.id, "status" => "passed", "name" => AppstoreCheck.step_7.name, "expected" => step_7_expected, "command" => step_7_command, "got" => step_7_got},
          {"id" => AppstoreCheck.step_8.id, "status" => "running", "name" => AppstoreCheck.step_8.name, "expected" => nil, "command" => step_8_command, "got" => nil},
          {"id" => AppstoreCheck.step_8.id, "status" => "passed", "name" => AppstoreCheck.step_8.name, "expected" => step_8_expected, "command" => step_8_command, "got" => step_8_got},
          {"id" => AppstoreCheck.step_9.id, "status" => "running", "name" => AppstoreCheck.step_9.name, "expected" => nil, "command" => step_9_command, "got" => nil},
          {"id" => AppstoreCheck.step_9.id, "status" => "passed", "name" => AppstoreCheck.step_9.name, "expected" => step_9_expected, "command" => step_9_command, "got" => step_9_got},
          {"id" => AppstoreCheck.step_10.id, "status" => "running", "name" => AppstoreCheck.step_10.name, "expected" => nil, "command" => step_10_command, "got" => nil},
          {"id" => AppstoreCheck.step_10.id, "status" => "passed", "name" => AppstoreCheck.step_10.name, "expected" => step_10_expected, "command" => step_10_command, "got" => step_10_got},
          {"id" => AppstoreCheck.step_11.id, "status" => "running", "name" => AppstoreCheck.step_11.name, "expected" => nil, "command" => step_11_command, "got" => nil},
          {"id" => AppstoreCheck.step_11.id, "status" => "passed", "name" => AppstoreCheck.step_11.name, "expected" => step_11_expected, "command" => step_11_command, "got" => step_11_got},
          {"id" => AppstoreCheck.step_12.id, "status" => "running", "name" => AppstoreCheck.step_12.name, "expected" => nil, "command" => step_12_command, "got" => nil},
          {"id" => AppstoreCheck.step_12.id, "status" => "passed", "name" => AppstoreCheck.step_12.name, "expected" => step_12_expected, "command" => step_12_command, "got" => step_12_got},
          {"id" => AppstoreCheck.step_13.id, "status" => "running", "name" => AppstoreCheck.step_13.name, "expected" => nil, "command" => step_13_command, "got" => nil},
          {"id" => AppstoreCheck.step_13.id, "status" => "passed", "name" => AppstoreCheck.step_13.name, "expected" => step_13_expected, "command" => step_13_command, "got" => step_13_got},
          {"id" => AppstoreCheck.step_14.id, "status" => "running", "name" => AppstoreCheck.step_14.name, "expected" => nil, "command" => step_14_command, "got" => nil},
          {"id" => AppstoreCheck.step_14.id, "status" => "passed", "name" => AppstoreCheck.step_14.name, "expected" => step_14_expected, "command" => step_14_command, "got" => step_14_got},
          {"id" => AppstoreCheck.step_15.id, "status" => "running", "name" => AppstoreCheck.step_15.name, "expected" => nil, "command" => step_15_command, "got" => nil},
          {"id" => AppstoreCheck.step_15.id, "status" => "passed", "name" => AppstoreCheck.step_15.name, "expected" => step_15_expected, "command" => step_15_command, "got" => step_15_got},
          {"id" => AppstoreCheck.step_16.id, "status" => "running", "name" => AppstoreCheck.step_16.name, "expected" => nil, "command" => step_16_command, "got" => nil},
          {"id" => AppstoreCheck.step_16.id, "status" => "passed", "name" => AppstoreCheck.step_16.name, "expected" => step_16_expected, "command" => step_16_command, "got" => step_16_got},
          {"id" => AppstoreCheck.step_18.id, "status" => "running", "name" => AppstoreCheck.step_18.name, "expected" => nil, "command" => step_18_command, "got" => nil},
          {"id" => AppstoreCheck.step_18.id, "status" => "passed", "name" => AppstoreCheck.step_18.name, "expected" => step_18_expected, "command" => step_18_command, "got" => step_18_got},
          {"id" => AppstoreCheck.step_19.id, "status" => "running", "name" => AppstoreCheck.step_19.name, "expected" => nil, "command" => step_19_command, "got" => nil},
          {"id" => AppstoreCheck.step_19.id, "status" => "passed", "name" => AppstoreCheck.step_19.name, "expected" => step_19_expected, "command" => step_19_command, "got" => step_19_got},
          {"id" => AppstoreCheck.step_20.id, "status" => "running", "name" => AppstoreCheck.step_20.name, "expected" => nil, "command" => step_20_command, "got" => nil},
          {"id" => AppstoreCheck.step_20.id, "status" => "passed", "name" => AppstoreCheck.step_20.name, "expected" => step_20_expected, "command" => step_20_command, "got" => step_20_got},
          {"id" => AppstoreCheck.step_21.id, "status" => "running", "name" => AppstoreCheck.step_21.name, "expected" => nil, "command" => step_21_command, "got" => nil},
          {"id" => AppstoreCheck.step_21.id, "status" => "passed", "name" => AppstoreCheck.step_21.name, "expected" => step_21_expected, "command" => step_21_command, "got" => step_21_got},
          {"finished" => true}
      ]
    end

    def parse(text)
      text.split('data: ').select { |el| el.present? }.map { |el| JSON.parse(el) }
    end


  end

  describe "GET check steps" do
    it "should show error if not existing id" do
      get :send_message, {id: -1}
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)).to eq ({"success" => false, "error" => (I18n.t('step_file.failure.not_found') % -1)})
    end
  end

end
