# -*- encoding : utf-8 -*-
require 'rails_helper'

describe ApiController, type: :controller do

  render_views

  describe "GET index" do
    it "should show index" do
      get :index
      expect(response).to have_http_status(:success)

      expect(assigns(:testruns_api)).to eq Testrun.where(type: TestrunType.api)
    end
  end

end
