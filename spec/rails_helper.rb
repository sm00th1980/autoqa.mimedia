# -*- encoding : utf-8 -*-
# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'sidekiq/testing'
require 'webmock/rspec'

# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

include ActionDispatch::TestProcess

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  config.include Rails.application.routes.url_helpers
  config.include FactoryGirl::Syntax::Methods
  config.include AssertDifference

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  config.before(:all) do
    Node.delete_all
    JobStatus.delete_all
    AppstoreCheck.delete_all
    TestcaseStatus.delete_all
    TestrunType.delete_all
    Testrun.delete_all
    Testcase.delete_all
    InfrastructureLog.delete_all
    Infrastructure.delete_all
    Scenario.delete_all
    JenkinsJob.delete_all

    create(:node)
    create(:job_status_scheduled)
    create(:job_status_running)
    create(:job_status_finished_with_success)
    create(:job_status_finished_with_fail)

    create(:testcase_status_passed)
    create(:testcase_status_pending)
    create(:testcase_status_broken)
    create(:testcase_status_unknown)
    create(:testcase_status_broken_by_infrastructure)

    create(:infrastructure_api)
    create(:infrastructure_api_login)
    create(:infrastructure_node1)

    create(:testrun_type_api)

    create(:testrun_type_simulator_ios_8_x)
    create(:testrun_type_simulator_ios_9_x)

    create(:testrun_type_device_ios_8_x)
    create(:testrun_type_device_ios_9_x)

    create(:testrun_type_emulator_android_4_3)
    create(:testrun_type_emulator_android_4_4)
    create(:testrun_type_emulator_android_5_0)
    create(:testrun_type_emulator_android_5_1)

    create(:appstore_check_prepare)
    create(:appstore_check_step_1)
    create(:appstore_check_step_2)
    create(:appstore_check_step_3)
    create(:appstore_check_step_4)
    create(:appstore_check_step_5)
    create(:appstore_check_step_6)
    create(:appstore_check_step_7)
    create(:appstore_check_step_8)
    create(:appstore_check_step_9)
    create(:appstore_check_step_10)
    create(:appstore_check_step_11)
    create(:appstore_check_step_12)
    create(:appstore_check_step_13)
    create(:appstore_check_step_14)
    create(:appstore_check_step_15)
    create(:appstore_check_step_16)
    create(:appstore_check_step_17)
    create(:appstore_check_step_18)
    create(:appstore_check_step_19)
    create(:appstore_check_step_20)
    create(:appstore_check_step_21)

    10.times { create(:testrun) }

  end
end

def stubbing(current_job_name, config, &block)
  stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/api/json?tree=useCrumbs").
      with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
      to_return(status: 200, body: {"useCrumbs" => false}.to_json, headers: {})

  #delete
  stub_request(:post, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{current_job_name}/doDelete").
      with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type' => 'application/x-www-form-urlencoded', 'User-Agent' => 'Ruby'}).
      to_return(:status => 200, :body => "", :headers => {})

  #create
  stub_request(:post, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/createItem?name=#{current_job_name}").
      with(:body => config, :headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type' => 'application/xml;charset=UTF-8', 'User-Agent' => 'Ruby'}).
      to_return(:status => 200, :body => "", :headers => {})

  #build
  stub_request(:post, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{current_job_name}/build").
      with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type' => 'application/x-www-form-urlencoded', 'User-Agent' => 'Ruby'}).
      to_return(:status => 200, :body => "", :headers => {})

  yield if block_given?
end

def check_running_job(job)
  job = job.reload

  expect(job).to_not be_nil
  expect(job.status.running?).to be true
  expect(job.current_job_name).to_not be_nil
  expect(job.start).to_not be_nil
  expect(job.stop).to be_nil
  expect(job.node_id).to_not be_nil
end

def check_scheduled_job(job)
  job = job.reload
  expect(job).to_not be_nil
  expect(job.status.scheduled?).to be true
  expect(job.current_job_name).to be_nil
  expect(job.start).to be_nil
  expect(job.stop).to be_nil
  expect(job.node_id).to be_nil
end

def check_finished_with_success_job(job)
  job = job.reload
  expect(job).to_not be_nil
  expect(job.status.finished_with_success?).to be true
  expect(job.current_job_name).to_not be_nil
  expect(job.start).to_not be_nil
  expect(job.stop).to_not be_nil
  expect(job.node_id).to_not be_nil
end

def check_finished_with_fail_job(job)
  job = job.reload
  expect(job).to_not be_nil
  expect(job.status.finished_with_fail?).to be true
  expect(job.current_job_name).to_not be_nil
  expect(job.start).to_not be_nil
  expect(job.stop).to_not be_nil
  expect(job.node_id).to_not be_nil
end

def freeze_time(time=Time.now, &block)
  Timecop.freeze(time)
  yield if block_given?
  Timecop.return
end
