# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Steps::Step_2 do

  describe "check Steps::Step_2 with success" do
    before(:all) do
      @node = Node.appstore_node
      @step_file = create(:step_file, name: Rails.root.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.zip'), original_name: 'MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.zip')
      expect(@step_file).to_not be_nil

      clean_remote_dir
      Steps::Prepare.new(nil, @step_file).run
    end

    after(:all) do
      clean_remote_dir
    end

    it "should check build for step_2 with success" do
      content = SshEndpoint.execute("ls #{@node.upload_dir}", @node)
      expect(content).to_not eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
      expect(content.split.empty?).to eq false

      expect do
        step = Steps::Step_2.new(AppstoreCheck.step_2, @step_file)
        step.run
        expect(step.expected == step.got).to eq true
        expect(step.result).to eq true
      end.not_to raise_error
    end

    private
    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{@node.upload_dir}", @node)
    end

  end

  # describe "check Steps::Step_2 with failed" do
  #   before(:all) do
  #     @node = Node.appstore_node
  #     @step_file = create(:step_file, name: File.join('spec', 'fixtures', 'appstore_checks', 'MiMedia-iOS-AppStore-1.0.21-2015-08-12.zip'))
  #     expect(@step_file).to_not be_nil
  #
  #     clean_remote_dir
  #     Steps::Prepare.new(nil, @step_file).run
  #   end
  #
  #   after(:all) do
  #     clean_remote_dir
  #   end
  #
  #   it "should check build for step_2 with success" do
  #     content = SshEndpoint.execute("ls #{@node.upload_dir}", @node)
  #     expect(content).to_not eq ("ls: %s: No such file or directory\n" % @node.upload_dir)
  #     expect(content.split.empty?).to eq false
  #
  #     expect do
  #       step = Steps::Step_2.new(AppstoreCheck.step_2, @step_file)
  #       step.run
  #
  #       expect(step.expected == step.got).to eq false
  #       expect(step.result).to eq false
  #     end.not_to raise_error
  #   end
  #
  #   private
  #   def clean_remote_dir
  #     SshEndpoint.execute("rm -rf #{@node.upload_dir}", @node)
  #   end
  #
  # end

end
