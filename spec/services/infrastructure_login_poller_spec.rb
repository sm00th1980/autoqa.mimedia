# -*- encoding : utf-8 -*-
require 'rails_helper'

describe InfrastructureLoginPoller do

  describe "poll success login infrastructure" do
    before(:all) do
      @infrastructure = Infrastructure.api_login

      stub_request(:post, "#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:session_postfix]}").
          to_return(status: 200, body: session.to_json, headers: {})

      stub_request(:post, "#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:login_postfix]}").
          to_return(status: 200, body: login.to_json, headers: {})
    end

    it "should poll success login infrastructure" do

      Sidekiq::Testing.inline! do
        assert_difference "InfrastructureLog.count" do
          InfrastructureLoginPoller.perform_async

          log = InfrastructureLog.last
          expect(log.infrastructure).to eq @infrastructure
          expect(log.available).to be true
          expect(log.datetime).to be > (Time.now - 5.seconds)
        end
      end
    end

    private
    def session
      {"session" => {"apiKey" => 1, "createdOn" => "2015-09-07T10:15:17.415Z", "accessedOn" => "2015-09-07T10:15:17.415Z", "modifiedOn" => "2015-09-07T10:15:17.415Z", "expiresOn" => "2015-09-08T10:15:17.415Z", "renewedOn" => nil, "accountId" => nil, "version" => 1, "storageAuthToken" => nil, "tttl" => 540000, "id" => "-47581810525950959294704116940519568986"}}
    end

    def login
      {"session" => {"apiKey" => 1, "createdOn" => "2015-09-07T10:16:15.929Z", "accessedOn" => "2015-09-07T10:16:15.929Z", "modifiedOn" => "2015-09-07T10:16:15.929Z", "expiresOn" => "2015-09-08T10:16:16.302Z", "renewedOn" => nil, "accountId" => 880, "version" => 1, "storageAuthToken" => "", "tttl" => 540000, "id" => "44095411360982206649177293259630263904"}, "account" => {"id" => "880", "emailAddress" => Rails.configuration.api_login[:login]}}
    end
  end

  describe "poll failure login infrastructure" do
    before(:all) do
      @infrastructure = Infrastructure.api_login

      stub_request(:post, "#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:session_postfix]}").
          to_return(status: 200, body: {}.to_json, headers: {})

      stub_request(:post, "#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:login_postfix]}").
          to_return(status: 200, body: {}.to_json, headers: {})
    end

    it "should poll failure login infrastructure" do

      Sidekiq::Testing.inline! do
        assert_difference "InfrastructureLog.count" do
          InfrastructureLoginPoller.perform_async

          log = InfrastructureLog.last
          expect(log.infrastructure).to eq @infrastructure
          expect(log.available).to be false
          expect(log.datetime).to be > (Time.now - 5.seconds)
        end
      end
    end

  end

end
