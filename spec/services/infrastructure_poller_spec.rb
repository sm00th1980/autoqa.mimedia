# -*- encoding : utf-8 -*-
require 'rails_helper'

describe InfrastructurePoller do

  describe "poll available infrastructure" do
    before(:all) do
      @infrastructure= create(:infrastructure_api)

      stub_request(:get, @infrastructure.url).
          to_return(status: 401, body: @infrastructure.success_result, headers: {})
    end

    it "should poll infrastructure" do

      Sidekiq::Testing.inline! do
        assert_difference "InfrastructureLog.count" do
          InfrastructurePoller.perform_async([@infrastructure.id])

          log = InfrastructureLog.last
          expect(log.infrastructure).to eq @infrastructure
          expect(log.available).to be true
          expect(log.datetime).to be > (Time.now - 5.seconds)
        end
      end
    end
  end


end
