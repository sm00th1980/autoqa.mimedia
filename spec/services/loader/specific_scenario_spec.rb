# -*- encoding : utf-8 -*-
require 'rails_helper'

describe LoaderMobile do

  describe "perform files" do
    before(:all) do
      JenkinsJob.delete_all

      @testrun = create(:testrun_failed)

      expect(@testrun.start).to_not be_nil
      expect(@testrun.stop).to_not be_nil
      expect(@testrun.failed_scenarios_count).to be > 0

      @scenario = @testrun.scenarios.select { |s| s.failed? }.first
      expect(@scenario).to_not be_nil
      @start = @testrun.start
      @stop = @testrun.stop

      @job = create(:running_jenkins_job, testrun: @testrun, scenario_id: @scenario.id)
      check_running_job(@job)
      expect(@job.scenario).to eq @scenario

      @manager = Jenkins::Manager.new(@job, @job.current_job_name)
    end

    it "should load data into testruns" do
      stubbing(@manager.current_job_name, @manager.config) do
        Sidekiq::Testing.inline! do
          assert_difference "Scenario.count", 2 do
            LoaderMobile.perform_async(@job.id)

            #check stats
            expect(@testrun.reload.start).to eq @start #should not change
            expect(@testrun.reload.stop).to eq @stop #should not change
            expect(@testrun.reload.failed_scenarios_count).to eq 2
            expect(@testrun.reload.passed_scenarios_count).to eq 0
            expect(@testrun.reload.pending_scenarios_count).to eq 0

            #check job
            expect(JenkinsJob.exists?(id: @job.id)).to eq true
            expect(@job.reload.stop).to_not be_nil
            expect(@job.reload.status).to eq JobStatus.finished_with_success
          end
        end
      end
    end

  end

end
