# -*- encoding : utf-8 -*-
require 'rails_helper'

describe LoaderMobile do

  describe "perform files" do
    before(:all) do
      @testrun = create(:testrun_without_data)

      expect(@testrun.start).to be_nil
      expect(@testrun.stop).to be_nil
      expect(@testrun.failed_scenarios_count).to eq 0
      expect(@testrun.passed_scenarios_count).to eq 0
      expect(@testrun.pending_scenarios_count).to eq 0

      @job = create(:running_jenkins_job, testrun: @testrun)
      check_running_job(@job)
      expect(@job.scenario).to be_nil

      @manager = Jenkins::Manager.new(@job, @job.current_job_name)
    end

    it "should load data into testruns" do
      stubbing(@manager.current_job_name, @manager.config) do
        Sidekiq::Testing.inline! do
          assert_difference "Scenario.count", 2 do
            LoaderMobile.perform_async(@job.id)

            #check stats
            expect(@testrun.reload.start).to_not be_nil
            expect(@testrun.reload.stop).to_not be_nil
            expect(@testrun.reload.failed_scenarios_count).to eq 1
            expect(@testrun.reload.passed_scenarios_count).to eq 1
            expect(@testrun.reload.pending_scenarios_count).to eq 0

            #first scenario
            scenario1 = @testrun.reload.scenarios.first
            expect(scenario1).to_not be_nil
            expect(scenario1.name).to eq 'Pass allow photo screen'
            expect(scenario1.allure_id).to eq '53295e7dd85dd168'
            expect(scenario1.start).to eq DateTime.parse('2015-08-20 09:40:12').to_time
            expect(scenario1.stop).to eq DateTime.parse('2015-08-20 09:40:16').to_time
            expect(scenario1.job).to eq @job

            expect(scenario1.testcases.count).to eq 3

            #second scenario
            scenario2 = @testrun.reload.scenarios.last
            expect(scenario2).to_not be_nil
            expect(scenario2.name).to eq 'Pass Start page via login'
            expect(scenario2.allure_id).to eq '2581ffe6bd995c'
            expect(scenario2.start).to eq DateTime.parse('2015-08-20 09:40:43').to_time
            expect(scenario2.stop).to eq DateTime.parse('2015-08-20 09:40:44').to_time
            expect(scenario2.job).to eq @job

            expect(scenario2.testcases.count).to eq 3

            #check job
            expect(JenkinsJob.exists?(id: @job.id)).to eq true
            expect(@job.reload.stop).to_not be_nil
            expect(@job.reload.status).to eq JobStatus.finished_with_success
          end
        end
      end
    end

  end

end
