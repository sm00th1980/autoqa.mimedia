# -*- encoding : utf-8 -*-
require 'rails_helper'

describe NightRun do

  describe "check night time" do
    before(:all) do
      @day_time = Time.now.change(hour: NightRun::BEGIN_HOUR, min: NightRun::BEGIN_MIN, sec: NightRun::BEGIN_SEC) + 1.hour
      @night_time = Time.now.change(hour: NightRun::END_HOUR, min: NightRun::END_MIN, sec: NightRun::END_SEC) + 1.hour
    end

    it "should check correct night time" do
      freeze_time(@night_time) do
        expect(NightRun.night_time?).to eq true
      end
    end

    it "should check correct day time" do
      freeze_time(@day_time) do
        expect(NightRun.night_time?).to eq false
      end
    end

  end

end
