# -*- encoding : utf-8 -*-
require 'rails_helper'

describe NightRun do

  describe "scheduling job" do
    before(:all) do
      @night_time = Time.now.change(hour: NightRun::END_HOUR, min: NightRun::END_MIN, sec: NightRun::END_SEC) + 1.hour

      @testrun = create(:testrun_iphone_4s_8_4)
      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
      expect(@testrun.type.ios?).to eq true
      expect(@testrun.disabled?).to eq false
    end

    it "should schedule job at night time" do
      freeze_time(@night_time) do
        expect(NightRun.night_time?).to eq true

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq true
        expect(@testrun.reload.running?).to eq false
      end
    end

  end

end
