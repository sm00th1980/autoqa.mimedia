# -*- encoding : utf-8 -*-
require 'rails_helper'

describe NightRun do

  describe "not scheduling job ios at day" do
    before(:all) do
      @day_time = Time.now.change(hour: NightRun::BEGIN_HOUR, min: NightRun::BEGIN_MIN, sec: NightRun::BEGIN_SEC) + 1.hour

      @testrun = create(:testrun_iphone_4s_8_4)
      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
      expect(@testrun.type.ios?).to eq true
      expect(@testrun.disabled?).to eq false
    end

    it "should not schedule job at day time" do
      freeze_time(@day_time) do
        expect(NightRun.night_time?).to eq false

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

  end

  describe "not scheduling job not ios" do
    before(:all) do
      @day_time = Time.now.change(hour: NightRun::BEGIN_HOUR, min: NightRun::BEGIN_MIN, sec: NightRun::BEGIN_SEC) + 1.hour
      @night_time = Time.now.change(hour: NightRun::END_HOUR, min: NightRun::END_MIN, sec: NightRun::END_SEC) + 1.hour

      @testrun = create(:testrun_android_4_3)
      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
      expect(@testrun.type.ios?).to eq false
      expect(@testrun.disabled?).to eq false
    end

    it "should not schedule job at day time" do
      freeze_time(@day_time) do
        expect(NightRun.night_time?).to eq false

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

    it "should not schedule job at night time" do
      freeze_time(@night_time) do
        expect(NightRun.night_time?).to eq true

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

  end

  describe "not scheduling job for ios disabled" do
    before(:all) do
      @day_time = Time.now.change(hour: NightRun::BEGIN_HOUR, min: NightRun::BEGIN_MIN, sec: NightRun::BEGIN_SEC) + 1.hour
      @night_time = Time.now.change(hour: NightRun::END_HOUR, min: NightRun::END_MIN, sec: NightRun::END_SEC) + 1.hour

      @testrun = create(:testrun_iphone_4s_8_4, disabled: true)

      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
      expect(@testrun.type.ios?).to eq true
      expect(@testrun.disabled?).to eq true
    end

    it "should not schedule job disabled at day time" do
      freeze_time(@day_time) do
        expect(NightRun.night_time?).to eq false

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

    it "should not schedule job disabled at night time" do
      freeze_time(@night_time) do
        expect(NightRun.night_time?).to eq true

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

  end

  describe "not scheduling job for not ios disabled" do
    before(:all) do
      @day_time = Time.now.change(hour: NightRun::BEGIN_HOUR, min: NightRun::BEGIN_MIN, sec: NightRun::BEGIN_SEC) + 1.hour
      @night_time = Time.now.change(hour: NightRun::END_HOUR, min: NightRun::END_MIN, sec: NightRun::END_SEC) + 1.hour

      @testrun = create(:testrun_android_4_3, disabled: true)

      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq false
      expect(@testrun.type.ios?).to eq false
      expect(@testrun.disabled?).to eq true
    end

    it "should not schedule job disabled at day time" do
      freeze_time(@day_time) do
        expect(NightRun.night_time?).to eq false

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

    it "should not schedule job disabled at night time" do
      freeze_time(@night_time) do
        expect(NightRun.night_time?).to eq true

        NightRun.perform
        expect(@testrun.reload.scheduled?).to eq false
        expect(@testrun.reload.running?).to eq false
      end
    end

  end

end
