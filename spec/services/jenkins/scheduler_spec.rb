# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Jenkins::Scheduler do

  describe "run new jobs" do
    before(:all) do
      @job = create(:jenkins_job)
      check_scheduled_job(@job)

      @node = Node.free.first
      expect(@node).to_not be_nil
    end

    it "should run job" do
      manager = Jenkins::Manager.new(@job)

      stubbing(manager.current_job_name, manager.config(@node)) do
        Jenkins::Scheduler.run(manager.current_job_name)
        check_running_job(@job)
      end
    end

  end

end
