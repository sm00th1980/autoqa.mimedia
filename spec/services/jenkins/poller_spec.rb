# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Jenkins::Poller do

  describe "running jobs" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 200, body: File.read('spec/fixtures/jenkins/running.json'), headers: {})
    end

    it "should not touch running job" do
      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        check_running_job(@job)
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

  describe "jenkins downtime" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 500, body: '', headers: {})
    end

    it "should not touch running job if jenkins downtime" do
      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        check_running_job(@job)
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

  describe "unstable jobs" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 200, body: File.read('spec/fixtures/jenkins/unstable.json'), headers: {})
    end

    it "should finish unstable job" do

      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        expect(JenkinsJob.exists?(id: @job)).to eq true
        expect(@job.reload.stop).to_not be_nil
        expect(@job.reload.status).to eq JobStatus.finished_with_fail
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

  describe "failed jobs" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 200, body: File.read('spec/fixtures/jenkins/failed.json'), headers: {})
    end

    it "should finish failed job" do

      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        expect(JenkinsJob.exists?(id: @job)).to eq true
        expect(@job.reload.stop).to_not be_nil
        expect(@job.reload.status).to eq JobStatus.finished_with_fail
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

  describe "invalid jobs" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 200, body: '', headers: {})
    end

    it "should not touch running job if response is invalid" do
      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        check_running_job(@job)
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

  describe "poll not existing jobs" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)

      stub_request(:get, "http://#{Rails.configuration.jenkins[:username]}:#{Rails.configuration.jenkins[:password]}@#{Rails.configuration.jenkins[:host]}:8080/job/#{@job.current_job_name}/api/json").
          with(:headers => {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
          to_return(status: 404, body: '', headers: {})
    end

    it "should poll status into testruns " do
      Sidekiq::Testing.inline! do
        Jenkins::Poller.perform_async

        expect(JenkinsJob.exists?(id: @job)).to eq true
        expect(@job.reload.stop).to_not be_nil
        expect(@job.reload.status).to eq JobStatus.finished_with_fail
      end
    end

    after(:all) do
      JenkinsJob.delete_all
    end

  end

end
