# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Jenkins::Manager do

  describe "run new jobs" do
    before(:all) do
      JenkinsJob.delete_all

      @job = create(:running_jenkins_job)

      check_running_job(@job)
      expect(@job.scenario_id).to be_nil

      @manager = Jenkins::Manager.new(@job, @job.current_job_name)
    end

    it "should run job" do
      stubbing(@manager.current_job_name, @manager.config) do
        @manager.perform

        check_running_job(@job)
      end
    end

  end

  describe "config for job" do
    before(:all) do
      JenkinsJob.delete_all

      @job = create(:running_jenkins_job)
      check_running_job(@job)
      expect(@job.scenario_id).to be_nil
    end

    it "should generate correct xml config for job" do
      manager = Jenkins::Manager.new(@job)

      expect(manager.config).to_not be_nil
      expect(manager.config).to eq ERB.new(File.read('app/views/jenkins/config.xml.erb')).result(manager.get_binding)
    end
  end

end
