# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Build::Distributor do

  describe "build distributor - commands" do
    before(:all) do
      Node.delete_all
      @node = create(:node, home_dir: '/Users/deyarov')
      expect(@node).to_not be_nil
      expect(@node.home_dir).to eq '/Users/deyarov'

      @compilator = Build::Compilator.new
      expect(@compilator).to_not be_nil

      @distributor = Build::Distributor.new
      expect(@distributor).to_not be_nil
    end

    it "should has correct build_name" do
      expect(@distributor.build_name).to eq "#{Build::Compilator::APP}_i386.app.zip"
    end

    it "should has correct remote_path" do
      expect(@distributor.remote_path(@node.home_dir)).to eq "#{@node.home_dir}/projects/Mobile_AutomatedTest/ios/prebuilds/#{@distributor.build_name}"
    end

    it "should has correct local_path" do
      expect(@distributor.local_path).to eq Rails.root.join('tmp', @distributor.build_name).to_s
    end

  end

end
