# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Build::Compilator do

  describe "build compilator - commands" do
    before(:all) do
      Node.delete_all
      @node = create(:node, home_dir: '/Users/deyarov')
      expect(@node).to_not be_nil
      expect(@node.home_dir).to eq '/Users/deyarov'

      @compilator = Build::Compilator.new
      expect(@compilator).to_not be_nil
    end

    it "should has correct dir" do
      expect(@compilator.dir).to eq "#{@node.home_dir}/projects/iOSClient_V2"
    end

    it "should has correct build command" do
      project = "#{@compilator.dir}/#{Build::Compilator::APP}.xcodeproj"
      expect(@compilator.build).to eq "xcodebuild -project #{project} -configuration UITesting -target #{Build::Compilator::APP} -arch i386 -sdk iphonesimulator9.0"
    end

    it "should has correct clean_build_dir command" do
      expect(@compilator.clean_build_dir).to eq "rm -rf #{@compilator.dir}/build"
    end

    it "should has correct zip_remote command" do
      output = "#{@node.home_dir}/projects/Mobile_AutomatedTest/ios/prebuilds/#{Build::Compilator::APP}_i386.app.zip"
      input = "#{Build::Compilator::APP}.app"

      expect(@compilator.zip_remote).to eq "zip -r #{output} #{input}"
    end

    it "should has correct chdir_to_ios_build command" do
      expect(@compilator.chdir_to_ios_build).to eq "cd #{@compilator.dir}/build/UITesting-iphonesimulator"
    end

    it "should has correct chdir_to_ios_project command" do
      expect(@compilator.chdir_to_ios_project).to eq "cd #{@compilator.dir}"
    end

    it "should has correct git_update command" do
      expect(@compilator.git_update).to eq "git pull"
    end

  end

end
