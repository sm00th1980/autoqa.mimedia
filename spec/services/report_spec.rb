# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Report do

  describe "send email to report team" do

    it "should be set to be delivered to the email passed in" do
      Report.send_to_report_team
      expect(ActionMailer::Base.deliveries.empty?).to be false

      ActionMailer::Base.deliveries.each do |email|
        expect(email.from).to eq [Rails.configuration.mail_sender]
        expect(Rails.configuration.report_team.include?(email.to.first)).to be true
        expect(email.subject).to eq Report.subject
      end
    end

  end

end
