# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 2" do

    before(:all) do
      @step_2 = AppstoreCheck.step_2
    end

    it "should step_1 be step_1" do
      expect(@step_2).to_not be_nil
      expect(@step_2.internal_name).to eq 'step_2'
      expect(@step_2.klass).to eq 'Steps::Step_2'
      expect(@step_2.sort).to eq 2
    end

  end

end
