# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "streaming_checks" do

    before(:all) do
      expect(AppstoreCheck.prepare).to_not be_nil
      expect(AppstoreCheck.step_1).to_not be_nil
      expect(AppstoreCheck.step_2).to_not be_nil
      expect(AppstoreCheck.step_3).to_not be_nil
      expect(AppstoreCheck.step_4).to_not be_nil
      expect(AppstoreCheck.step_5).to_not be_nil
      expect(AppstoreCheck.step_6).to_not be_nil
      expect(AppstoreCheck.step_7).to_not be_nil
      expect(AppstoreCheck.step_8).to_not be_nil
      expect(AppstoreCheck.step_9).to_not be_nil
      expect(AppstoreCheck.step_10).to_not be_nil
      expect(AppstoreCheck.step_11).to_not be_nil
      expect(AppstoreCheck.step_12).to_not be_nil
      expect(AppstoreCheck.step_13).to_not be_nil
      expect(AppstoreCheck.step_14).to_not be_nil
      expect(AppstoreCheck.step_15).to_not be_nil
      expect(AppstoreCheck.step_16).to_not be_nil
      expect(AppstoreCheck.step_17).to_not be_nil
      expect(AppstoreCheck.step_18).to_not be_nil
      expect(AppstoreCheck.step_19).to_not be_nil
      expect(AppstoreCheck.step_20).to_not be_nil
      expect(AppstoreCheck.step_21).to_not be_nil

      @streaming_checks = AppstoreCheck.streaming_checks
    end

    it "should steps consiste only steps and not have prepare" do
      expect(@streaming_checks).to_not be_nil
      expect(@streaming_checks.empty?).to be false
      expect(@streaming_checks).to eq [
                               AppstoreCheck.step_1,
                               AppstoreCheck.step_3,
                               AppstoreCheck.step_4,
                               AppstoreCheck.step_5,
                               AppstoreCheck.step_6,
                               AppstoreCheck.step_7,
                               AppstoreCheck.step_8,
                               AppstoreCheck.step_9,
                               AppstoreCheck.step_10,
                               AppstoreCheck.step_11,
                               AppstoreCheck.step_12,
                               AppstoreCheck.step_13,
                               AppstoreCheck.step_14,
                               AppstoreCheck.step_15,
                               AppstoreCheck.step_16,
                               AppstoreCheck.step_18,
                               AppstoreCheck.step_19,
                               AppstoreCheck.step_20,
                               AppstoreCheck.step_21
                           ]
    end

  end

end
