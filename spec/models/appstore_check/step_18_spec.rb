# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 18" do

    before(:all) do
      @step_18 = AppstoreCheck.step_18
    end

    it "should step_18 be step_18" do
      expect(@step_18).to_not be_nil
      expect(@step_18.internal_name).to eq 'step_18'
      expect(@step_18.klass).to eq 'Steps::Step_18'
      expect(@step_18.sort).to eq 18
    end

  end

end
