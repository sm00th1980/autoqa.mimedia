# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 11" do

    before(:all) do
      @step_11 = AppstoreCheck.step_11
    end

    it "should step_11 be step_11" do
      expect(@step_11).to_not be_nil
      expect(@step_11.internal_name).to eq 'step_11'
      expect(@step_11.klass).to eq 'Steps::Step_11'
      expect(@step_11.sort).to eq 11
    end

  end

end
