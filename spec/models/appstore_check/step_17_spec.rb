# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "step 17" do

    before(:all) do
      @step_17 = AppstoreCheck.step_17
    end

    it "should step_17 be step_17" do
      expect(@step_17).to_not be_nil
      expect(@step_17.internal_name).to eq 'step_17'
      expect(@step_17.klass).to eq 'Steps::Step_17'
      expect(@step_17.sort).to eq 17
    end

  end

end
