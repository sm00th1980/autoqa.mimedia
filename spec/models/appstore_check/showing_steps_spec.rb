# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AppstoreCheck, type: :model do

  describe "showing_checks" do

    before(:all) do
      expect(AppstoreCheck.prepare).to_not be_nil
      expect(AppstoreCheck.step_1).to_not be_nil
      expect(AppstoreCheck.step_2).to_not be_nil
      expect(AppstoreCheck.step_3).to_not be_nil
      expect(AppstoreCheck.step_4).to_not be_nil
      expect(AppstoreCheck.step_5).to_not be_nil
      expect(AppstoreCheck.step_6).to_not be_nil
      expect(AppstoreCheck.step_7).to_not be_nil
      expect(AppstoreCheck.step_8).to_not be_nil
      expect(AppstoreCheck.step_9).to_not be_nil
      expect(AppstoreCheck.step_10).to_not be_nil
      expect(AppstoreCheck.step_11).to_not be_nil
      expect(AppstoreCheck.step_12).to_not be_nil
      expect(AppstoreCheck.step_13).to_not be_nil
      expect(AppstoreCheck.step_14).to_not be_nil
      expect(AppstoreCheck.step_15).to_not be_nil
      expect(AppstoreCheck.step_16).to_not be_nil
      expect(AppstoreCheck.step_17).to_not be_nil
      expect(AppstoreCheck.step_18).to_not be_nil
      expect(AppstoreCheck.step_19).to_not be_nil
      expect(AppstoreCheck.step_20).to_not be_nil
      expect(AppstoreCheck.step_21).to_not be_nil

      @showing_steps = AppstoreCheck.showing_steps
    end

    it "should @showing_steps consiste only steps and have prepare" do
      expect do
        expect(@showing_steps).to_not be_nil
        expect(@showing_steps.empty?).to be false
        expect do
          AppstoreCheck.showing_steps
        end.not_to raise_error
      end

    end

  end
end
