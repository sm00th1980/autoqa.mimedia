# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "getting new job to run" do
    before(:all) do
      JenkinsJob.delete_all

      @job = create(:jenkins_job)

      check_scheduled_job(@job)
    end

    it "should return first not running job" do
      next_job = JenkinsJob.next
      expect(next_job).to eq @job
      check_scheduled_job(next_job)
    end

  end

  describe "getting new job to run if exists many jobs" do
    before(:all) do
      JenkinsJob.delete_all
      5.times { create(:jenkins_job) }

      @job = JenkinsJob.first

      check_scheduled_job(@job)
    end

    it "should return first not running job" do
      next_job = JenkinsJob.next
      expect(next_job).to eq @job
      check_scheduled_job(next_job)
    end

  end

  describe "getting new job if one is running" do
    before(:all) do
      JenkinsJob.delete_all
      @job = create(:running_jenkins_job)

      check_running_job(@job)
    end

    it "should not return any job if one is running" do
      next_job = JenkinsJob.next
      expect(next_job).to be_nil
    end

  end

end
