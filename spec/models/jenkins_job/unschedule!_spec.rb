# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "unschedule job with success" do
    before(:all) do
      @job = create(:jenkins_job)
      check_scheduled_job(@job)
    end

    it "should be able to unschedule schedulled job" do
      assert_difference "JenkinsJob.count", -1 do
        result = @job.unschedule!
        expect(JenkinsJob.exists?(id: @job.id)).to eq false
        expect(result[:success]).to eq true
        expect(result[:message]).to eq I18n.t('job.success.unschedulled')
      end
    end
  end

  describe "unschedule running job with fail" do
    before(:all) do
      @job = create(:running_jenkins_job)
      check_running_job(@job)
    end

    it "should not be able to unschedule running job" do
      assert_difference "JenkinsJob.count", 0 do
        result = @job.unschedule!
        expect(JenkinsJob.exists?(id: @job.id)).to eq true
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('job.failure.unschedulled')
        check_running_job(@job)
      end
    end
  end

  describe "unschedule finished success job with fail" do
    before(:all) do
      @job = create(:jenkins_job_finished_with_success)
      check_finished_with_success_job(@job)
    end

    it "should not be able to unschedule finished_with_success job" do
      assert_difference "JenkinsJob.count", 0 do
        result = @job.unschedule!
        expect(JenkinsJob.exists?(id: @job.id)).to eq true
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('job.failure.unschedulled')
        check_finished_with_success_job(@job)
      end
    end
  end

  describe "unschedule finished fail job with fail" do
    before(:all) do
      @job = create(:jenkins_job_finished_with_fail)
      check_finished_with_fail_job(@job)
    end

    it "should not be able to unschedule finished_with_fail job" do
      assert_difference "JenkinsJob.count", 0 do
        result = @job.unschedule!
        expect(JenkinsJob.exists?(id: @job.id)).to eq true
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('job.failure.unschedulled')
        check_finished_with_fail_job(@job)
      end
    end
  end

end
