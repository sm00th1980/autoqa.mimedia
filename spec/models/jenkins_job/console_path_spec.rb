# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "running job" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)
    end

    it "should has correct console_path" do
      host = Rails.configuration.jenkins[:host]
      port = Rails.configuration.jenkins[:port]

      expect(@job.console_path).to eq "http://#{host}:#{port}/job/#{@job.current_job_name}/1/console"
    end

  end

  describe "scheduled job" do
    before(:all) do
      @job = create(:jenkins_job)
      check_scheduled_job(@job)
    end

    it "should has correct console_path" do
      expect(@job.console_path).to be_nil
    end

  end

end
