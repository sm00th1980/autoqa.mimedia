# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "schedule with failed scenarios new job with success" do
    before(:all) do
      @testrun = create(:testrun_failed)
      @scenario_ids = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.id }.sort

      expect(@testrun).to_not be_nil
      expect(@scenario_ids.count).to be > 1
      expect(@testrun.scheduled?).to eq false
    end

    it "should schedule new job as last" do
      expect(@testrun.reload.scheduled?).to eq false

      assert_difference "JenkinsJob.count", @scenario_ids.count do
        result = JenkinsJob.schedule_failed_or_pending(@testrun)
        expect(result[:success]).to eq true
        expect(result[:message]).to eq I18n.t('testrun.success.schedulled')

        @scenario_ids.each do |scenario_id|
          job = JenkinsJob.find_by(testrun: @testrun, scenario_id: scenario_id)

          expect(job).to_not be_nil
          expect(job.start).to be_nil
          expect(@testrun.reload.scheduled?).to eq true
        end
      end
    end

    after(:all) do
      Testrun.delete_all
      Scenario.delete_all
      Testcase.delete_all
    end

  end

  describe "not schedule new job with failure if no failed or pending scenarios" do
    before(:all) do
      @testrun = create(:testrun_passed)

      expect(@testrun).to_not be_nil
      expect(@testrun.scenarios.select { |s| s.failed? }.count).to eq 0
      expect(@testrun.scheduled?).to eq false
    end

    it "should not append new job if testrun not has failed or pending scenarios" do
      assert_difference "JenkinsJob.count", 0 do
        result = JenkinsJob.schedule_failed_or_pending(@testrun)
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('testrun.failure.without_failed_or_pending_scenarios')
        expect(@testrun.reload.scheduled?).to eq false
      end
    end

  end

  describe "not schedule new job with failure if its already scheduled" do
    before(:all) do
      @testrun = create(:testrun_failed)
      @scenario_ids = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.id }.sort

      expect(@testrun).to_not be_nil
      expect(@scenario_ids.count).to be > 0
      expect(@testrun.scheduled?).to eq false
    end

    it "should not append new job if testrun is alredy schedulled" do
      JenkinsJob.schedule_failed_or_pending(@testrun)
      expect(@testrun.reload.scheduled?).to eq true

      assert_difference "JenkinsJob.count", 0 do
        result = JenkinsJob.schedule_failed_or_pending(@testrun)
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('testrun.failure.already_schedulled')
        expect(@testrun.reload.scheduled?).to eq true
      end
    end

  end

  describe "not schedule running job" do
    before(:all) do
      JenkinsJob.delete_all

      @testrun = create(:running_testrun)
      @scenario_ids = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.id }.sort

      expect(@testrun).to_not be_nil
      expect(@scenario_ids.count).to be > 0
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq true
    end

    it "should not be able to schedule running job" do
      result = JenkinsJob.schedule_failed_or_pending(@testrun)
      expect(result[:success]).to eq false
      expect(result[:message]).to eq I18n.t('testrun.failure.already_schedulled')

      expect(@testrun.reload.scheduled?).to eq false
      expect(@testrun.reload.running?).to eq true
    end
  end

end
