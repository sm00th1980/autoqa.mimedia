# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "unschedule scheduled job" do
    before(:all) do
      JenkinsJob.delete_all

      @testrun = create(:scheduled_testrun)

      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq true
      expect(@testrun.running?).to eq false
    end

    it "should be able to unschedule schedulled job" do
      result = JenkinsJob.unschedule(@testrun)
      expect(result[:success]).to eq true
      expect(result[:message]).to eq I18n.t('testrun.success.unschedulled')

      expect(@testrun.reload.scheduled?).to eq false
      expect(@testrun.reload.running?).to eq false
    end
  end

  describe "not unschedule running job" do
    before(:all) do
      JenkinsJob.delete_all

      @testrun = create(:running_testrun)

      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq true
    end

    it "should not be able to unschedule running job" do
      result = JenkinsJob.unschedule(@testrun)
      expect(result[:success]).to eq false
      expect(result[:message]).to eq I18n.t('testrun.failure.unschedulled')

      expect(@testrun.reload.scheduled?).to eq false
      expect(@testrun.reload.running?).to eq true
    end
  end

end
