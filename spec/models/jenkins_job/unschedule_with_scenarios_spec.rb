# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "unschedule job with success" do
    before(:all) do
      @testrun = create(:testrun_failed)
      @scenario_ids = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.id }.sort

      expect(@testrun).to_not be_nil
      expect(@scenario_ids.count).to be > 1
      expect(@testrun.scheduled?).to eq false

      JenkinsJob.schedule_failed_or_pending(@testrun)
      expect(@testrun.reload.scheduled?).to eq true
    end

    it "should unschedule job" do
      assert_difference "JenkinsJob.count", -1*@scenario_ids.count do
        result = JenkinsJob.unschedule(@testrun)
        expect(result[:success]).to eq true
        expect(result[:message]).to eq I18n.t('testrun.success.unschedulled')
        expect(@testrun.reload.scheduled?).to eq false
      end
    end

  end

  describe "unschedule job with failure" do
    before(:all) do
      @testrun = create(:testrun_failed)
      expect(@testrun.scheduled?).to eq false
    end

    it "should unschedule job not scheduled job" do
      assert_difference "JenkinsJob.count", 0 do
        result = JenkinsJob.unschedule(@testrun)
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('testrun.failure.unschedulled')
        expect(@testrun.reload.scheduled?).to eq false
      end
    end

  end

end
