# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "schedule new job with success" do
    before(:all) do
      @testrun = create(:testrun)

      expect(@testrun).to_not be_nil
    end

    it "should schedule new job as last" do
      expect(@testrun.reload.scheduled?).to eq false

      assert_difference "JenkinsJob.count" do
        result = JenkinsJob.schedule(@testrun)
        expect(result[:success]).to eq true

        job = JenkinsJob.last

        expect(job.testrun).to eq @testrun
        expect(job.start).to be_nil
        expect(@testrun.reload.scheduled?).to eq true
      end
    end

  end

  describe "schedule new job with failure" do
    before(:all) do
      @testrun = create(:scheduled_testrun)

      expect(@testrun).to_not be_nil
      expect(@testrun.reload.scheduled?).to eq true
    end

    it "should not append new job if testrun is alredy schedulled" do
      assert_difference "JenkinsJob.count", 0 do
        result = JenkinsJob.schedule(@testrun)
        expect(result[:success]).to eq false
        expect(result[:message]).to eq I18n.t('testrun.failure.already_schedulled')
      end
    end

  end

  describe "not schedule running job" do
    before(:all) do
      JenkinsJob.delete_all

      @testrun = create(:running_testrun)

      expect(@testrun).to_not be_nil
      expect(@testrun.scheduled?).to eq false
      expect(@testrun.running?).to eq true
    end

    it "should not be able to schedule running job" do
      result = JenkinsJob.schedule(@testrun)
      expect(result[:success]).to eq false
      expect(result[:message]).to eq I18n.t('testrun.failure.already_schedulled')

      expect(@testrun.reload.scheduled?).to eq false
      expect(@testrun.reload.running?).to eq true
    end
  end

end
