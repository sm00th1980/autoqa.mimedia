# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "job have node if node_id is filled" do
    before(:all) do
      @job = create(:running_jenkins_job)

      check_running_job(@job)
      expect(@job).to_not be_nil
      expect(@job.node_id).to_not be_nil

      @node = Node.find_by(id: @job.node_id)
      expect(@node).to_not be_nil
    end

    it "should have node" do
      expect(@job.node).to eq @node
    end

  end

  describe "job not have node if node_id is not filled" do
    before(:all) do
      @job = create(:jenkins_job)

      check_scheduled_job(@job)
      expect(@job).to_not be_nil
      expect(@job.node_id).to be_nil
    end

    it "should have not node" do
      expect(@job.node).to be_nil
    end

  end

end
