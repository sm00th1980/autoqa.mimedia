# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "mobile maven string" do

    it "should return correct maven string on iphone 4s 8.4" do
      job = create(:running_jenkins_job, testrun: create(:testrun_iphone_4s_8_4))
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPhone_4s -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should return correct maven string on iphone 6 plus 8.4" do
      job = create(:running_jenkins_job, testrun: create(:testrun_iphone_6_plus_8_4))
      expect(job.testrun.name).to eq 'iPhone 6+'
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPhone_6_Plus -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should return correct maven string on iphone 4s 9.0" do
      job = create(:running_jenkins_job, testrun: create(:testrun_iphone_4s_9_0))
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_4s -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should return correct maven string on iphone 5 9.0" do
      job = create(:running_jenkins_job, testrun: create(:testrun_iphone_5_9_0))
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_5 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should return correct maven string on iphone 6 plus 9.0" do
      job = create(:running_jenkins_job, testrun: create(:testrun_iphone_6_plus_9_0))
      expect(job.testrun.name).to eq 'iPhone 6+'
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_6_Plus -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    private
    def options
      "&quot;--tags ~@tablet --tags ~@ios_device_only&quot;"
    end

  end

  describe "tablet maven string" do

    it "should return correct maven string on ipad 2 8.4" do
      job = create(:running_jenkins_job, testrun: create(:testrun_ipad_2_8_4))
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPad_2 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should return correct maven string on ipad 2 9.0" do
      job = create(:running_jenkins_job, testrun: create(:testrun_ipad_2_9_0))
      expect(job.maven_string).to eq "clean test -Dcucumber.options=#{options} -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPad_2 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    private
    def options
      "&quot;--tags ~@mobile --tags ~@ios_device_only&quot;"
    end

  end

end
