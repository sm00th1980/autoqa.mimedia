# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "mobile maven string" do

    it "should generate correct maven string on iphone 4s 8.4" do
      testrun = create(:testrun_iphone_4s_8_4)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPhone_4s -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should generate correct maven string on iphone 6 plus 8.4" do
      testrun = create(:testrun_iphone_6_plus_8_4)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)
      expect(job.testrun.name).to eq 'iPhone 6+'

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPhone_6_Plus -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end


    it "should generate correct maven string on iphone 6 plus 9.0" do
      testrun = create(:testrun_iphone_6_plus_9_0)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)
      expect(job.testrun.name).to eq 'iPhone 6+'

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_6_Plus -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should generate correct maven string on iphone 4s 9.0" do
      testrun = create(:testrun_iphone_4s_9_0)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_4s -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should generate correct maven string on iphone 5 9.0" do
      testrun = create(:testrun_iphone_5_9_0)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPhone_5 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

  end

  describe "tablet maven string" do

    it "should generate correct maven string on ipad 2 8.4" do
      testrun = create(:testrun_ipad_2_8_4)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPad_2 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

    it "should generate correct maven string on ipad 2 9.0" do
      testrun = create(:testrun_ipad_2_9_0)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job).id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=9.0 -DdeviceName=iPad_2 -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

  end

  describe "should escape scpecial symbols" do

    it "should generate correct maven string on iphone 4s 8.4" do
      testrun = create(:testrun_iphone_4s_8_4)
      job = create(:running_jenkins_job, testrun: testrun)
      job.update_attribute(:scenario_id, create(:scenario, testrun: testrun, job: job, name: "at AddToDrive page i'm able to open CreateDrive page").id)

      expect(job.maven_string).to eq "clean test -Dcucumber.options=&quot;#{options(job)}&quot; -DargLine=&quot;-DplatformVersion=8.4 -DdeviceName=iPhone_4s -DjobID=#{job.id} -DnodeIP=#{job.node.host} -DbuildPath=#{job.node.home_dir}&quot;"
    end

  end

  private
  def options(job)
    scenario = Scenario.find_by(id: job.scenario_id)
    "--name '#{ERB::Util.html_escape(scenario.name)}'"
  end

end
