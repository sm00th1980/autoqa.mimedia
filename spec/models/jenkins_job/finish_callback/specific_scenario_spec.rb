# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "job has finish_callback url with specific scenarios" do

    before(:all) do
      @job = create(:running_jenkins_job_with_failed_testrun)
      expect(@job.scenario_id).to_not be_nil
    end

    it "should has valid finish callback url" do
      expect(@job.finish_callback).to eq "curl #{Rails.configuration.hostname}#{api_v1_job_finished_path(id: @job.id)}"
    end

  end

end
