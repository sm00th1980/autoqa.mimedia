# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "run job" do
    before(:all) do
      @job = create(:jenkins_job)

      check_scheduled_job(@job)
      @manager = Jenkins::Manager.new(@job)

      @node = Node.free.first
      expect(@node).to_not be_nil
    end

    it "should run job" do
      stubbing(@manager.current_job_name, @manager.config(@node)) do
        check_scheduled_job(@job)

        @job.run!(@manager.current_job_name)

        check_running_job(@job)
        expect(Time.now - @job.reload.start).to be < 5

        expect(@node.busy?).to eq true
        expect(@job.reload.node_id).to eq @node.id
      end
    end

  end

  describe "not run job if it running" do
    before(:all) do
      JenkinsJob.delete_all

      @running_job = create(:running_jenkins_job)
      @not_running_jobs = 5.times.collect { create(:jenkins_job) }

      check_running_job(@running_job)

      @not_running_jobs.each do |not_running_job|
        check_scheduled_job(not_running_job)
      end
    end

    it "should not run running any other job" do
      check_running_job(@running_job)

      @running_job.run!

      check_running_job(@running_job)

      @not_running_jobs.each do |not_running_job|
        check_scheduled_job(not_running_job)
      end
    end

  end

  describe "not run job if no free nodes" do
    before(:all) do
      JenkinsJob.delete_all

      @running_job = create(:running_jenkins_job)
      check_running_job(@running_job)

      @not_running_job = create(:jenkins_job)
      check_scheduled_job(@not_running_job)

      Node.delete_all
      expect(Node.free.empty?).to eq true
    end

    it "should not run job if no free nodes" do
      @not_running_job.run!

      check_running_job(@running_job)
      check_scheduled_job(@not_running_job)
    end

  end

end
