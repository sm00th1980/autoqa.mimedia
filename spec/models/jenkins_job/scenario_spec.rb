# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "job have scenario if scenario_id is filled" do
    before(:all) do
      @job = create(:running_jenkins_job_with_failed_testrun)

      expect(@job).to_not be_nil
      expect(@job.scenario_id).to_not be_nil

      @scenario = Scenario.find_by(id: @job.scenario_id)
      expect(@scenario).to_not be_nil
    end

    it "should have scenario" do
      expect(@job.scenario).to eq @scenario
    end

  end

  describe "job not have scenario if scenario_id is not filled" do
    before(:all) do
      @job = create(:jenkins_job)

      expect(@job).to_not be_nil
      expect(@job.scenario_id).to be_nil
    end

    it "should have not scenario" do
      expect(@job.scenario).to be_nil
    end

  end

end
