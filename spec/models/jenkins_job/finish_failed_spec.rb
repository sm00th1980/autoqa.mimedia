# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JenkinsJob, type: :model do

  describe "finish failed job" do
    before(:all) do
      @job = create(:running_jenkins_job)
      check_running_job(@job)
    end

    it "should finish failed job" do
      assert_difference "JenkinsJob.count", 0 do
        @job.finish_failed!
        expect(JenkinsJob.exists?(id: @job)).to eq true
        expect(@job.reload.status).to eq JobStatus.finished_with_fail
        expect(@job.stop).to_not be_nil
      end
    end

  end

end
