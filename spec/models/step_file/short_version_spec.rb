# -*- encoding : utf-8 -*-
require 'rails_helper'

describe StepFile, type: :model do

  describe "short version 3 length" do
    before(:all) do
      @step_file = create(:step_file, version: '1.0.24')

      expect(@step_file).to_not be_nil
      expect(@step_file.version).to_not be_nil
    end

    it "should has correct short version for 3 length" do
      expect(@step_file.short_version).to eq '1.0.24'
    end

  end

  describe "short version 4 length" do
    before(:all) do
      @step_file = create(:step_file, version: '1.0.24.1')

      expect(@step_file).to_not be_nil
      expect(@step_file.version).to_not be_nil
    end

    it "should has correct short version for 4 length" do
      expect(@step_file.short_version).to eq '1.0.24'
    end

  end

  describe "short version 5 length" do
    before(:all) do
      @step_file = create(:step_file, version: '1.0.24.1.6')

      expect(@step_file).to_not be_nil
      expect(@step_file.version).to_not be_nil
    end

    it "should has correct short version for 5 length" do
      expect(@step_file.short_version).to eq '1.0.24'
    end

  end

  describe "short version 4 length with subversion" do
    before(:all) do
      @step_file = create(:step_file, version: '1.0.24.1_x')

      expect(@step_file).to_not be_nil
      expect(@step_file.version).to_not be_nil
    end

    it "should has correct short version for 4 length with subversion" do
      expect(@step_file.short_version).to eq '1.0.24'
    end

  end

  describe "short version with blank version" do
    before(:all) do
      @step_file = create(:step_file, version: nil)

      expect(@step_file).to_not be_nil
      expect(@step_file.version).to be_nil
    end

    it "should has correct short version for nil version" do
      expect(@step_file.short_version).to be_nil
    end

  end

end
