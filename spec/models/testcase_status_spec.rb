# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestcaseStatus, type: :model do
  before(:all) do
    TestcaseStatus.delete_all

    @passed = create(:testcase_status_passed)
    @pending = create(:testcase_status_pending)
    @broken = create(:testcase_status_broken)
    @unknown = create(:testcase_status_unknown)
    @broken_by_infrastructure = create(:testcase_status_broken_by_infrastructure)
  end

  after(:all) do
    TestcaseStatus.delete_all
  end

  #passed
  it "should passed status be passed" do
    expect(@passed.passed?).to eq true
    expect(@pending.passed?).to eq false
    expect(@broken.passed?).to eq false
    expect(@unknown.passed?).to eq false
    expect(@broken_by_infrastructure.passed?).to eq false

    expect(@passed.internal_name).to eq 'passed'
    expect(TestcaseStatus.passed).to eq @passed
  end

  #pending
  it "should pending status be pending" do
    expect(@passed.pending?).to eq false
    expect(@pending.pending?).to eq true
    expect(@broken.pending?).to eq false
    expect(@unknown.pending?).to eq false
    expect(@broken_by_infrastructure.pending?).to eq false

    expect(@pending.internal_name).to eq 'pending'
    expect(TestcaseStatus.pending).to eq @pending
  end

  #broken
  it "should broken status be broken" do
    expect(@passed.broken?).to eq false
    expect(@pending.broken?).to eq false
    expect(@broken.broken?).to eq true
    expect(@unknown.broken?).to eq false
    expect(@broken_by_infrastructure.broken?).to eq false

    expect(@broken.internal_name).to eq 'broken'
    expect(TestcaseStatus.broken).to eq @broken
  end

  #unknown
  it "should unknown status be unknown" do
    expect(@passed.unknown?).to eq false
    expect(@pending.unknown?).to eq false
    expect(@broken.unknown?).to eq false
    expect(@unknown.unknown?).to eq true
    expect(@broken_by_infrastructure.unknown?).to eq false

    expect(@unknown.internal_name).to eq 'unknown'
    expect(TestcaseStatus.unknown).to eq @unknown
  end

  #broken_by_infrastructure
  it "should broken_by_infrastructure status be broken_by_infrastructure" do
    expect(@passed.broken_by_infrastructure?).to eq false
    expect(@pending.broken_by_infrastructure?).to eq false
    expect(@broken.broken_by_infrastructure?).to eq false
    expect(@unknown.broken_by_infrastructure?).to eq false
    expect(@broken_by_infrastructure.broken_by_infrastructure?).to eq true

    expect(@broken_by_infrastructure.internal_name).to eq 'broken_by_infrastructure'
    expect(TestcaseStatus.broken_by_infrastructure).to eq @broken_by_infrastructure
  end

end
