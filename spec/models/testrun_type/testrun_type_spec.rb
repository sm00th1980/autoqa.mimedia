# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunType, type: :model do
  before(:all) do
    TestrunType.delete_all

    @api = create(:testrun_type_api)

    @simulator_ios_7_x = create(:testrun_type_simulator_ios_7_x)
    @simulator_ios_8_x = create(:testrun_type_simulator_ios_8_x)
    @simulator_ios_9_x = create(:testrun_type_simulator_ios_9_x)

    @device_ios_7_x = create(:testrun_type_device_ios_7_x)
    @device_ios_8_x = create(:testrun_type_device_ios_8_x)
    @device_ios_9_x = create(:testrun_type_device_ios_9_x)

    @emulator_android_4_3 = create(:testrun_type_emulator_android_4_3)
    @emulator_android_4_4 = create(:testrun_type_emulator_android_4_4)
    @emulator_android_5_0 = create(:testrun_type_emulator_android_5_0)
    @emulator_android_5_1 = create(:testrun_type_emulator_android_5_1)
  end

  after(:all) do
    TestrunType.delete_all
  end

  #api
  it "should api type be api" do
    expect(@api.mobile?).to eq false
    expect(@api.api?).to eq true
    expect(@api.internal_name).to eq 'api'
    expect(TestrunType.api).to eq @api
    expect(@api.duration).to be @api.testruns.map { |testrun| testrun.duration }.sum
  end

  #ios
  it "should simulator ios 7x be simulator ios 7x" do
    expect(@simulator_ios_7_x.mobile?).to eq true
    expect(@simulator_ios_7_x.api?).to eq false
    expect(@simulator_ios_7_x.internal_name).to eq 'simulator_ios_7_x'
    expect(TestrunType.simulator_ios_7_x).to eq @simulator_ios_7_x
    expect(@simulator_ios_7_x.duration).to be @simulator_ios_7_x.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should simulator ios 8x be simulator ios 8x" do
    expect(@simulator_ios_8_x.mobile?).to eq true
    expect(@simulator_ios_8_x.api?).to eq false
    expect(@simulator_ios_8_x.internal_name).to eq 'simulator_ios_8_x'
    expect(TestrunType.simulator_ios_8_x).to eq @simulator_ios_8_x
    expect(@simulator_ios_8_x.duration).to be @simulator_ios_8_x.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should simulator ios 9x be simulator ios 9x" do
    expect(@simulator_ios_9_x.mobile?).to eq true
    expect(@simulator_ios_9_x.api?).to eq false
    expect(@simulator_ios_9_x.internal_name).to eq 'simulator_ios_9_x'
    expect(TestrunType.simulator_ios_9_x).to eq @simulator_ios_9_x
    expect(@simulator_ios_9_x.duration).to be @simulator_ios_9_x.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should device ios 7x be device ios 7x" do
    expect(@device_ios_7_x.mobile?).to eq true
    expect(@device_ios_7_x.api?).to eq false
    expect(@device_ios_7_x.internal_name).to eq 'device_ios_7_x'
    expect(TestrunType.device_ios_7_x).to eq @device_ios_7_x
    expect(@device_ios_7_x.duration).to be @device_ios_7_x.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should device ios 8x be device ios 8x" do
    expect(@device_ios_8_x.mobile?).to eq true
    expect(@device_ios_8_x.api?).to eq false
    expect(@device_ios_8_x.internal_name).to eq 'device_ios_8_x'
    expect(TestrunType.device_ios_8_x).to eq @device_ios_8_x
    expect(@device_ios_8_x.duration).to be @device_ios_8_x.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should device ios 9x be device ios 9x" do
    expect(@device_ios_9_x.mobile?).to eq true
    expect(@device_ios_9_x.api?).to eq false
    expect(@device_ios_9_x.internal_name).to eq 'device_ios_9_x'
    expect(TestrunType.device_ios_9_x).to eq @device_ios_9_x
    expect(@device_ios_9_x.duration).to be @device_ios_9_x.testruns.map { |testrun| testrun.duration }.sum
  end

  #android
  it "should emulator android 4.3 be emulator 4.3" do
    expect(@emulator_android_4_3.mobile?).to eq true
    expect(@emulator_android_4_3.api?).to eq false
    expect(@emulator_android_4_3.internal_name).to eq 'emulator_android_4_3'
    expect(TestrunType.emulator_android_4_3).to eq @emulator_android_4_3
    expect(@emulator_android_4_3.duration).to be @emulator_android_4_3.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should emulator android 4.4 be emulator 4.4" do
    expect(@emulator_android_4_4.mobile?).to eq true
    expect(@emulator_android_4_4.api?).to eq false
    expect(@emulator_android_4_4.internal_name).to eq 'emulator_android_4_4'
    expect(TestrunType.emulator_android_4_4).to eq @emulator_android_4_4
    expect(@emulator_android_4_4.duration).to be @emulator_android_4_4.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should emulator android 5.0 be emulator 5.0" do
    expect(@emulator_android_5_0.mobile?).to eq true
    expect(@emulator_android_5_0.api?).to eq false
    expect(@emulator_android_5_0.internal_name).to eq 'emulator_android_5_0'
    expect(TestrunType.emulator_android_5_0).to eq @emulator_android_5_0
    expect(@emulator_android_5_0.duration).to be @emulator_android_5_0.testruns.map { |testrun| testrun.duration }.sum
  end

  it "should emulator android 5.1 be emulator 5.1" do
    expect(@emulator_android_5_1.mobile?).to eq true
    expect(@emulator_android_5_1.api?).to eq false
    expect(@emulator_android_5_1.internal_name).to eq 'emulator_android_5_1'
    expect(TestrunType.emulator_android_5_1).to eq @emulator_android_5_1
    expect(@emulator_android_5_1.duration).to be @emulator_android_5_1.testruns.map { |testrun| testrun.duration }.sum
  end

end
