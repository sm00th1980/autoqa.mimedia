# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TestrunType, type: :model do
  before(:all) do
    TestrunType.delete_all

    @api = create(:testrun_type_api)

    @simulator_ios_7_x = create(:testrun_type_simulator_ios_7_x)
    @simulator_ios_8_x = create(:testrun_type_simulator_ios_8_x)
    @simulator_ios_9_x = create(:testrun_type_simulator_ios_9_x)

    @device_ios_7_x = create(:testrun_type_device_ios_7_x)
    @device_ios_8_x = create(:testrun_type_device_ios_8_x)
    @device_ios_9_x = create(:testrun_type_device_ios_9_x)

    @emulator_android_4_3 = create(:testrun_type_emulator_android_4_3)
    @emulator_android_4_4 = create(:testrun_type_emulator_android_4_4)
    @emulator_android_5_0 = create(:testrun_type_emulator_android_5_0)
    @emulator_android_5_1 = create(:testrun_type_emulator_android_5_1)
  end

  after(:all) do
    TestrunType.delete_all
  end

  it "should ios type be ios" do
    expect(@api.ios?).to eq false

    expect(@simulator_ios_7_x.ios?).to eq true
    expect(@simulator_ios_8_x.ios?).to eq true
    expect(@simulator_ios_9_x.ios?).to eq true

    expect(@device_ios_7_x.ios?).to eq true
    expect(@device_ios_8_x.ios?).to eq true
    expect(@device_ios_9_x.ios?).to eq true

    expect(@emulator_android_4_3.ios?).to eq false
    expect(@emulator_android_4_4.ios?).to eq false
    expect(@emulator_android_5_0.ios?).to eq false
    expect(@emulator_android_5_1.ios?).to eq false
  end

end
