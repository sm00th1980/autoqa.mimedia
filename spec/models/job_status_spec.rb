# -*- encoding : utf-8 -*-
require 'rails_helper'

describe JobStatus, type: :model do
  before(:all) do
    JobStatus.delete_all

    @scheduled = create(:job_status_scheduled)
    @running = create(:job_status_running)
    @finished_with_success = create(:job_status_finished_with_success)
    @finished_with_fail = create(:job_status_finished_with_fail)
  end

  after(:all) do
    JobStatus.delete_all
  end

  #scheduled
  it "should scheduled status be scheduled" do
    expect(@scheduled.scheduled?).to eq true
    expect(@scheduled.running?).to eq false
    expect(@scheduled.finished_with_success?).to eq false
    expect(@scheduled.finished_with_fail?).to eq false

    expect(@scheduled.internal_name).to eq 'scheduled'
    expect(JobStatus.scheduled).to eq @scheduled
  end

  #running
  it "should running status be running" do
    expect(@running.scheduled?).to eq false
    expect(@running.running?).to eq true
    expect(@running.finished_with_success?).to eq false
    expect(@running.finished_with_fail?).to eq false

    expect(@running.internal_name).to eq 'running'
    expect(JobStatus.running).to eq @running
  end

  #finished_with_success
  it "should finished_with_success status be finished_with_success" do
    expect(@finished_with_success.scheduled?).to eq false
    expect(@finished_with_success.running?).to eq false
    expect(@finished_with_success.finished_with_success?).to eq true
    expect(@finished_with_success.finished_with_fail?).to eq false

    expect(@finished_with_success.internal_name).to eq 'finished_with_success'
    expect(JobStatus.finished_with_success).to eq @finished_with_success
  end

  #finished_with_fail
  it "should finished_with_fail status be finished_with_fail" do
    expect(@finished_with_fail.scheduled?).to eq false
    expect(@finished_with_fail.running?).to eq false
    expect(@finished_with_fail.finished_with_success?).to eq false
    expect(@finished_with_fail.finished_with_fail?).to eq true

    expect(@finished_with_fail.internal_name).to eq 'finished_with_fail'
    expect(JobStatus.finished_with_fail).to eq @finished_with_fail
  end

end
