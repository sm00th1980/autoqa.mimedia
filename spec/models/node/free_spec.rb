# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Node, type: :model do

  describe "free" do

    before(:all) do
      Node.delete_all
      busy_nodes = 5.times.collect { create(:node_busy) }
      @free_nodes = 5.times.collect { create(:node_free) }

      busy_nodes.each do |node|
        expect(node.busy?).to eq true
      end

      @free_nodes.each do |node|
        expect(node.busy?).to eq false
      end

    end

    it "should return only free nodes" do
      expect(Node.free.sort).to eq @free_nodes.sort
      Node.free.each do |node|
        expect(node.disabled?).to eq false
      end
    end

    after(:all) do
      Node.delete_all
    end
  end

  describe "no free" do

    before(:all) do
      Node.delete_all

      busy_nodes = 5.times.collect { create(:node_busy) }

      busy_nodes.each do |node|
        expect(node.busy?).to eq true
      end

    end

    it "should return only free nodes" do
      expect(Node.free).to eq []
    end

    after(:all) do
      Node.delete_all
    end
  end

  describe "no free if all disabled" do

    before(:all) do
      Node.delete_all

      disabled_nodes = 5.times.collect { create(:node_free, disabled: true) }

      disabled_nodes.each do |node|
        expect(node.busy?).to eq false
        expect(node.disabled?).to eq true
      end

    end

    it "should return only free and enabled nodes" do
      expect(Node.free).to eq []
    end

    after(:all) do
      Node.delete_all
    end
  end

end
