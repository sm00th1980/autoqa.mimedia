# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Node, type: :model do

  describe "busy node" do

    before(:all) do
      @node = create(:node_busy)
      expect(JenkinsJob.where(status: JobStatus.running, node_id: @node.id).count).to eq 1
    end

    it "should busy node be busy" do
      expect(@node.busy?).to eq true
      expect(@node.disabled?).to eq false
    end
  end

  describe "not busy node" do

    before(:all) do
      @node = create(:node_free)
      expect(JenkinsJob.where(status: JobStatus.running, node_id: @node.id).count).to eq 0
    end

    it "should free node be free" do
      expect(@node.busy?).to eq false
      expect(@node.disabled?).to eq false
    end
  end

end
