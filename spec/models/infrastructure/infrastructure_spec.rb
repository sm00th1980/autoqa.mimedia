# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Infrastructure, type: :model do
  before(:all) do
    Infrastructure.delete_all

    @api = create(:infrastructure_api)
    @api_login = create(:infrastructure_api_login)
    @node1 = create(:infrastructure_node1)
  end

  after(:all) do
    Infrastructure.delete_all
  end

  #api
  it "should api infrastructure be api" do
    expect(@api.name).to eq 'api.dev.mimedia.com'
    expect(@api.primitive).to be true
    expect(Infrastructure.api).to eq @api
  end

  #api_login
  it "should api_login infrastructure be api_login" do
    expect(@api_login.name).to eq 'api.dev.mimedia.com/login'
    expect(@api_login.primitive).to be false
    expect(Infrastructure.api_login).to eq @api_login
  end

  #node1
  it "should node1 infrastructure be node1" do
    expect(@node1.name).to eq 'node1'
    expect(@node1.primitive).to be true
    expect(Infrastructure.node1).to eq @node1
  end
  
end
