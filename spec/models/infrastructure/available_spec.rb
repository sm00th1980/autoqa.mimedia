# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Infrastructure, type: :model do
  describe "available if all available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      @infrastructure = create(:infrastructure_api)

      5.times do |n|
        InfrastructureLog.create!(infrastructure: @infrastructure, available: true, datetime: Time.now - n.days)
      end
    end

    it "should available be available" do
      start_time = InfrastructureLog.where(infrastructure: @infrastructure).order(:datetime).first.datetime
      stop_time = InfrastructureLog.where(infrastructure: @infrastructure).order(:datetime).last.datetime

      expect(@infrastructure.available?(start_time, stop_time)).to eq true
    end

  end

  describe "non available if all non-available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      @infrastructure = create(:infrastructure_api)

      5.times do |n|
        InfrastructureLog.create!(infrastructure: @infrastructure, available: false, datetime: Time.now - n.days)
      end
    end

    it "should non-available be non-available" do
      start_time = InfrastructureLog.where(infrastructure: @infrastructure).order(:datetime).first.datetime
      stop_time = InfrastructureLog.where(infrastructure: @infrastructure).order(:datetime).last.datetime

      expect(@infrastructure.available?(start_time, stop_time)).to eq false
    end

  end

  describe "available if not exists logs at all" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      @infrastructure = create(:infrastructure_api)
      expect(InfrastructureLog.count).to eq 0
    end

    it "should available be available" do
      expect(@infrastructure.available?(Time.now - 1.year, Time.now + 1.year)).to eq true
    end
  end

end
