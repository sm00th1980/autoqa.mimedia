# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Infrastructure, type: :model do
  describe "api non available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      api = create(:infrastructure_api)
      node1 = create(:infrastructure_node1)
      api_login = create(:infrastructure_api_login)

      @start = Time.now
      @stop = Time.now

      InfrastructureLog.create!(infrastructure: api, available: false, datetime: @start)
      InfrastructureLog.create!(infrastructure: node1, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: api_login, available: true, datetime: @start)
    end

    it "should ios available be non available" do
      expect(Infrastructure.ios_available?(@start, @stop)).to eq false
    end

  end

  describe "node1 non available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      api = create(:infrastructure_api)
      node1 = create(:infrastructure_node1)
      api_login = create(:infrastructure_api_login)

      @start = Time.now
      @stop = Time.now

      InfrastructureLog.create!(infrastructure: api, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: node1, available: false, datetime: @start)
      InfrastructureLog.create!(infrastructure: api_login, available: true, datetime: @start)
    end

    it "should ios available be non available" do
      expect(Infrastructure.ios_available?(@start, @stop)).to eq false
    end

  end

  describe "api_login non available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      api = create(:infrastructure_api)
      node1 = create(:infrastructure_node1)
      api_login = create(:infrastructure_api_login)

      @start = Time.now
      @stop = Time.now

      InfrastructureLog.create!(infrastructure: api, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: node1, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: api_login, available: false, datetime: @start)
    end

    it "should ios available be non available" do
      expect(Infrastructure.ios_available?(@start, @stop)).to eq false
    end

  end

  describe "ios infrastructure all available" do
    before(:all) do
      Infrastructure.delete_all
      InfrastructureLog.delete_all

      api = create(:infrastructure_api)
      node1 = create(:infrastructure_node1)
      api_login = create(:infrastructure_api_login)

      @start = Time.now
      @stop = Time.now

      InfrastructureLog.create!(infrastructure: api, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: node1, available: true, datetime: @start)
      InfrastructureLog.create!(infrastructure: api_login, available: true, datetime: @start)
    end

    it "should ios available be available" do
      expect(Infrastructure.ios_available?(@start, @stop)).to eq true
    end

  end

end
