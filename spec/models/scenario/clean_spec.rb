# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Scenario, type: :model do
  describe "cleaning old logs" do
    before(:all) do
      Scenario.delete_all
      Testcase.delete_all

      @scenarios = 10.times.collect do |n|
        create(:scenario_with_testcase_with_data, stop: Time.now - n.days, job_id: rand(100))
      end

      expect(@scenarios).to_not be_nil
      expect(@scenarios.count).to eq 10
      @scenarios.each do |scenario|
        expect(scenario.testcases.count).to be > 0
      end
    end

    it "should clean old logs and stay only last week" do
      assert_difference 'Scenario.count', -3 do
        assert_difference 'Testcase.count', -15 do
          Scenario.clean

          Scenario.find_each do |log|
            expect(log.stop).to be > Time.now - 1.week
          end
        end
      end
    end

  end

end
