# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Scenario, type: :model do

  describe "front xunit" do

    before(:all) do
      testrun = create(:testrun_passed)
      @scenario = create(:scenario_with_testcase_with_data, job: create(:jenkins_job, testrun: testrun))

      expect(@scenario).to_not be_nil
      expect(@scenario.allure_id).to_not be_nil
      expect(@scenario.name).to_not be_nil
      expect(@scenario.testcases.empty?).to eq false
      expect(@scenario.start).to_not be_nil
      expect(@scenario.stop).to_not be_nil
    end

    it "should has xunit view" do
      xunit = {
          "uid" => @scenario.allure_id,
          "name" => @scenario.name,
          "title" => @scenario.name,
          "time" => {
              "start" => @scenario.start.to_i * 1000,
              "stop" => @scenario.stop.to_i * 1000,
              "duration" => @scenario.stop.to_i * 1000 - @scenario.start.to_i * 1000
          },
          "statistic" => {
              "total" => @scenario.testcases.count,
              "passed" => @scenario.testcases.select { |s| s.status.passed? }.count,
              "pending" => @scenario.testcases.select { |s| s.status.pending? }.count,
              "canceled" => 0,
              "failed" => @scenario.testcases.select { |s| s.status.broken? or s.status.broken_by_infrastructure? }.count,
              "broken" => 0
          },
          "description" => nil,
          "testCases" => @scenario.testcases.map { |testcase| testcase.xunit }
      }

      expect(@scenario.xunit).to eq xunit
    end

  end
end
