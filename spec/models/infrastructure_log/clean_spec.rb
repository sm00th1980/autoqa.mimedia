# -*- encoding : utf-8 -*-
require 'rails_helper'

describe InfrastructureLog, type: :model do
  describe "cleaning old logs" do
    before(:all) do
      InfrastructureLog.delete_all

      @infrastructure = create(:infrastructure_api)

      10.times do |n|
        InfrastructureLog.create!(infrastructure: @infrastructure, available: [true, false].sample, datetime: Time.now - n.days)
      end
    end

    it "should clean old logs and stay only last week" do
      assert_difference 'InfrastructureLog.count', -3 do
        InfrastructureLog.clean

        InfrastructureLog.find_each do |log|
          expect(log.datetime).to be > Time.now - 1.week
        end
      end
    end

  end

end
