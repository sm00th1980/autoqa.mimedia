# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testcase, type: :model do

  describe "defects view" do

    before(:all) do
      @testcase = create(:testcase_failed, scenario: create(:scenario_with_testcase_with_data, job: create(:jenkins_job, testrun: create(:testrun_passed))))

      expect(@testcase).to_not be_nil
      expect(@testcase.status.broken?).to be true
      expect(@testcase.data).to_not be_nil
    end

    it "should defects view be correct" do
      data = {
          "uid" => @testcase.allure_id,
          "failure" => {
              "message" => @testcase.data["failure"]["message"],
              "stackTrace" => nil
          },
          "testCases" => [@testcase.xunit]
      }

      expect(@testcase.defects_view).to eq data
    end

  end

  describe "defects view with nil data" do

    before(:all) do
      @testcase = create(:testcase_failed_without_data, scenario: create(:scenario_with_testcase_with_data, job: create(:jenkins_job, testrun: create(:testrun_passed))))

      expect(@testcase).to_not be_nil
      expect(@testcase.status.broken?).to be true
      expect(@testcase.data).to be_nil
    end

    it "should defects view without data be correct" do
      data = {
          "uid" => @testcase.allure_id,
          "failure" => {
              "message" => "",
              "stackTrace" => nil
          },
          "testCases" => [{}]
      }

      expect(@testcase.defects_view).to eq data
    end

  end

end
