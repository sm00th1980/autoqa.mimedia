# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testcase, type: :model do

  describe "front xunit" do

    before(:all) do
      @testcase = create(:testcase_with_data, scenario: create(:scenario_with_testcase_with_data, job: create(:jenkins_job, testrun: create(:testrun_passed))))

      expect(@testcase).to_not be_nil
      expect(@testcase.data).to_not be_nil
    end

    it "should has xunit view" do
      xunit = {
          "uid" => @testcase.data["uid"],
          "name" => @testcase.data["name"],
          "title" => @testcase.data["title"],
          "time" => @testcase.data["time"],
          "severity" => @testcase.data["severity"],
          "status" => @testcase.data["status"]
      }

      expect(@testcase.xunit).to eq xunit
    end

  end

end
