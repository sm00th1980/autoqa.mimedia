# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "check passed testrun duration" do
    before(:each) do
      @passed_testrun = create(:testrun_passed)

      expect(@passed_testrun).to_not be_nil
      expect(@passed_testrun.start).to_not be_nil
      expect(@passed_testrun.stop).to_not be_nil
    end

    it "should calculate duration" do
      expect(@passed_testrun.last_duration).to eq TimeDifference.between(@passed_testrun.start, @passed_testrun.stop).in_seconds.round(0)
    end
  end

  describe "check unknown testrun duration" do
    before(:each) do
      @unknown_testrun = create(:testrun_unknown)

      expect(@unknown_testrun).to_not be_nil
      expect(@unknown_testrun.start).to be_nil
      expect(@unknown_testrun.stop).to be_nil
    end

    it "should calculate duration" do
      expect(@unknown_testrun.last_duration).to eq 0
    end
  end

end
