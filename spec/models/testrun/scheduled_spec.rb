# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "schedule for testrun" do
  
    before(:all) do
      @testrun = create(:testrun_passed)

      expect(@testrun).to_not be_nil
      expect(@testrun.jobs.empty?).to be true
    end

    it "should not be scheduled if not jobs at all" do
      expect(@testrun.scheduled?).to eq false
    end

  end

  describe "schedule for testrun" do

    before(:all) do
      @running_testrun = create(:running_testrun)

      expect(@running_testrun).to_not be_nil
      expect(@running_testrun.jobs.empty?).to be false
      expect(@running_testrun.running?).to eq true
    end

    it "should not scheduled if running" do
      expect(@running_testrun.scheduled?).to eq false
    end

  end

  describe "schedule for testrun" do

    before(:all) do
      JenkinsJob.delete_all
      @scheduled_testrun = create(:scheduled_testrun)

      expect(@scheduled_testrun).to_not be_nil
      expect(@scheduled_testrun.jobs.empty?).to be false
      expect(@scheduled_testrun.jobs.first).to eq JenkinsJob.next
      expect(@scheduled_testrun.running?).to eq false
    end

    it "should scheduled if scheduled" do
      expect(@scheduled_testrun.scheduled?).to eq true
    end

  end

end
