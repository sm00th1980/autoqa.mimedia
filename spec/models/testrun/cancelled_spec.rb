# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "cancelled testruns" do

    before(:all) do
      @cancelled_testrun = create(:testrun_cancelled)

      expect(@cancelled_testrun).to_not be_nil
      expect(@cancelled_testrun.failed_scenarios_count).to eq 0
      expect(@cancelled_testrun.passed_scenarios_count).to be > 0
      expect(@cancelled_testrun.pending_scenarios_count).to be > 0
    end

    it "should passed testrun be passed" do
      expect(@cancelled_testrun.unknown?).to be false
      expect(@cancelled_testrun.failed?).to be false
      expect(@cancelled_testrun.passed?).to be false
      expect(@cancelled_testrun.cancelled?).to be true
    end

  end

end
