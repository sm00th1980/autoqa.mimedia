# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "count scenarios for cancelled testruns" do

    before(:all) do
      @cancelled_testrun = create(:testrun_cancelled)

      expect(@cancelled_testrun).to_not be_nil
      expect(@cancelled_testrun.failed_scenarios_count).to eq 0
      expect(@cancelled_testrun.passed_scenarios_count).to be > 0
      expect(@cancelled_testrun.pending_scenarios_count).to be > 0
    end

    it "should calculate scenarios for cancelled" do
      expect(@cancelled_testrun.total_scenarios_count).to eq (@cancelled_testrun.failed_scenarios_count + @cancelled_testrun.passed_scenarios_count + @cancelled_testrun.pending_scenarios_count)
    end

  end

  describe "count scenarios for failed testruns" do

    before(:all) do
      @failed_testrun = create(:testrun_failed)

      expect(@failed_testrun).to_not be_nil
      expect(@failed_testrun.failed_scenarios_count).to be > 0
      expect(@failed_testrun.passed_scenarios_count).to be > 0
      expect(@failed_testrun.pending_scenarios_count).to be > 0
    end

    it "should calculate scenarios for failed" do
      expect(@failed_testrun.total_scenarios_count).to eq @failed_testrun.failed_scenarios_count + @failed_testrun.passed_scenarios_count + @failed_testrun.pending_scenarios_count
    end

  end

  describe "count scenarios for unknown testruns" do
    before(:all) do
      @unknown_testrun = create(:testrun_unknown)

      expect(@unknown_testrun).to_not be_nil
      expect(@unknown_testrun.failed_scenarios_count).to be 0
      expect(@unknown_testrun.passed_scenarios_count).to be 0
      expect(@unknown_testrun.pending_scenarios_count).to be 0
    end

    it "should calculate scenarios for unknown" do
      expect(@unknown_testrun.total_scenarios_count).to be 0
    end

  end

  describe "count scenarios for passed testruns" do

    before(:all) do
      @passed_testrun = create(:testrun_passed)

      expect(@passed_testrun).to_not be_nil
      expect(@passed_testrun.failed_scenarios_count).to eq 0
      expect(@passed_testrun.passed_scenarios_count).to be > 0
      expect(@passed_testrun.pending_scenarios_count).to eq 0
    end

    it "should calculate scenarios for passed" do
      expect(@passed_testrun.total_scenarios_count).to eq (@passed_testrun.failed_scenarios_count + @passed_testrun.passed_scenarios_count + @passed_testrun.pending_scenarios_count)
    end

  end

end
