# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "defects view" do

    before(:all) do
      @testrun = create(:testrun_failed)

      expect(@testrun).to_not be_nil
      expect(@testrun.name).to_not be_nil
      expect(@testrun.failed?).to be true
    end

    it "should defects view be correct" do
      failed_testcases = @testrun.scenarios.select { |s| s.failed? }.map { |s| s.testcases }.flatten.select { |t| t.status.broken? }
      expect(failed_testcases.count).to be > 0

      data = {
          "defectsList" => [
              {
                  "title" => @testrun.name,
                  "status" => "BROKEN",
                  "defects" => failed_testcases.map { |t| t.defects_view }

              }
          ]
      }

      expect(@testrun.defects_view).to eq data
    end

  end

end
