# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "graph view" do

    before(:all) do
      @testrun = create(:testrun_failed)

      expect(@testrun).to_not be_nil
      @testcases = @testrun.scenarios.map { |s| s.testcases }.flatten
      expect(@testcases.count).to be > 0
    end

    it "should graph view be correct" do
      data = {"testCases" => @testcases.map { |t| t.xunit }}

      expect(@testrun.graph_view).to eq data
    end

  end

end
