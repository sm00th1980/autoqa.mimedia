# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "environment view" do

    before(:all) do
      @testrun = create(:testrun_without_defects)

      expect(@testrun).to_not be_nil
      expect(@testrun.name).to_not be_nil
      expect(@testrun.start).to_not be_nil
      expect(@testrun.stop).to_not be_nil
    end

    it "should environment view be correct" do
      data = {
          "id" => "",
          "name" => @testrun.name,
          "parameter" => [{
                              "name" => "Report started at",
                              "key" => "report.time.start",
                              "value" => @testrun.start.strftime('%Y-%m-%d %H:%M:%S')
                          }, {
                              "name" => "Report finished at",
                              "key" => "report.time.stop",
                              "value" => @testrun.stop.strftime('%Y-%m-%d %H:%M:%S')
                          }
          ]
      }

      expect(@testrun.environment_view).to eq data
    end

  end

  describe "environment view without start and stop" do

    before(:all) do
      @testrun = create(:testrun_unknown)

      expect(@testrun).to_not be_nil
      expect(@testrun.name).to_not be_nil
      expect(@testrun.start).to be_nil
      expect(@testrun.stop).to be_nil
    end

    it "should environment view be correct" do
      data = {
          "id" => "",
          "name" => @testrun.name,
          "parameter" => [{
                              "name" => "Report started at",
                              "key" => "report.time.start",
                              "value" => ""
                          }, {
                              "name" => "Report finished at",
                              "key" => "report.time.stop",
                              "value" => ""
                          }
          ]
      }

      expect(@testrun.environment_view).to eq data
    end

  end

end
