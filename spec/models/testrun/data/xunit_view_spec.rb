# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "xunit view" do

    before(:all) do
      @testrun = create(:testrun_without_defects)

      expect(@testrun).to_not be_nil
      expect(@testrun.scenarios.count).to be > 0
    end

    it "should front_xunit be as xunit" do
      expect(@testrun.xunit_view).to_not be_nil
    end

  end

end
