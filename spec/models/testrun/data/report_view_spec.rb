# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "report view" do

    before(:all) do
      @testrun = create(:testrun_failed)

      expect(@testrun).to_not be_nil
    end

    it "should report view be correct" do
      data = {"size" => 1000, "time" => 1000}
      expect(@testrun.report_view).to eq data
    end

  end

end
