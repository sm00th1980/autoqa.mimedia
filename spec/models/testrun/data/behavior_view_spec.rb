# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "behavior view" do

    before(:all) do
      @testrun = create(:testrun_failed)

      expect(@testrun).to_not be_nil
      expect(@testrun.scenarios.count).to be > 0
    end

    it "should behavior view be correct" do
      data = {
          "features" => @testrun.scenarios.map do |scenario|
            {
                "title" => scenario.name,
                "statistic" => {
                    "total" => scenario.testcases.count,
                    "passed" => scenario.testcases.select { |s| s.status.passed? }.count,
                    "pending" => scenario.testcases.select { |s| s.status.pending? }.count,
                    "canceled" => 0,
                    "failed" => scenario.testcases.select { |s| s.status.broken? or s.status.broken_by_infrastructure? }.count,
                    "broken" => 0
                },
                "stories" => [scenario.xunit]
            }
          end
      }

      expect(@testrun.behavior_view).to eq data
    end

  end

end

# {
#     "features":[
#         {
#             "title":"Home tests",
#             "statistic":{
#                 "total":10,
#                 "passed":10,
#                 "pending":0,
#                 "canceled":0,
#                 "failed":0,
#                 "broken":0
#             },
#             "stories":[
#                 {
#                     "uid":"b1ad76303f58e62d7e44673fc3ac1722",
#                     "title":"Open and close side bar",
#                     "statistic":{
#                         "total":10,
#                         "passed":10,
#                         "pending":0,
#                         "canceled":0,
#                         "failed":0,
#                         "broken":0
#                     },
#                     "testCases":[
#                         {
#                             "uid":"944905ed89064ec5",
#                             "name":"Given I act as \"owner\"",
#                             "title":"Given I act as \"owner\"",
#                             "time":{
#                                 "start":1442336102135,
#                                 "stop":1442336102135,
#                                 "duration":0
#                             },
#                             "severity":"NORMAL",
#                             "status":"PASSED"
#                         },
#                         {
#                             "uid":"6a8b318245f38ae3",
#                             "name":"When I login",
#                             "title":"When I login",
#                             "time":{
#                                 "start":1442336122749,
#                                 "stop":1442336122749,
#                                 "duration":0
#                             },
#                             "severity":"NORMAL",
#                             "status":"PASSED"
#                         },
