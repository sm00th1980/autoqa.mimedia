# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "testrun with running status" do

    before(:all) do
      @testrun = create(:running_testrun)
      expect(@testrun).to_not be_nil

      job = JenkinsJob.where(testrun: @testrun, status: JobStatus.running).last
      check_running_job(job)
    end

    it "should running testrun be running" do
      expect(@testrun.running?).to be true
      expect(@testrun.scheduled?).to be false
    end

  end

  describe "testrun with not running status" do

    before(:all) do
      @testrun = create(:testrun)
      expect(@testrun).to_not be_nil

      expect(JenkinsJob.exists?(testrun: @testrun)).to eq false
    end

    it "should not-running testrun be not-running" do
      expect(@testrun.running?).to be false
    end

  end

  describe "testrun with not running status if it in jobs" do

    before(:all) do
      @testrun = create(:testrun)
      expect(@testrun).to_not be_nil
      JenkinsJob.schedule(@testrun)

      expect(JenkinsJob.exists?(testrun: @testrun)).to eq true
    end

    it "should not-running testrun be not-running" do
      expect(@testrun.running?).to be false
      expect(@testrun.scheduled?).to be true
    end

  end

end
