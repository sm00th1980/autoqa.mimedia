# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "passed testruns" do

    before(:all) do
      @passed_testrun = create(:testrun_passed)

      expect(@passed_testrun).to_not be_nil
      expect(@passed_testrun.failed_scenarios_count).to eq 0
      expect(@passed_testrun.passed_scenarios_count).to be > 0
      expect(@passed_testrun.pending_scenarios_count).to eq 0
    end

    it "should passed testrun be passed" do
      expect(@passed_testrun.unknown?).to be false
      expect(@passed_testrun.failed?).to be false
      expect(@passed_testrun.passed?).to be true
      expect(@passed_testrun.cancelled?).to be false
    end

  end

end
