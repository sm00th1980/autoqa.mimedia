# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "unknown testruns" do

    before(:all) do
      @unknown_testrun = create(:testrun_unknown)

      expect(@unknown_testrun).to_not be_nil
      expect(@unknown_testrun.failed_scenarios_count).to eq 0
      expect(@unknown_testrun.passed_scenarios_count).to eq 0
      expect(@unknown_testrun.pending_scenarios_count).to eq 0
    end

    it "should unknown testrun be unknown" do
      expect(@unknown_testrun.unknown?).to be true
      expect(@unknown_testrun.failed?).to be false
      expect(@unknown_testrun.passed?).to be false
      expect(@unknown_testrun.cancelled?).to be false
    end

  end

end
