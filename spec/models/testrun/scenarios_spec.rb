# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "scenarios without restarted by one" do

    before(:all) do
      @testrun = create(:testrun)

      @job = create(:jenkins_job, testrun: @testrun, status: JobStatus.finished_with_success, scenario_id: nil)

      @scenarios = 3.times.collect do
        create(:scenario, testrun: @testrun, job: @job)
      end
    end

    it "should return only recent scenarios" do
      expect(@testrun.scenarios.count).to eq 3
      expect(@testrun.scenarios).to eq @scenarios
    end

  end

  describe "scenarios with several restarted by one" do

    before(:all) do
      @testrun = create(:testrun)

      @job = create(:jenkins_job, testrun: @testrun, status: JobStatus.finished_with_success, scenario_id: nil)

      @scenario1 = create(:scenario, testrun: @testrun, job: @job)
      @scenario2 = create(:scenario, testrun: @testrun, job: @job)
      @scenario3 = create(:scenario, testrun: @testrun, job: @job)

      job1 = create(:jenkins_job, testrun: @testrun, status: JobStatus.finished_with_success, scenario_id: @scenario1.id)
      job2 = create(:jenkins_job, testrun: @testrun, status: JobStatus.finished_with_success, scenario_id: @scenario2.id)

      @scenario1_restarted = create(:scenario, name: @scenario1.name, testrun: @testrun, job: job1)
      @scenario2_restarted = create(:scenario, name: @scenario2.name, testrun: @testrun, job: job2)
    end

    it "should return only recent scenarios" do
      expect(Scenario.where(testrun: @testrun).count).to eq 5
      expect(@testrun.scenarios.count).to eq 3
      expect(@testrun.scenarios).to eq [@scenario1_restarted, @scenario2_restarted, @scenario3]
    end

  end

end
