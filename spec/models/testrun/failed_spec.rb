# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Testrun, type: :model do

  describe "failed testruns" do

    before(:all) do
      @failed_testrun = create(:testrun_failed)

      expect(@failed_testrun).to_not be_nil
      expect(@failed_testrun.failed_scenarios_count).to be > 0
      expect(@failed_testrun.passed_scenarios_count).to be > 0
      expect(@failed_testrun.pending_scenarios_count).to be > 0
    end

    it "should failed testrun be failed" do
      expect(@failed_testrun.unknown?).to be false
      expect(@failed_testrun.failed?).to be true
      expect(@failed_testrun.passed?).to be false
      expect(@failed_testrun.cancelled?).to be false
    end

  end

end
