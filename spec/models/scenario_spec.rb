# -*- encoding : utf-8 -*-
require 'rails_helper'

describe Scenario, type: :model do

  describe "pending scenario" do
    before(:all) do
      job = create(:jenkins_job_finished_with_success)
      @scenario = create(:scenario_pending, job: job, testrun: job.testrun)

      expect(@scenario.failed_by_appium?).to eq true
    end

    it "should pending be pending" do
      allow(Infrastructure).to receive(:ios_available?).and_return(false)
      expect(Infrastructure.ios_available?(@scenario.start, @scenario.stop)).to eq false

      expect(@scenario.pending?).to eq true
      expect(@scenario.failed?).to eq false
      expect(@scenario.passed?).to eq false
    end

  end

  describe "pending scenario with testcase infrastructure problem" do
    before(:all) do
      job = create(:jenkins_job_finished_with_success)
      @scenario = create(:scenario_pending_with_infrastructure_problem_testcase, job: job)
      expect(@scenario.testcases.select { |t| t.status.broken_by_infrastructure? }.empty?).to eq false
    end

    it "should scenario with testcase infrastructure problem be pending" do
      expect(@scenario.pending?).to eq true
      expect(@scenario.failed?).to eq false
      expect(@scenario.passed?).to eq false
    end

  end

  describe "pending scenario all testcases are pending" do
    before(:all) do
      job = create(:jenkins_job_finished_with_success)
      @scenario = create(:scenario_pending_all_pending_testcases, job: job)
      expect(@scenario.testcases.select { |t| t.status.pending? }.empty?).to eq false
      expect(@scenario.testcases.select { |t| t.status.pending? }.count).to eq @scenario.testcases.count
    end

    it "should scenario with all pending testcases be pending" do
      expect(@scenario.pending?).to eq true
      expect(@scenario.failed?).to eq false
      expect(@scenario.passed?).to eq false
    end

  end

  describe "failed scenario" do
    before(:all) do
      @scenario = create(:scenario_failed, job: create(:jenkins_job_finished_with_success))

      expect(@scenario.failed_by_appium?).to eq true
    end

    it "should failed be failed" do
      expect(Infrastructure.ios_available?(@scenario.start, @scenario.stop)).to eq true

      expect(@scenario.pending?).to eq false
      expect(@scenario.failed?).to eq true
      expect(@scenario.passed?).to eq false
    end

  end

  describe "passed scenario" do
    before(:all) do
      @scenario = create(:scenario_passed, job: create(:jenkins_job_finished_with_success))

      expect(@scenario.failed_by_appium?).to eq false
    end

    it "should passed be passed" do
      expect(@scenario.pending?).to eq false
      expect(@scenario.failed?).to eq false
      expect(@scenario.passed?).to eq true
    end

  end

end
