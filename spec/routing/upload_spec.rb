# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to upload", :type => :routing do

  it "routing get /upload to upload#upload" do
    expect(post: "/upload").to route_to(controller: "upload", action: "upload")
  end

end
