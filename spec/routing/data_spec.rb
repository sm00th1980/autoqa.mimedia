# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to data/*", :type => :routing do

  it "routing get /data/behavior.json to data#behavior" do
    expect(get: "/data/behavior.json").to route_to(controller: "data", action: "behavior")
  end

  it "routing get /data/defects.json to data#defects" do
    expect(get: "/data/defects.json").to route_to(controller: "data", action: "defects")
  end

  it "routing get /data/environment.json to data#environment" do
    expect(get: "/data/environment.json").to route_to(controller: "data", action: "environment")
  end

  it "routing get /data/graph.json to data#graph" do
    expect(get: "/data/graph.json").to route_to(controller: "data", action: "graph")
  end

  it "routing get /data/report.json to data#report" do
    expect(get: "/data/report.json").to route_to(controller: "data", action: "report")
  end

  it "routing get /data/xunit.json to data#xunit" do
    expect(get: "/data/xunit.json").to route_to(controller: "data", action: "xunit")
  end

  it "routing get /data/1b3c72f98009a9f8-testcase.json to data#testcase" do
    expect(get: "/data/1b3c72f98009a9f8-testcase.json").to route_to(controller: "data", action: "testcase", id: "1b3c72f98009a9f8")
  end

end
