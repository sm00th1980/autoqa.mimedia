# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to android", :type => :routing do

  it "routing get /android to android#index" do
    expect(get: "/android").to route_to(controller: "android", action: "index")
  end

end
