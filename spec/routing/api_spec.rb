# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to api", :type => :routing do

  it "routing get /api to api#index" do
    expect(get: "/api").to route_to(controller: "api", action: "index")
  end

end
