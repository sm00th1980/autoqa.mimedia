# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to api v1", :type => :routing do

  it "routing get /api/v1/restart_appium_for_simulator to api/v1#restart_appium_for_simulator" do
    expect(get: "/api/v1/restart_appium_for_simulator").to route_to(controller: "api/v1", action: "restart_appium_for_simulator")
  end

  it "routing get /api/v1/job_finished to api/v1#job_finished" do
    expect(get: "/api/v1/job_finished").to route_to(controller: "api/v1", action: "job_finished")
  end

  it "routing post /api/v1/job_unschedule to api/v1#job_unschedule" do
    expect(post: "/api/v1/job_unschedule?id=123").to route_to(controller: "api/v1", action: "job_unschedule", id: "123")
  end

end
