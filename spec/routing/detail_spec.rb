# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to testrun/:id", :type => :routing do

  #details
  it "routing get /testrun/:id to testrun#edit" do
    expect(get: "/testrun/31").to route_to(controller: "testrun", action: "index", id: "31")
  end

end
