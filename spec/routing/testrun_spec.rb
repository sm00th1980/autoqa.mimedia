# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to testrun/*", :type => :routing do

  it "routing get /testrun/1 to testrun#index" do
    expect(get: "/testrun/1").to route_to(controller: "testrun", action: "index", id: "1")
  end

  it "routing get /testrun/1/schedule to testrun#schedule" do
    expect(post: "/testrun/1/schedule").to route_to(controller: "testrun", action: "schedule", id: "1")
  end

  it "routing get /testrun/1/unschedule to testrun#unschedule" do
    expect(post: "/testrun/1/unschedule").to route_to(controller: "testrun", action: "unschedule", id: "1")
  end

  it "routing get /testrun/1/schedule_failed_or_pending to testrun#schedule_failed_or_pending" do
    expect(post: "/testrun/1/schedule_failed_or_pending").to route_to(controller: "testrun", action: "schedule_failed_or_pending", id: "1")
  end

end
