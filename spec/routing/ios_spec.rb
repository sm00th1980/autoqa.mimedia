# -*- encoding : utf-8 -*-
require 'rails_helper'

describe "routing to ios", :type => :routing do

  it "routing get /ios to ios#index" do
    expect(get: "/ios").to route_to(controller: "ios", action: "index")
  end

end
