# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :testcase_status do

    factory :testcase_status_passed do
      name 'passed'
      internal_name 'passed'
    end

    factory :testcase_status_pending do
      name 'pending'
      internal_name 'pending'
    end

    factory :testcase_status_broken do
      name 'broken'
      internal_name 'broken'
    end

    factory :testcase_status_unknown do
      name 'unknown'
      internal_name 'unknown'
    end

    factory :testcase_status_broken_by_infrastructure do
      name 'broken_by_infrastructure'
      internal_name 'broken_by_infrastructure'
    end

  end

end
