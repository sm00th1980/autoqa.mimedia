# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :testrun do

    name { Faker::Name.name }
    description { Faker::Address.street_address }
    type { TestrunType.all.sample }
    disabled false

    factory :testrun_unknown do
      start nil
      stop nil
      failed_scenarios_count 0
      passed_scenarios_count 0
      pending_scenarios_count 0
    end

    factory :testrun_failed do
      start { Time.now - 2.hour }
      stop { Time.now - 1.hour }
      failed_scenarios_count 2
      passed_scenarios_count 1
      pending_scenarios_count 1

      after(:create) do |testrun|
        job = create(:jenkins_job_finished_with_success, testrun: testrun)
        2.times { create(:scenario_failed, testrun: testrun, job: job) }
      end
    end

    factory :testrun_passed do
      start { Time.now - 2.hour }
      stop { Time.now - 1.hour }
      failed_scenarios_count 0
      passed_scenarios_count 1
      pending_scenarios_count 0
    end

    factory :testrun_cancelled do
      start { Time.now - 2.hour }
      stop { Time.now - 1.hour }
      failed_scenarios_count 0
      passed_scenarios_count 1
      pending_scenarios_count 1
    end

    factory :testrun_iphone_4s_8_4 do
      name 'iPhone 4s'
      type { TestrunType.simulator_ios_8_x }
    end

    factory :testrun_iphone_6_plus_8_4 do
      name 'iPhone 6+'
      type { TestrunType.simulator_ios_8_x }
    end

    factory :testrun_iphone_6_plus_9_0 do
      name 'iPhone 6+'
      type { TestrunType.simulator_ios_9_x }
    end

    factory :testrun_iphone_4s_9_0 do
      name 'iPhone 4s'
      type { TestrunType.simulator_ios_9_x }
    end

    factory :testrun_iphone_5_9_0 do
      name 'iPhone 5'
      type { TestrunType.simulator_ios_9_x }
    end

    factory :testrun_ipad_2_8_4 do
      name 'iPad 2'
      type { TestrunType.simulator_ios_8_x }
    end

    factory :testrun_ipad_2_9_0 do
      name 'iPad 2'
      type { TestrunType.simulator_ios_9_x }
    end

    factory :testrun_android_4_3 do
      name 'Android 4.3'
      type { TestrunType.emulator_android_4_3 }
    end

    factory :testrun_without_data do
    end

    factory :running_testrun do
      after(:create) do |testrun|
        job = create(:jenkins_job_finished_with_success, testrun: testrun)
        create(:scenario_failed, testrun: testrun, job: job)

        JenkinsJob.schedule(testrun)

        job = JenkinsJob.next
        job.start = Time.now
        job.status = JobStatus.running
        job.current_job_name = rand(1000).to_s
        job.node_id = Node.first.id
        job.save!
      end
    end

    factory :scheduled_testrun do
      after(:create) do |testrun|
        JenkinsJob.schedule(testrun)
      end
    end

    factory :scheduled_testrun_with_scenarios do
      after(:create) do |testrun|
        create(:scenario_failed, testrun: testrun)

        JenkinsJob.schedule_failed_or_pending(testrun)
      end
    end

    factory :testrun_with_testcases do
      after(:create) do |testrun|
        job = create(:jenkins_job_finished_with_success, testrun: testrun)
        create(:testcase, scenario: create(:scenario, testrun: testrun, job: job), allure_id: Faker::Name.name)
      end
    end

    factory :testrun_without_defects do
      start { DateTime.strptime((1440954460477.to_f / 1000).to_s, "%s").to_time }
      stop { DateTime.strptime((1440959175895.to_f / 1000).to_s, "%s").to_time }

      after(:create) do |testrun|
        job = create(:jenkins_job_finished_with_success, testrun: testrun)
        create(:scenario_passed, testrun: testrun, job: job)
      end
    end

    factory :testrun_with_defects do
      after(:create) do |testrun|
        job = create(:jenkins_job_finished_with_success, testrun: testrun)
        create(:scenario_failed, testrun: testrun, job: job)
      end
    end

  end
end
