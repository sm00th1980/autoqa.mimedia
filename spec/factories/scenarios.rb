# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :scenario do

    sequence(:name) { |n| "name_#{n}" }
    allure_id '123'
    testrun { Testrun.all.sample }
    start { Time.now - 1.day }
    stop { Time.now - 1.day + 10.minutes }
    failed_by_appium { [true, false].sample }

    factory :scenario_pending do
      failed_by_appium true
    end

    factory :scenario_failed do
      failed_by_appium true

      after(:create) do |scenario|
        InfrastructureLog.delete_all

        InfrastructureLog.create!(datetime: scenario.start, infrastructure: Infrastructure.api, available: true)
        InfrastructureLog.create!(datetime: scenario.start, infrastructure: Infrastructure.node1, available: true)
        InfrastructureLog.create!(datetime: scenario.start, infrastructure: Infrastructure.api_login, available: true)

        create(:testcase_failed, scenario: scenario)
      end

    end

    factory :scenario_passed do
      failed_by_appium false
    end

    factory :scenario_pending_with_infrastructure_problem_testcase do
      after(:create) do |scenario|
        Testcase.create(allure_id: '12345678', scenario: scenario, status: TestcaseStatus.broken_by_infrastructure)
      end
    end

    factory :scenario_pending_all_pending_testcases do
      after(:create) do |scenario|

        5.times do |n|
          Testcase.create(allure_id: (123456789 + n).to_s, scenario: scenario, status: TestcaseStatus.pending)
        end
      end
    end

    factory :scenario_with_testcase_with_data do
      after(:create) do |scenario|

        5.times do |n|
          create(:testcase_with_data, scenario: scenario)
        end
      end
    end

  end

end
