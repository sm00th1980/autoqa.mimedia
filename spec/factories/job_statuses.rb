# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :job_status do

    factory :job_status_scheduled do
      name 'scheduled'
      internal_name 'scheduled'
    end

    factory :job_status_running do
      name 'running'
      internal_name 'running'
    end

    factory :job_status_finished_with_success do
      name 'finished_with_success'
      internal_name 'finished_with_success'
    end

    factory :job_status_finished_with_fail do
      name 'finished_with_fail'
      internal_name 'finished_with_fail'
    end

  end

end
