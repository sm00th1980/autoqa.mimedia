# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :appstore_check do
    expected nil
    detail nil

    factory :appstore_check_prepare do
      name 'prepare'
      internal_name 'prepare'
      sort 0
      klass 'Steps::Prepare'
    end

    factory :appstore_check_step_1 do
      name 'step_1'
      internal_name 'step_1'
      sort 1
      klass 'Steps::Step_1'
      detail 'codesign --verify --no-strict -vvvv %{app}'
      expected '%{app}: valid on disk
%{app}: satisfies its Designated Requirement
'
    end

    factory :appstore_check_step_2 do
      name 'Check code sign entitlements'
      detail 'codesign -dvvv --entitlements - %{app}'
      expected "Executable=%{executable}\nIdentifier=com.mimedia.iOSv2\nFormat=bundle with Mach-O universal (armv7 arm64)\nCodeDirectory v=20200 size=127681 flags=0x0(none) hashes=6375+5 location=embedded\nHash type=sha1 size=20\nCDHash=22b5005c5f6ec5da3c07190224757fe1c8999275\nSignature size=4333\nAuthority=iPhone Distribution: Mimedia Inc. (958B23VCBE)\nAuthority=Apple Worldwide Developer Relations Certification Authority\nAuthority=Apple Root CA\nSigned Time=22 Oct 2015 14:50:45\nInfo.plist entries=42\nTeamIdentifier=958B23VCBE\nSealed Resources version=2 rules=12 files=1067\nInternal requirements count=1 size=184\n??qqa<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n<dict>\n        <key>application-identifier</key>\n        <string>958B23VCBE.com.mimedia.iOSv2</string>\n        <key>aps-environment</key>\n        <string>production</string>\n        <key>beta-reports-active</key>\n        <true/>\n        <key>com.apple.developer.team-identifier</key>\n        <string>958B23VCBE</string>\n        <key>com.apple.security.application-groups</key>\n        <array>\n                <string>group.com.mimedia.iOSv2.add-to</string>\n        </array>\n        <key>get-task-allow</key>\n        <false/>\n</dict>\n</plist>\n"
      sort 2
      internal_name 'step_2'
      klass 'Steps::Step_2'
    end

    factory :appstore_check_step_3 do
      name 'Check configuration'
      detail 'otool -L -vh - %{path}'
      expected "%{path} (architecture armv7):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\n   MH_MAGIC     ARM         V7  0x00     EXECUTE    51       5504   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n\t/usr/lib/libicucore.A.dylib (compatibility version 1.0.0, current version 55.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.5)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libsqlite3.dylib (compatibility version 9.0.0, current version 215.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Photos.framework/Photos (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/QuickLook.framework/QuickLook (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreBluetooth.framework/CoreBluetooth (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreMotion.framework/CoreMotion (compatibility version 1.0.0, current version 1861.0.9)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AddressBook.framework/AddressBook (compatibility version 1.0.0, current version 30.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices (compatibility version 1.0.0, current version 727.5.3)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MediaPlayer.framework/MediaPlayer (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/OpenGLES.framework/OpenGLES (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/ImageIO.framework/ImageIO (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/GLKit.framework/GLKit (compatibility version 1.0.0, current version 21.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreText.framework/CoreText (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreData.framework/CoreData (compatibility version 1.0.0, current version 637.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AVFoundation.framework/AVFoundation (compatibility version 1.0.0, current version 2.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreLocation.framework/CoreLocation (compatibility version 1.0.0, current version 1861.0.9)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MapKit.framework/MapKit (compatibility version 1.0.0, current version 14.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AssetsLibrary.framework/AssetsLibrary (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AdSupport.framework/AdSupport (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/QuartzCore.framework/QuartzCore (compatibility version 1.2.0, current version 1.11.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony (compatibility version 1.0.0, current version 0.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration (compatibility version 1.0.0, current version 802.1.1)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Security.framework/Security (compatibility version 1.0.0, current version 0.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics (compatibility version 64.0.0, current version 600.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/UIKit.framework/UIKit (compatibility version 1.0.0, current version 3505.16.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Foundation.framework/Foundation (compatibility version 300.0.0, current version 1240.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 228.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 237.2.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1225.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Accelerate.framework/Accelerate (compatibility version 1.0.0, current version 4.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CFNetwork.framework/CFNetwork (compatibility version 1.0.0, current version 758.0.2)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation (compatibility version 150.0.0, current version 1240.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreImage.framework/CoreImage (compatibility version 1.0.0, current version 4.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreMedia.framework/CoreMedia (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n%{path} (architecture arm64):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\nMH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    51       6208   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n\t/usr/lib/libicucore.A.dylib (compatibility version 1.0.0, current version 55.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.5)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libsqlite3.dylib (compatibility version 9.0.0, current version 215.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Photos.framework/Photos (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/QuickLook.framework/QuickLook (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreBluetooth.framework/CoreBluetooth (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreMotion.framework/CoreMotion (compatibility version 1.0.0, current version 1861.0.9)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AddressBook.framework/AddressBook (compatibility version 1.0.0, current version 30.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MobileCoreServices.framework/MobileCoreServices (compatibility version 1.0.0, current version 727.5.3)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MediaPlayer.framework/MediaPlayer (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/OpenGLES.framework/OpenGLES (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/ImageIO.framework/ImageIO (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/GLKit.framework/GLKit (compatibility version 1.0.0, current version 21.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreText.framework/CoreText (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreData.framework/CoreData (compatibility version 1.0.0, current version 637.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AVFoundation.framework/AVFoundation (compatibility version 1.0.0, current version 2.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreLocation.framework/CoreLocation (compatibility version 1.0.0, current version 1861.0.9)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/MapKit.framework/MapKit (compatibility version 1.0.0, current version 14.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AssetsLibrary.framework/AssetsLibrary (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/AdSupport.framework/AdSupport (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/QuartzCore.framework/QuartzCore (compatibility version 1.2.0, current version 1.11.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony (compatibility version 1.0.0, current version 0.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration (compatibility version 1.0.0, current version 802.1.1)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Security.framework/Security (compatibility version 1.0.0, current version 0.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreGraphics.framework/CoreGraphics (compatibility version 64.0.0, current version 600.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/UIKit.framework/UIKit (compatibility version 1.0.0, current version 3505.16.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Foundation.framework/Foundation (compatibility version 300.0.0, current version 1240.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libobjc.A.dylib (compatibility version 1.0.0, current version 228.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 237.2.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1225.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/Accelerate.framework/Accelerate (compatibility version 1.0.0, current version 4.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CFNetwork.framework/CFNetwork (compatibility version 1.0.0, current version 758.0.2)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation (compatibility version 150.0.0, current version 1240.1.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreImage.framework/CoreImage (compatibility version 1.0.0, current version 4.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n\t/System/Library/Frameworks/CoreMedia.framework/CoreMedia (compatibility version 1.0.0, current version 1.0.0)\n\ttime stamp 2 Thu Jan  1 04:00:02 1970\n"
      sort 3
      internal_name 'step_3'
      klass 'Steps::Step_3'
    end

    factory :appstore_check_step_4 do
      name 'Check provision'
      detail 'openssl smime -inform der -verify -noverify -in %{provision}'
      expected "Verification successful\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n<dict>\n        <key>AppIDName</key>\n        <string>MiMedia V2</string>\n        <key>ApplicationIdentifierPrefix</key>\n        <array>\n        <string>958B23VCBE</string>\n        </array>\n        <key>CreationDate</key>\n        <date>2015-10-15T09:56:36Z</date>\n        <key>Platform</key>\n        <array>\n                <string>iOS</string>\n        </array>\n        <key>DeveloperCertificates</key>\n        <array>\n                <data>MIIFmzCCBIOgAwIBAgIIJBv7LRD/G9IwDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUwMzAzMDgxMjAxWhcNMTYwMzAyMDgxMjAxWjCBjjEaMBgGCgmSJomT8ixkAQEMCjk1OEIyM1ZDQkUxNzA1BgNVBAMMLmlQaG9uZSBEaXN0cmlidXRpb246IE1pbWVkaWEgSW5jLiAoOTU4QjIzVkNCRSkxEzARBgNVBAsMCjk1OEIyM1ZDQkUxFTATBgNVBAoMDE1pbWVkaWEgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDDESgM4dzLkG6tiPvKbvC7qRYFMVyiaAH/Y1I64ITn7gfz3KFcoknnqKnlzEKqrdTnatc8YXa8lu9ZJBotzMNvI4DH++semHL5MtOBVhQayf8TQVE7JS39RWJsnQ+sEPYvOOgwhW+yudRoG1kQ2oyVFTDlrR0Ew02C3hgndbO0na9cvf5qY63zPXR94mtQuSZs6X4txG/xzJtGkIKG3ZZgDYw1w5dePmvol3Axjig1wjcVakvZnQB29di1a7ca50ykbPYHv6cQX+YwIP9BfGFXgH9A2PwxYFnkPJ3BBLZcs+mfr+wf9rq9hVAHtQO9riCTXKg/ggZILEGHtt3zv9vJAgMBAAGjggHxMIIB7TAdBgNVHQ4EFgQUKrpYWs8zZMq1YEvQxZLiWmPXgukwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAQ8GA1UdIASCAQYwggECMIH/BgkqhkiG92NkBQEwgfEwgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wKQYIKwYBBQUHAgEWHWh0dHA6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvME0GA1UdHwRGMEQwQqBAoD6GPGh0dHA6Ly9kZXZlbG9wZXIuYXBwbGUuY29tL2NlcnRpZmljYXRpb25hdXRob3JpdHkvd3dkcmNhLmNybDAOBgNVHQ8BAf8EBAMCB4AwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwMwEwYKKoZIhvdjZAYBBAEB/wQCBQAwDQYJKoZIhvcNAQEFBQADggEBAEQ4kr2vU4CMapcptpf9DWeTD6IzrsY6UvXKbQ9lYbrMQB3ETyO/JUcYZxZBENZcqP/rbjXG0pr6OXlwcVoFNfnhr4Svv3tZYuym1BG6pHX5AVpBtPYfwr6wL4Sfq0zIPY2NAH7pG6U7lzjVIYTx5JCbQjjv/DsuX6y6j3zO2bCsS0owYoaOmQI1kKFWyL+Dn3aoa6Llke6rjsLd4uCLDwANjUXpQjCSWyZw2VogruJjHhHB2iLDQoCMEPFwKQOj3XwIFd2c0wwfVcUz/7ThPArR65E0ANOGtrBFXrICbq8yLGFpCxKutiYRh2X/vZahqBgeNqs9YtVh4r8q2FGN2yY=</data>\n        </array>\n        <key>Entitlements</key>\n        <dict>\n                <key>keychain-access-groups</key>\n                <array>\n                        <string>958B23VCBE.*</string>                \n                </array>\n                <key>get-task-allow</key>\n                <false/>\n                <key>application-identifier</key>\n                <string>958B23VCBE.com.mimedia.iOSv2</string>\n                <key>com.apple.security.application-groups</key>\n                <array>\n                        <string>group.com.mimedia.iOSv2.add-to</string>\n                </array>\n                <key>com.apple.developer.team-identifier</key>\n                <string>958B23VCBE</string>\n                <key>aps-environment</key>\n                <string>production</string>\n                <key>beta-reports-active</key>\n                <true/>\n\n        </dict>\n        <key>ExpirationDate</key>\n        <date>2016-03-02T08:12:01Z</date>\n        <key>Name</key>\n        <string>MiMediaV2-AppStore-10152015</string>\n        <key>TeamIdentifier</key>\n        <array>\n                <string>958B23VCBE</string>\n        </array>\n        <key>TeamName</key>\n        <string>Mimedia Inc.</string>\n        <key>TimeToLive</key>\n        <integer>138</integer>\n        <key>UUID</key>\n        <string>54d84fb0-a6ba-440c-8615-2cc5f2e38d09</string>\n        <key>Version</key>\n        <integer>1</integer>\n</dict>\n</plist>"
      sort 4
      internal_name 'step_4'
      klass 'Steps::Step_4'
    end

    factory :appstore_check_step_5 do
      name 'Check Bundle ID'
      detail 'defaults read %{app}/Info.plist CFBundleIdentifier'
      expected "com.mimedia.iOSv2\n"
      sort 5
      internal_name 'step_5'
      klass 'Steps::Step_5'
    end

    factory :appstore_check_step_6 do
      name 'Check version'
      detail 'defaults read %{app}/Info.plist CFBundleVersion'
      expected "%{version}\n"
      sort 6
      internal_name 'step_6'
      klass 'Steps::Step_6'
    end

    factory :appstore_check_step_7 do
      name 'Check platform version'
      detail 'defaults read %{app}/Info.plist DTPlatformVersion'
      expected "9.0\n"
      sort 7
      internal_name 'step_7'
      klass 'Steps::Step_7'
    end

    factory :appstore_check_step_8 do
      name 'Check platform build'
      detail 'defaults read %{app}/Info.plist DTPlatformBuild'
      expected "13A340\n"
      sort 8
      internal_name 'step_8'
      klass 'Steps::Step_8'
    end

    factory :appstore_check_step_9 do
      name 'Check DTXcode'
      detail 'defaults read %{app}/Info.plist DTXcode'
      expected "0700\n"
      sort 9
      internal_name 'step_9'
      klass 'Steps::Step_9'
    end

    factory :appstore_check_step_10 do
      name 'Check DTXcodeBuild'
      detail 'defaults read %{app}/Info.plist DTXcodeBuild'
      expected "7A220\n"
      sort 10
      internal_name 'step_10'
      klass 'Steps::Step_10'
    end

    factory :appstore_check_step_11 do
      name 'Check iPhone/iPod Icons'
      detail '-'
      expected "AppIcon60x60@2x.png (MD5 = c2ac7beedc47df00f1706ed8ea49c9b6, SIZE = 120x120)\nAppIcon60x60@3x.png (MD5 = 77e6e87e39f0fc66cc7b88092515ccf7, SIZE = 180x180)"
      sort 11
      internal_name 'step_11'
      klass 'Steps::Step_11'
    end

    factory :appstore_check_step_12 do
      name 'Check iPad Icons'
      detail '-'
      expected "AppIcon76x76~ipad.png (MD5 = 55afe79a201f7598a6b522a9ae0164fb, SIZE = 76x76)\nAppIcon76x76@2x~ipad.png (MD5 = 3e2cdbea632f477fcec314d843776486, SIZE = 152x152)"
      sort 12
      internal_name 'step_12'
      klass 'Steps::Step_12'
    end

    factory :appstore_check_step_13 do
      name 'Check Settings Icons'
      detail '-'
      expected "AppIcon29x29.png (MD5 = 7d8929a5c8a67517cda1a6ac904e6e39, SIZE = 29x29)\nAppIcon29x29@2x.png (MD5 = fe1627baf4758dc8b012a9412587de44, SIZE = 58x58)\nAppIcon29x29~ipad.png (MD5 = 7ee252ac9b82d606767fffb927bb2d9e, SIZE = 29x29)\nAppIcon29x29@2x~ipad.png (MD5 = 7ef4e4032b2ddfdc3787a243725ac734, SIZE = 58x58)"
      sort 13
      internal_name 'step_13'
      klass 'Steps::Step_13'
    end

    factory :appstore_check_step_14 do
      name 'Check iPhone/iPod launch images'
      detail '-'
      expected "LaunchImage.png (MD5 = b327f96c529f29daa169a591b0dfcd8e, SIZE = 320x480)\nLaunchImage@2x.png (MD5 = b8a62b3cf58a668e79c7ab0defa6df3a, SIZE = 640x960)\nLaunchImage-568h@2x.png (MD5 = 30951d5509a3341d61350c337575108a, SIZE = 640x1136)\nLaunchImage-800-667h@2x.png (MD5 = 9e395ad42d7a18b80eb6b3277fda2568, SIZE = 750x1334)\nLaunchImage-800-Portrait-736h@3x.png (MD5 = 98eb2eabf0b28f7b7cd2bf0461703705, SIZE = 1242x2208)\nLaunchImage-800-Landscape-736h@3x.png (MD5 = 044d1a4cfdecb9f902a05f35a0c3d3a4, SIZE = 2208x1242)"
      sort 14
      internal_name 'step_14'
      klass 'Steps::Step_14'
    end

    factory :appstore_check_step_15 do
      name 'Check iPad launch images'
      detail '-'
      expected "LaunchImage-Portrait~ipad.png (MD5 = 8914bc71fea59318dca3b09890c4ff30, SIZE = 768x1024)\nLaunchImage-Landscape~ipad.png (MD5 = 67023476a6dd24d8ee4d86243a6bac56, SIZE = 1024x768)\nLaunchImage-Portrait@2x~ipad.png (MD5 = 092ccfb6b9fbdbd7b0a5703e1d954d32, SIZE = 1536x2048)\nLaunchImage-Landscape@2x~ipad.png (MD5 = 24cee3bf1e2cc6c1797410334030addc, SIZE = 2048x1536)"
      sort 15
      internal_name 'step_15'
      klass 'Steps::Step_15'
    end

    factory :appstore_check_step_16 do
      name 'Check iTunes Artworks are not present'
      detail '-'
      expected "iTunesArtwork file is not present\niTunesArtwork@2x file is not present"
      sort 16
      internal_name 'step_16'
      klass 'Steps::Step_16'
    end

    factory :appstore_check_step_17 do
      name 'Check architecture #1'
      detail 'codesign -dvvv %{executable}'
      expected "Executable=%{executable}\nIdentifier=com.mimedia.iOSv2\nFormat=bundle with Mach-O universal (armv7 arm64)\nCodeDirectory v=20200 size=127681 flags=0x0(none) hashes=6375+5 location=embedded\nHash type=sha1 size=20\nCDHash=22b5005c5f6ec5da3c07190224757fe1c8999275\nSignature size=4333\nAuthority=iPhone Distribution: Mimedia Inc. (958B23VCBE)\nAuthority=Apple Worldwide Developer Relations Certification Authority\nAuthority=Apple Root CA\nSigned Time=22 Oct 2015 14:50:45\nInfo.plist entries=42\nTeamIdentifier=958B23VCBE\nSealed Resources version=2 rules=12 files=1067\nInternal requirements count=1 size=184\n"
      sort 17
      internal_name 'step_17'
      klass 'Steps::Step_17'
    end

    factory :appstore_check_step_18 do
      name 'Check architecture'
      detail 'otool -vh %{executable}'
      expected "%{executable} (architecture armv7):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\n   MH_MAGIC     ARM         V7  0x00     EXECUTE    51       5504   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n%{executable} (architecture arm64):\nMach header\n      magic cputype cpusubtype  caps    filetype ncmds sizeofcmds      flags\nMH_MAGIC_64   ARM64        ALL  0x00     EXECUTE    51       6208   NOUNDEFS DYLDLINK TWOLEVEL WEAK_DEFINES BINDS_TO_WEAK PIE\n"
      sort 18
      internal_name 'step_18'
      klass 'Steps::Step_18'
    end

    factory :appstore_check_step_19 do
      name 'Check ARM64 support'
      detail 'otool -vh %{executable}'
      expected "arm64 has been supported"
      sort 19
      internal_name 'step_19'
      klass 'Steps::Step_19'
    end

    factory :appstore_check_step_20 do
      name 'Check PIE binary'
      detail 'otool -vh %{executable}'
      expected "armv7 is present and has PIE binary support\narm64 is present and has PIE binary support"
      sort 20
      internal_name 'step_20'
      klass 'Steps::Step_20'
    end

    factory :appstore_check_step_21 do
      name 'Check short version'
      detail 'defaults read %{app}/Info.plist CFBundleShortVersionString'
      expected "%{short_version}\n"
      sort 21
      internal_name 'step_21'
      klass 'Steps::Step_21'
    end

  end

end
