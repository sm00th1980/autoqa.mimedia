# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :infrastructure do

    factory :infrastructure_api do
      name 'api.dev.mimedia.com'
      url 'https://api.dev.mimedia.com/2.0'
      success_result '{"session_expired":true}'
      primitive true
    end

    factory :infrastructure_node1 do
      name 'node1'
      url 'http://192.168.25.7:4723'
      success_result 'That URL did not map to a valid JSONWP resource'
      primitive true
    end

    factory :infrastructure_api_login do
      name 'api.dev.mimedia.com/login'
      url 'api.dev.mimedia.com/login'
      success_result ''
      primitive false
    end

  end

end
