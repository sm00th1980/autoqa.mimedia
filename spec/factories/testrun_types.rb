# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :testrun_type do

    factory :testrun_type_simulator_ios_7_x do
      name 'iOS 7.1 (Simulator)'
      internal_name 'simulator_ios_7_x'
    end

    factory :testrun_type_simulator_ios_8_x do
      name 'iOS 8.4 (Simulator)'
      internal_name 'simulator_ios_8_x'
    end

    factory :testrun_type_simulator_ios_9_x do
      name 'iOS 9.0 (Simulator)'
      internal_name 'simulator_ios_9_x'
    end

    factory :testrun_type_api do
      name 'API'
      internal_name 'api'
    end

    factory :testrun_type_device_ios_7_x do
      name 'iOS 7.x (Device)'
      internal_name 'device_ios_7_x'
    end

    factory :testrun_type_device_ios_8_x do
      name 'iOS 8.x (Device)'
      internal_name 'device_ios_8_x'
    end

    factory :testrun_type_device_ios_9_x do
      name 'iOS 9.x (Device)'
      internal_name 'device_ios_9_x'
    end

    factory :testrun_type_emulator_android_4_3 do
      name 'Android 4.3 - Jelly Bean (Emulator)'
      internal_name 'emulator_android_4_3'
    end

    factory :testrun_type_emulator_android_4_4 do
      name 'Android 4.4 - KitKat (Emulator)'
      internal_name 'emulator_android_4_4'
    end

    factory :testrun_type_emulator_android_5_0 do
      name 'Android 5.0 - Lollipop (Emulator)'
      internal_name 'emulator_android_5_0'
    end

    factory :testrun_type_emulator_android_5_1 do
      name 'Android 5.1 - Lollipop (Emulator)'
      internal_name 'emulator_android_5_1'
    end

  end

end
