# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :node do
    name 'node1'
    host '127.0.0.1'
    login 'deyarov'
    password 'Rv-*9K7_'
    upload_dir '/Users/deyarov/.tmp'
    home_dir '/Users/deyarov'

    factory :node_busy do
      after(:create) do |node|
        JenkinsJob.where(status: JobStatus.running, node_id: node.id).delete_all
        create(:running_jenkins_job, node_id: node.id)
      end
    end

    factory :node_free do
      after(:create) do |node|
        JenkinsJob.where(status: JobStatus.running, node_id: node.id).delete_all
      end
    end

  end

end
