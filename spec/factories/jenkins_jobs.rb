# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :jenkins_job do
    testrun
    scenario_id nil
    status { JobStatus.scheduled }

    factory :running_jenkins_job do
      start { Time.now }
      status { JobStatus.running }
      current_job_name { Faker::Name.name }
      node_id { create(:node).id }

      factory :running_jenkins_job_with_failed_testrun do
        after(:create) do |job|
          job.testrun = create(:testrun_failed)
          job.scenario_id = job.testrun.scenarios.first.id
          job.save!
        end
      end
    end

    factory :jenkins_job_finished_with_success do
      start { Time.now - 1.hour }
      stop { Time.now + 1.hour }
      status { JobStatus.finished_with_success }
      current_job_name { Faker::Name.name }
      node_id { create(:node).id }
    end

    factory :jenkins_job_finished_with_fail do
      start { Time.now - 1.hour }
      stop { Time.now + 1.hour }
      status { JobStatus.finished_with_fail }
      current_job_name { Faker::Name.name }
      node_id { create(:node).id }
    end

  end

end
