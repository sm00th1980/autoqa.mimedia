# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :step_file do
    sequence(:name) { |n| "name_#{n}" }
  end

end
