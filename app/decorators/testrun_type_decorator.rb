# -*- encoding : utf-8 -*-
require 'chronic_duration'

class TestrunTypeDecorator < ApplicationDecorator
  decorates :testrun_type
  delegate_all

  def status
    if model.unknown?
      h.content_tag(:span, "UNKNOWN", class: ['label', 'label-primary'])
    else
      if model.failed?
        h.content_tag(:span, "FAILED #{model.failed_scenarios_count}/#{model.total_scenarios_count}", class: ['label', 'label-danger'])
      else
        h.content_tag(:span, "PASSED #{model.passed_scenarios_count}/#{model.total_scenarios_count}", class: ['label', 'label-success'])
      end
    end
  end
  
  def details
    h.link_to model.name, h.ios_url
  end

end
