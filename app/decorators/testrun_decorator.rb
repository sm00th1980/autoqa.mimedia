# -*- encoding : utf-8 -*-
require 'chronic_duration'

class TestrunDecorator < ApplicationDecorator
  decorates :testrun
  delegate_all

  def status
    return h.content_tag(:span, content, class: ['label', 'label-danger']) if model.failed?
    return h.content_tag(:span, content, class: ['label', 'label-success']) if model.passed?
    return h.content_tag(:span, content, class: ['label', 'label-warning']) if model.cancelled?

    h.content_tag(:span, "UNKNOWN", class: ['label', 'label-primary'])
  end

  def last_run_at
    "#{h.distance_of_time_in_words(model.start, Time.now)} ago" if model.start
  end

  def duration
    ChronicDuration.output(model.last_duration)
  end

  def details
    if !model.unknown?
      h.link_to h.raw(model.name), h.testrun_path(model)
    else
      h.raw(model.name)
    end
  end

  def running_now
    if model.running?
      h.content_tag(:span, "YES", class: ['label', 'label-primary'])
    else
      h.content_tag(:span, "NO", class: ['label', 'label-info'])
    end
  end

  def action_button
    if model.scheduled?
      unschedule_button
    else
      if model.running?
        console
      else
        #not scheduled and not running
        h.content_tag :div, nil, class: 'dropdown' do
          h.raw([menu_top_button, menu_buttons].join)
        end
      end
    end
  end

  private
  def content
    "FAILED #{model.failed_scenarios_count}, PASSED: #{model.passed_scenarios_count}, CANCELLED: #{model.pending_scenarios_count}, TOTAL: #{model.total_scenarios_count}"
  end

  def unschedule_button
    h.link_to 'Unschedule', h.testrun_unschedule_path(model), class: 'btn btn-warning btn-sm', method: :post
  end

  def schedule_all_scenarios_button
    h.link_to 'All scenarios', h.testrun_schedule_path(model), method: :post
  end

  def schedule_only_failed_or_pending_scenarios_button
    h.link_to 'Only failed or cancelled scenarios', h.testrun_schedule_failed_or_pending_path(model), method: :post
  end

  def menu_top_button
    h.raw('<button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Schedule
              <span class="caret"></span>
            </button>')
  end

  def menu_buttons
    h.content_tag :ul, class: "dropdown-menu", :"aria-labelledby" => "dropdownMenu1" do
      h.raw([schedule_all_scenarios_button, schedule_only_failed_or_pending_scenarios_button].map { |b| h.content_tag(:li, b) }.join)
    end
  end

  def console
    jobs = JenkinsJob.where(status: JobStatus.running, testrun: model)

    h.content_tag :div, nil do
      h.raw(jobs.map { |job| console_button(job) }.join)
    end

  end

  def console_button(job)
    h.link_to 'Console', job.console_path, class: 'btn btn-link btn-sm', method: :get
  end

end
