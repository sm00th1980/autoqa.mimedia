# -*- encoding : utf-8 -*-
require 'chronic_duration'

class JenkinsJobDecorator < ApplicationDecorator
  decorates :jenkins_job
  delegate_all

  def status
    if model.status.running?
      return "running"
    end

    if model.status.scheduled?
      return "scheduled"
    end

    if model.status.finished_with_success?
      return "finished_with_success"
    end

    if model.status.finished_with_fail?
      return "finished_with_fail"
    end

    "unknown"
  end

  def duration
    ChronicDuration.output(model.duration)
  end

  def start_at
    model.start.strftime('%Y-%m-%d %H:%M:%S') if model.start
  end

  def scenario_name
    return scenario.name if scenario.present?
    'All'
  end

  def name
    "#{model.testrun.name} - #{model.testrun.type.name}"
  end

  def node_name
    return "#{model.node.name} (#{model.node.host})" if node.present?
    '-'
  end

end
