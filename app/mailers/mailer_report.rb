# -*- encoding : utf-8 -*-

class MailerReport < ActionMailer::Base

  def send_to_report_team(email, subject, message)
    send_email(email, Rails.configuration.mail_sender, subject, message, :report, [])
  end

  def send_email(email, from, subject, message, template, files)
    deliver_email(email, from, subject, message, template, files)
  end

  private
  def deliver_email(email, from, subject, locals, template, files)
    mail(:to => email, :from => from, :subject => subject) do |format|
      format.html { render template, layout: 'mail', locals: locals }
    end
  end

end
