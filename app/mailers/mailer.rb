# -*- encoding : utf-8 -*-

class Mailer < ActionMailer::Base

  def send_to_billing(email, subject, filename)
    send_email(email, Rails.configuration.mail_sender, subject, '', :billing, [filename])
  end

  def send_to_admin(email, subject, message)
    send_email(email, Rails.configuration.mail_sender, subject, message, :admin, [])
  end

  def send_email(email, from, subject, message, template, files)
    deliver_email(email, from, subject, {message: message}, template, files)
  end

  private
  def deliver_email(email, from, subject, locals, template, files)
    files.each do |file|
      name = file.split('/').last

      attachments[name] = {
          encoding: 'base64',
          content: Base64.encode64(File.read(file))
      }
    end

    mail(:to => email, :from => from, :subject => subject) do |format|
      format.html { render template, :locals => locals }
    end
  end

end
