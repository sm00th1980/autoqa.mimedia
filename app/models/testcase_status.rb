# -*- encoding : utf-8 -*-
class TestcaseStatus < ActiveRecord::Base

  has_many :testcases, :foreign_key => 'status_id'

  def self.passed
    find_by(internal_name: 'passed')
  end

  def self.pending
    find_by(internal_name: 'pending')
  end

  def self.broken
    find_by(internal_name: 'broken')
  end

  def self.unknown
    find_by(internal_name: 'unknown')
  end

  def self.broken_by_infrastructure
    find_by(internal_name: 'broken_by_infrastructure')
  end

  def passed?
    self == self.class.passed
  end

  def pending?
    self == self.class.pending
  end

  def broken?
    self == self.class.broken
  end

  def unknown?
    self == self.class.unknown
  end

  def broken_by_infrastructure?
    self == self.class.broken_by_infrastructure
  end

end
