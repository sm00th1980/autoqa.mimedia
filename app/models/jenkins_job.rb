# -*- encoding : utf-8 -*-
class JenkinsJob < ActiveRecord::Base
  include Rails.application.routes.url_helpers

  belongs_to :testrun
  belongs_to :status, class_name: 'JobStatus'

  #scheduling start
  def self.schedule(testrun)
    if !testrun.running? and !testrun.scheduled?
      create!(testrun: testrun, start: nil, stop: nil, status: JobStatus.scheduled, scenario_id: nil)
      return {success: true, message: I18n.t('testrun.success.schedulled')}
    else
      return {success: false, message: I18n.t('testrun.failure.already_schedulled')}
    end
  end

  def self.schedule_failed_or_pending(testrun)
    if !testrun.running? and !testrun.scheduled?
      scenario_ids = testrun.scenarios.select { |s| s.failed? or s.pending? }.map { |s| s.id }.sort
      if scenario_ids.count > 0
        scenario_ids.each do |scenario_id|
          create!(testrun: testrun, start: nil, stop: nil, status: JobStatus.scheduled, scenario_id: scenario_id)
        end
        return {success: true, message: I18n.t('testrun.success.schedulled')}
      else
        return {success: false, message: I18n.t('testrun.failure.without_failed_or_pending_scenarios')}
      end
    else
      return {success: false, message: I18n.t('testrun.failure.already_schedulled')}
    end
  end

  def self.unschedule(testrun)
    if testrun.scheduled? and !testrun.running?
      where(testrun: testrun, status: JobStatus.scheduled).delete_all
      return {success: true, message: I18n.t('testrun.success.unschedulled')}
    else
      return {success: false, message: I18n.t('testrun.failure.unschedulled')}
    end
  end

  def unschedule!
    if status.scheduled?
      self.delete
      return {success: true, message: I18n.t('job.success.unschedulled')}
    else
      return {success: false, message: I18n.t('job.failure.unschedulled')}
    end
  end

  #scheduling end

  def self.next
    where(status: JobStatus.scheduled).first if !Node.free.empty?
  end

  def run!(job_name=nil)
    if status.scheduled? and !Node.free.empty?
      self.start = Time.now
      self.status = JobStatus.running
      self.node_id = Node.free.first.id

      Jenkins::Manager.new(self, job_name).perform

      self.save!
    end
  end

  def finish_failed!
    self.stop = Time.now
    self.status = JobStatus.finished_with_fail
    self.save!
  end

  def data_fullpath
    if testrun.type.mobile?
      Rails.configuration.data_fullpath % current_job_name
    else
      #api
      current_job_name
    end
  end

  def finish_callback
    "curl #{Rails.configuration.hostname}#{api_v1_job_finished_path(id: id)}"
  end

  def maven_string(node=nil)
    if testrun.type.mobile?
      if testrun.type == TestrunType.simulator_ios_8_x
        platformVersion = '8.4'
      end

      if testrun.type == TestrunType.simulator_ios_9_x
        platformVersion = '9.0'
      end

      deviceName = testrun.name.gsub('+', ' Plus').split.join('_')

      node_ = node.present? ? node : self.node

      if scenario.blank?
        #for all scenarios
        mobile = testrun.name[0..3].downcase == 'ipad' ? '~@mobile' : '~@tablet'
        "clean test -Dcucumber.options=&quot;--tags #{mobile} --tags ~@ios_device_only&quot; -DargLine=&quot;-DplatformVersion=#{platformVersion} -DdeviceName=#{deviceName} -DjobID=#{id} -DnodeIP=#{node_.host} -DbuildPath=#{node_.home_dir}&quot;"
      else
        #for specific scenario
        "clean test -Dcucumber.options=&quot;--name '#{ERB::Util.html_escape(scenario.name)}'&quot; -DargLine=&quot;-DplatformVersion=#{platformVersion} -DdeviceName=#{deviceName} -DjobID=#{id} -DnodeIP=#{node_.host} -DbuildPath=#{node_.home_dir}&quot;"
      end
    end
  end

  def duration
    if start
      return TimeDifference.between(start, Time.now).in_seconds.round(0)
    end

    0
  end

  def scenario
    Scenario.find_by(id: scenario_id, testrun: testrun)
  end

  def node
    Node.find_by(id: node_id)
  end

  def console_path
    if status.running?
      host = Rails.configuration.jenkins[:host]
      port = Rails.configuration.jenkins[:port]

      "http://#{host}:#{port}/job/#{current_job_name}/1/console"
    end
  end

end
