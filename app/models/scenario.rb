# -*- encoding : utf-8 -*-
class Scenario < ActiveRecord::Base
  belongs_to :testrun
  has_many :testcases
  belongs_to :job, class_name: 'JenkinsJob'

  def self.clean
    where('stop < ?', (Time.now - 1.week)).find_each do |scenario|
      scenario.testcases.destroy_all
      scenario.delete
    end
  end

  def pending?
    return true if failed_by_appium? and !Infrastructure.ios_available?(start, stop)
    return true if !testcases.select { |t| t.status.broken_by_infrastructure? }.empty?
    return true if testcases.select { |t| t.status.pending? }.count > 0 and testcases.select { |t| t.status.pending? }.count == testcases.count

    false
  end

  def failed?
    return true if failed_by_appium? and Infrastructure.ios_available?(start, stop) and !pending?
    false
  end

  def passed?
    if !pending? and !failed?
      return true
    end

    false
  end

  def xunit
    {
        "uid" => allure_id,
        "name" => name,
        "title" => name,
        "time" => {
            "start" => start.to_i * 1000,
            "stop" => stop.to_i * 1000,
            "duration" => stop.to_i * 1000 - start.to_i * 1000
        },
        "statistic" => {
            "total" => testcases.count,
            "passed" => testcases.select { |s| s.status.passed? }.count,
            "pending" => testcases.select { |s| s.status.pending? }.count,
            "canceled" => 0,
            "failed" => testcases.select { |s| s.status.broken? or s.status.broken_by_infrastructure? }.count,
            "broken" => 0
        },
        "description" => nil,
        "testCases" => testcases.map { |testcase| testcase.xunit }
    }
  end

  def behavior
    {
        "uid" => allure_id,
        "title" => name,
        "statistic" => {
            "total" => testcases.count,
            "passed" => testcases.select { |s| s.status.passed? }.count,
            "pending" => testcases.select { |s| s.status.pending? }.count,
            "canceled" => 0,
            "failed" => testcases.select { |s| s.status.broken? or s.status.broken_by_infrastructure? }.count,
            "broken" => 0
        },
        "testCases" => testcases.map { |testcase| testcase.xunit }
    }
  end

end
