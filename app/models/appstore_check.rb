# -*- encoding : utf-8 -*-
class AppstoreCheck < ActiveRecord::Base
  scope :streaming_checks, -> { where.not(id: [prepare, step_2, step_17]).order(:sort) }

  def self.app_path
    "#{Node.appstore_node.upload_dir}/#{Rails.configuration.mac[:app_name]}.app"
  end

  def self.prepare
    find_by(internal_name: 'prepare')
  end

  def self.step_1
    find_by(internal_name: 'step_1')
  end

  def self.step_2
    find_by(internal_name: 'step_2')
  end

  def self.step_3
    find_by(internal_name: 'step_3')
  end

  def self.step_4
    find_by(internal_name: 'step_4')
  end

  def self.step_5
    find_by(internal_name: 'step_5')
  end

  def self.step_6
    find_by(internal_name: 'step_6')
  end

  def self.step_7
    find_by(internal_name: 'step_7')
  end

  def self.step_8
    find_by(internal_name: 'step_8')
  end

  def self.step_9
    find_by(internal_name: 'step_9')
  end

  def self.step_10
    find_by(internal_name: 'step_10')
  end

  def self.step_11
    find_by(internal_name: 'step_11')
  end

  def self.step_12
    find_by(internal_name: 'step_12')
  end

  def self.step_13
    find_by(internal_name: 'step_13')
  end

  def self.step_14
    find_by(internal_name: 'step_14')
  end

  def self.step_15
    find_by(internal_name: 'step_15')
  end

  def self.step_16
    find_by(internal_name: 'step_16')
  end

  def self.step_17
    find_by(internal_name: 'step_17')
  end

  def self.step_18
    find_by(internal_name: 'step_18')
  end

  def self.step_19
    find_by(internal_name: 'step_19')
  end

  def self.step_20
    find_by(internal_name: 'step_20')
  end

  def self.step_21
    find_by(internal_name: 'step_21')
  end

  def step(step_file)
    klass.constantize.new(self, step_file)
  end

  def self.showing_steps
    where.not(id: [step_2, step_17]).order(:sort).map { |check| check.step(StepFile.new) }
  end

end
