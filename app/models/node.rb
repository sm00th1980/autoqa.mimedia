# -*- encoding : utf-8 -*-
class Node < ActiveRecord::Base

  def busy?
    JenkinsJob.exists?(status: JobStatus.running, node_id: id)
  end

  def self.free
    where(disabled: false).select { |node| !node.busy? }
  end

  def self.appstore_node
    first
  end

end
