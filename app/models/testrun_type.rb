# -*- encoding : utf-8 -*-
class TestrunType < ActiveRecord::Base
  has_many :testruns, -> { where disabled: false }, foreign_key: 'type_id'

  def self.simulator_ios_7_x
    find_by(internal_name: 'simulator_ios_7_x')
  end

  def self.simulator_ios_8_x
    find_by(internal_name: 'simulator_ios_8_x')
  end

  def self.simulator_ios_9_x
    find_by(internal_name: 'simulator_ios_9_x')
  end

  def self.device_ios_7_x
    find_by(internal_name: 'device_ios_7_x')
  end

  def self.device_ios_8_x
    find_by(internal_name: 'device_ios_8_x')
  end

  def self.device_ios_9_x
    find_by(internal_name: 'device_ios_9_x')
  end

  def self.emulator_android_4_3
    find_by(internal_name: 'emulator_android_4_3')
  end

  def self.emulator_android_4_4
    find_by(internal_name: 'emulator_android_4_4')
  end

  def self.emulator_android_5_0
    find_by(internal_name: 'emulator_android_5_0')
  end

  def self.emulator_android_5_1
    find_by(internal_name: 'emulator_android_5_1')
  end

  def self.api
    find_by(internal_name: 'api')
  end

  def api?
    self == self.class.api
  end

  def mobile?
    !api?
  end

  def failed?
    #failed status if any testrun is failed
    if !unknown? and testruns.select { |testrun| testrun.failed? }.count > 0
      return true
    end

    false
  end

  def unknown?
    #unkown status if no testruns at all
    #or all testruns has unknown status
    if testruns.count == 0 or testruns.select { |testrun| testrun.unknown? }.count == testruns.count
      return true
    end

    false
  end

  def total_scenarios_count
    testruns.select { |testrun| !testrun.unknown? }.map { |testrun| testrun.total_scenarios_count }.sum
  end

  def failed_scenarios_count
    testruns.select { |testrun| !testrun.unknown? }.map { |testrun| testrun.failed_scenarios_count }.sum
  end

  def passed_scenarios_count
    testruns.select { |testrun| !testrun.unknown? }.map { |testrun| testrun.passed_scenarios_count }.sum
  end

  def duration
    testruns.map { |testrun| testrun.last_duration }.sum
  end

  def ios?
    [
        self.class.simulator_ios_7_x,
        self.class.simulator_ios_8_x,
        self.class.simulator_ios_9_x,
        self.class.device_ios_7_x,
        self.class.device_ios_8_x,
        self.class.device_ios_9_x
    ].include? self
  end

end
