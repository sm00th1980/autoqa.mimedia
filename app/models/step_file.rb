# -*- encoding : utf-8 -*-
class StepFile < ActiveRecord::Base
  def short_version
    version.split('.').first(3).join('.') rescue nil
  end
end
