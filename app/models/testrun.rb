# -*- encoding : utf-8 -*-
class Testrun < ActiveRecord::Base
  belongs_to :os_version
  belongs_to :type, class_name: 'TestrunType'
  has_many :jobs, class_name: 'JenkinsJob'

  scope :api, -> { where(type: TestrunType.api, disabled: false) }
  scope :mobile, -> { where.not(type: TestrunType.api).where(disabled: false) }

  def failed?
    return true if total_scenarios_count > 0 and failed_scenarios_count > 0
    false
  end

  def passed?
    return true if total_scenarios_count > 0 and failed_scenarios_count == 0 and pending_scenarios_count == 0
    false
  end

  def unknown?
    return true if total_scenarios_count == 0
    false
  end

  def cancelled?
    return true if total_scenarios_count > 0 and failed_scenarios_count == 0 and pending_scenarios_count > 0
    false
  end

  def total_scenarios_count
    failed_scenarios_count + passed_scenarios_count + pending_scenarios_count
  end

  def last_duration
    if start and stop
      return TimeDifference.between(start, stop).in_seconds.round(0)
    end

    0
  end

  def running?
    if JenkinsJob.where(status: JobStatus.running, testrun: self).count > 0
      return true
    end

    false
  end

  def scheduled?
    JenkinsJob.where(testrun: self, status: JobStatus.scheduled).count > 0 and !running?
  end

  def xunit_view
    {
        "start" => start.to_i * 1000,
        "stop" => stop.to_i * 1000,
        "duration" => stop.to_i * 1000 - start.to_i * 1000,
        "testSuites" => scenarios.map { |scenario| scenario.xunit }
    }
  end

  def environment_view
    {
        "id" => "",
        "name" => name,
        "parameter" => [{
                            "name" => "Report started at",
                            "key" => "report.time.start",
                            "value" => (start.strftime('%Y-%m-%d %H:%M:%S') rescue "")
                        }, {
                            "name" => "Report finished at",
                            "key" => "report.time.stop",
                            "value" => (stop.strftime('%Y-%m-%d %H:%M:%S') rescue "")
                        }
        ]
    }
  end

  def defects_view
    {
        "defectsList" => [
            {
                "title" => name,
                "status" => "BROKEN",
                "defects" => scenarios.select { |s| s.failed? }.map { |s| s.testcases }.flatten.select { |t| t.status.broken? }.map { |t| t.defects_view }
            }]
    }
  end

  def graph_view
    {"testCases" => scenarios.map { |s| s.testcases }.flatten.map { |t| t.xunit }}
  end

  def behavior_view
    {
        "features" => scenarios.map do |scenario|
          {
              "title" => scenario.name,
              "statistic" => {
                  "total" => scenario.testcases.count,
                  "passed" => scenario.testcases.select { |s| s.status.passed? }.count,
                  "pending" => scenario.testcases.select { |s| s.status.pending? }.count,
                  "canceled" => 0,
                  "failed" => scenario.testcases.select { |s| s.status.broken? or s.status.broken_by_infrastructure? }.count,
                  "broken" => 0
              },
              "stories" => [scenario.xunit]
          }
        end
    }
  end

  def report_view
    {"size" => 1000, "time" => 1000}
  end

  def scenarios
    scenarios_ = []

    recent_full_job_finished_with_success = JenkinsJob.where(testrun: self, status: JobStatus.finished_with_success, scenario_id: nil).order(:start).last
    if recent_full_job_finished_with_success.present?
      Scenario.where(job: recent_full_job_finished_with_success).find_each do |scenario|
        recent_restarted_scenario = Scenario.where(testrun: self, name: scenario.name).where('job_id > ?', scenario.job_id).order(:job_id).last
        if recent_restarted_scenario.present?
          scenarios_ << recent_restarted_scenario
        else
          scenarios_ << scenario
        end
      end
    end

    scenarios_
  end

end
