# -*- encoding : utf-8 -*-
class Testcase < ActiveRecord::Base
  belongs_to :scenario
  belongs_to :status, class_name: 'TestcaseStatus'

  def xunit
    {
        "uid" => data["uid"],
        "name" => data["name"],
        "title" => data["title"],
        "time" => data["time"],
        "severity" => data["severity"],
        "status" => data["status"]
    } rescue {}
  end

  def defects_view
    {
        "uid" => allure_id,
        "failure" => {
            "message" => (data["failure"]["message"] rescue ""),
            "stackTrace" => nil
        },
        "testCases" => [xunit]
    }
  end

end
