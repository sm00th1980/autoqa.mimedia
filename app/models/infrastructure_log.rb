# -*- encoding : utf-8 -*-
class InfrastructureLog < ActiveRecord::Base
  belongs_to :infrastructure

  def self.clean
    where('datetime < ?', (Time.now - 1.week)).delete_all
  end
end
