# -*- encoding : utf-8 -*-
class Infrastructure < ActiveRecord::Base
  has_many :logs, class_name: 'InfrastructureLog'

  scope :primitive, -> { where(primitive: true) }

  def available?(start_time, stop_time)
    total_count = InfrastructureLog.where('datetime between ? and ?', start_time, stop_time).where(infrastructure_id: self.id).count
    failed_count = InfrastructureLog.where('datetime between ? and ?', start_time, stop_time).where(infrastructure_id: self.id, available: false).count
    if total_count == 0
      return true
    else
      if failed_count > 0
        return false
      else
        return true
      end
    end
  end

  def self.available_last_5_minutes?
    api.available?(Time.now - 5.minutes, Time.now) and api_login.available?(Time.now - 5.minutes, Time.now) and node1.available?(Time.now - 5.minutes, Time.now)
  end

  def self.api
    find_by(name: "api.dev.mimedia.com")
  end

  def self.api_login
    find_by(name: "api.dev.mimedia.com/login")
  end

  def self.node1
    find_by(name: "node1")
  end

  def self.ios_available?(start_time, stop_time)
    api.available?(start_time, stop_time) and api_login.available?(start_time, stop_time) and node1.available?(start_time, stop_time)
  end

end
