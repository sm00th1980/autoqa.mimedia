# -*- encoding : utf-8 -*-
class JobStatus < ActiveRecord::Base

  has_many :jobs, class_name: 'JenkinsJob', foreign_key: 'status_id'

  def self.scheduled
    find_by(internal_name: 'scheduled')
  end

  def self.running
    find_by(internal_name: 'running')
  end

  def self.finished_with_success
    find_by(internal_name: 'finished_with_success')
  end

  def self.finished_with_fail
    find_by(internal_name: 'finished_with_fail')
  end

  def scheduled?
    self == self.class.scheduled
  end

  def running?
    self == self.class.running
  end

  def finished_with_success?
    self == self.class.finished_with_success
  end

  def finished_with_fail?
    self == self.class.finished_with_fail
  end

end
