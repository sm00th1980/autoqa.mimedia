# -*- encoding : utf-8 -*-
class UploadController < ApplicationController

  def upload
    if save_file_on_disk?
      step_file = StepFile.create!(name: @temp_filename, version: upload_params[:version], original_name: @original_name)
      render json: {success: true, file_id: step_file.id}
    else
      render json: {success: false, message: I18n.t('upload.failed')}
    end
  end

  private
  def save_file_on_disk?
    begin
      @temp_filename = temp_filename
      File.open(@temp_filename, 'wb') do |file|
        file.write(upload_params[:build].read)
      end
    rescue
      return false
    else
      if !File.exists?(@temp_filename)
        backup_upload
        return false if !File.exists?(@temp_filename)
      end
    end

    true
  end

  def upload_params
    params.permit(:build, :version)
  end

  def backup_upload
    rack_file = Rails.root.join('/tmp', upload_params[:build].tempfile.path.split('/').last)
    FileUtils.cp(rack_file, @temp_filename)
  end

  def temp_filename
    @original_name = upload_params[:build].original_filename rescue ""
    Rails.root.join('/tmp', Tempfile.new(@original_name).path.split('/').last).to_s
  end

end
