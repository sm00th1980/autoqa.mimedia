# -*- encoding : utf-8 -*-
class IosController < ApplicationController

  def index
    @testruns_simulator_ios_8_x = Testrun.where(type: TestrunType.simulator_ios_8_x, disabled: false).order(:sort).decorate
    @testruns_simulator_ios_9_x = Testrun.where(type: TestrunType.simulator_ios_9_x, disabled: false).order(:sort).decorate

    @testruns_device_ios_8_x = []
    @testruns_device_ios_9_x = []

    @testruns_ios = Testrun.where(type: [TestrunType.simulator_ios_8_x, TestrunType.simulator_ios_9_x], disabled: false)

    @expected = [1, 2, 3].to_xml
    @got = [1, 2, 3, 4].to_xml

    @jobs = JenkinsJob.where(status: [JobStatus.scheduled, JobStatus.running]).order(:created_at).decorate

    @steps = AppstoreCheck.showing_steps
  end

end
