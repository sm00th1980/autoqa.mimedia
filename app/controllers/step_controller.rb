# -*- encoding : utf-8 -*-
class StepController < ApplicationController
  include ActionController::Live

  before_action :find_file!, only: :send_message

  def send_message
    response.headers['Content-Type'] = 'text/event-stream'
    @sse = ServerSide::SSE.new(response.stream)
    begin
      streaming_steps if streaming_prepare
    rescue IOError
    ensure
      @sse.write({finished: true})
      @sse.close
    end
  end

  private
  def streaming_steps
    AppstoreCheck.streaming_checks.each do |check|
      step = check.step(@file)
      @sse.write({
                     id: step.id,
                     status: 'running',
                     name: step.name,
                     expected: nil,
                     command: step.command,
                     got: nil
                 })

      step.run
      @sse.write({
                     id: step.id,
                     status: step.status,
                     name: step.name,
                     expected: step.expected,
                     command: step.command,
                     got: step.got
                 })
    end
  end

  def streaming_prepare
    step = Steps::Prepare.new(nil, @file)
    @sse.write({
                   id: step.id,
                   status: 'running',
                   name: step.name,
                   expected: nil,
                   command: step.command,
                   got: nil
               })
    step.run
    @sse.write({
                   id: step.id,
                   status: step.status,
                   name: step.name,
                   expected: step.expected,
                   command: step.command,
                   got: step.got
               })

    step.result
  end

  def step_params
    params.permit(:id)
  end

  def find_file!
    @file = StepFile.find_by(id: step_params[:id])
    if @file.blank?
      render json: {success: false, error: I18n.t('step_file.failure.not_found') % step_params[:id]} and return
    end
  end

end


