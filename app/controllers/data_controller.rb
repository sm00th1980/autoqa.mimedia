# -*- encoding : utf-8 -*-
class DataController < ApplicationController

  before_action :testrun
  before_action :find_testcase, only: :testcase

  def environment
    render json: @testrun.environment_view
  end

  def report
    render json: @testrun.report_view
  end

  def xunit
    render json: @testrun.xunit_view
  end

  def defects
    render json: @testrun.defects_view
  end

  def graph
    render json: @testrun.graph_view
  end

  def behavior
    render json: @testrun.behavior_view
  end

  def testcase
    render json: @testcase.data
  end

  private
  def testcase_params
    params.permit(:id)
  end

  def testrun
    @testrun = Testrun.find_by(id: (request.referer.split('/').last.to_i rescue nil))
    if @testrun.blank?
      render json: {success: false, error: I18n.t('testrun.not_found')} and return
    end
  end

  def find_testcase
    @testcase = Testcase.find_by(allure_id: testcase_params[:id], scenario: @testrun.scenarios)
    if @testcase.blank?
      render json: {success: false, error: I18n.t('testcase.not_found')} and return
    end
  end

end
