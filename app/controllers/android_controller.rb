# -*- encoding : utf-8 -*-
class AndroidController < ApplicationController
  def index
    @testruns_emulator_android_4_3 = Testrun.where(type: TestrunType.emulator_android_4_3).order(:sort).decorate
    @testruns_emulator_android_4_4 = Testrun.where(type: TestrunType.emulator_android_4_4).order(:sort).decorate
    @testruns_emulator_android_5_0 = Testrun.where(type: TestrunType.emulator_android_5_0).order(:sort).decorate
    @testruns_emulator_android_5_1 = Testrun.where(type: TestrunType.emulator_android_5_1).order(:sort).decorate

    @testruns_android = Testrun.where(type: [TestrunType.emulator_android_4_3, TestrunType.emulator_android_4_4, TestrunType.emulator_android_5_0, TestrunType.emulator_android_5_1])
  end
end
