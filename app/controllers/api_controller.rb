# -*- encoding : utf-8 -*-
class ApiController < ApplicationController
  def index
    @testruns_api = Testrun.where(type: TestrunType.api).order(:sort).decorate
  end
end
