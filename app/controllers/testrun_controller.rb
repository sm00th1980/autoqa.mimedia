# -*- encoding : utf-8 -*-
class TestrunController < ApplicationController
  before_action :find_testrun!

  def index
    render layout: false
  end

  def schedule
    result = JenkinsJob.schedule(@testrun)

    if result[:success]
      flash[:notice] = result[:message]
    else
      flash[:alert] = result[:message]
    end

    redirect_to ios_path
  end

  def unschedule
    result = JenkinsJob.unschedule(@testrun)

    if result[:success]
      flash[:notice] = result[:message]
    else
      flash[:alert] = result[:message]
    end

    redirect_to ios_path
  end

  def schedule_failed_or_pending
    result = JenkinsJob.schedule_failed_or_pending(@testrun)

    if result[:success]
      flash[:notice] = result[:message]
    else
      flash[:alert] = result[:message]
    end

    redirect_to ios_path
  end

  private
  def testrun_params
    params.permit(:id)
  end

  def find_testrun!
    @testrun = Testrun.find_by(id: testrun_params[:id])
    if @testrun.blank?
      redirect_to ios_path, alert: I18n.t('testrun.failure.not_found') % testrun_params[:id]
    end
  end

end
