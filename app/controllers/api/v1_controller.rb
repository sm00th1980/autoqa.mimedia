# -*- encoding : utf-8 -*-
class Api::V1Controller < ApplicationController
  before_action :find_job!
  before_action :check_job_is_running!, only: [:restart_appium_for_simulator, :job_finished]
  before_action :check_job_is_scheduled!, only: [:job_unschedule]

  def restart_appium_for_simulator
    node = Node.find_by(id: @job.node_id)
    if node
      Appium.new(node).restart
      render json: {success: true, message: I18n.t('job.success.appium_restarted') % node.name}
    end
  end

  def job_finished
    LoaderMobile.perform_async(@job.id)
    render json: {success: true, message: "starting collect data for testrun:<#{@job.testrun.name}>"}
  end

  def job_unschedule
    result = @job.unschedule!
    render json: {success: result[:success], message: result[:message]}
  end

  private
  def api_params
    params.permit(:id)
  end

  def find_job!
    @job = JenkinsJob.find_by(id: api_params[:id])
    if @job.blank?
      render json: {success: false, message: I18n.t('job.failure.not_found') % api_params[:id]}
    end
  end

  def check_job_is_running!
    if !@job.status.running?
      render json: {success: false, message: I18n.t('job.failure.not_running')}
    end
  end

  def check_job_is_scheduled!
    if !@job.status.scheduled?
      render json: {success: false, message: I18n.t('job.failure.not_scheduled')}
    end
  end
end
