# -*- encoding : utf-8 -*-
require 'chronic_duration'

module RootHelper

  def total_duration(testruns)
    total_in_seconds = testruns.map { |testrun| testrun.last_duration }.sum
    ChronicDuration.output(total_in_seconds)
  end

  def total_scenarios(testruns)
    testruns.map { |testrun| testrun.total_scenarios_count }.sum
  end

  def failed_scenarios(testruns)
    testruns.map { |testrun| testrun.failed_scenarios_count }.sum
  end

end
