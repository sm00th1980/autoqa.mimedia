# -*- encoding : utf-8 -*-

class InfrastructurePoller
  include Sidekiq::Worker

  def perform(infrastructure_ids=Infrastructure.primitive.pluck(:id))
    poll_status(infrastructure_ids)
  end

  def poll_status(infrastructure_ids)
    Infrastructure.where(id: infrastructure_ids).find_each do |infrastructure|
      if avalailable?(infrastructure)
        InfrastructureLog.create!(infrastructure: infrastructure, available: true, datetime: Time.now)
      else
        InfrastructureLog.create!(infrastructure: infrastructure, available: false, datetime: Time.now)
      end
    end
  end

  def avalailable?(infrastructure)
    result = false

    begin
      response = Typhoeus.get(infrastructure.url, followlocation: true, ssl_verifyhost: 0, ssl_verifypeer: false)
    rescue
    end

    if response.present? and response.body == infrastructure.success_result
      result = true
    end

    result
  end

end
