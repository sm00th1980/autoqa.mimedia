# -*- encoding : utf-8 -*-
require 'net/ssh/session'

class Build::Compilator

  APP = 'MiMedia'

  def initialize
    @node = Node.appstore_node
  end

  def compile
    via_ssh do |session|
      session.run(chdir_to_ios_project)
      session.run(git_update)
      session.run(clean_build_dir)
      session.run(build)
      session.run(chdir_to_ios_build)
      session.run(zip_remote)
    end
  end

  def via_ssh
    session = Net::SSH::Session.new(@node.host, @node.login, @node.password, port: 22)
    session.open
    session.export('PATH', '$PATH:$HOME/.rvm/bin:/usr/bin:/usr/local/bin')
    yield(session)
    session.close
  end

  def build
    project = "#{dir}/#{APP}.xcodeproj"
    "xcodebuild -project #{project} -configuration UITesting -target #{APP} -arch i386 -sdk iphonesimulator9.0"
  end

  def dir
    "#{@node.home_dir}/projects/iOSClient_V2"
  end

  def clean_build_dir
    "rm -rf #{dir}/build"
  end

  def zip_remote
    output = "#{@node.home_dir}/projects/Mobile_AutomatedTest/ios/prebuilds/#{APP}_i386.app.zip"
    input = "#{APP}.app"

    "zip -r #{output} #{input}"
  end

  def chdir_to_ios_build
    "cd #{dir}/build/UITesting-iphonesimulator"
  end

  def chdir_to_ios_project
    "cd #{dir}"
  end

  def git_update
    "git pull"
  end
end
