# -*- encoding : utf-8 -*-
require 'net/scp'

class Build::Distributor

  def initialize
    @compile_node = Node.appstore_node
    @nodes = Node.where(disabled: false)
  end

  def distribute
    Build::Compilator.new.compile

    download
    @nodes.each do |node|
      upload(node)
    end
  end

  def download
    Net::SCP.download!(@compile_node.host, @compile_node.login, remote_path(@compile_node.home_dir), local_path, ssh: { password: @compile_node.password })
  end

  def upload(node)
    Net::SCP.upload!(node.host, node.login, local_path, remote_path(node.home_dir), {ssh: {password: node.password}})
  end

  def remote_path(dir)
    "#{dir}/projects/Mobile_AutomatedTest/ios/prebuilds/#{build_name}"
  end

  def local_path
    Rails.root.join('tmp', build_name).to_s
  end

  def build_name
    "#{Build::Compilator::APP}_i386.app.zip"
  end

end
