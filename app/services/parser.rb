# -*- encoding : utf-8 -*-
require 'json'

class Parser

  def self.testcase_status(data)
    json = JSON.parse(data) rescue nil

    return TestcaseStatus.pending if pending?(json)
    return TestcaseStatus.passed if passed?(json)

    if broken?(json)
      return TestcaseStatus.broken_by_infrastructure if infrastructure_problem?(json)
      return TestcaseStatus.broken
    end

    TestcaseStatus.unknown
  end

  private
  def self.infrastructure_problem?(json)
    message_ = json["failure"]["message"] rescue nil

    if message_ and ["sessionnotfoundexception:", "webdriverexception:", "unreachablebrowserexception:"].include? message_.split.first.downcase
      return true
    end

    false
  end

  def self.pending?(json)
    begin
      return true if json["status"] == "PENDING"
    rescue
    end

    false
  end

  def self.passed?(json)
    begin
      return true if json["status"] == "PASSED"
    rescue
    end

    false
  end

  def self.broken?(json)
    begin
      return true if json["status"] == "BROKEN"
    rescue
    end

    false
  end

end
