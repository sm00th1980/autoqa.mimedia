# -*- encoding : utf-8 -*-
require 'zip'
require 'ssh_endpoint'

module Steps
  class Prepare

    attr_reader :result

    #ZIPPED_FILE = "%s.zip" % Rails.configuration.mac[:app_name]

    def initialize(check, step_file)
      @step_file = step_file
      @temp_dir = create_temp_dir
      @result = nil
    end

    def run
      if @step_file.name.blank? or !File.exists?(@step_file.name)
        raise("Filename with name <%s> not found" % @step_file.name)
      end

      @result = false

      begin
        unzip(@step_file.name, @temp_dir)
      rescue
        clean_on_failure
      else
        @step_file.local_path = "%s/Payload" % @temp_dir
        @step_file.save!

        begin
          clean_remote_dir
          SshEndpoint.upload(Rails.root.join("%s/Payload" % @temp_dir, "%s.app" % Rails.configuration.mac[:app_name]), Node.appstore_node)
        rescue
          clean_on_failure
        else
          @result = true
        end
      end

      @result
    end

    def expected
      "-"
    end

    def got
      "-"
    end

    def command
      "-"
    end

    def id
      AppstoreCheck.prepare.id
    end

    def name
      AppstoreCheck.prepare.name
    end

    def status
      return 'unknown' if @result.nil?
      return 'passed' if @result == true
      'failed'
    end

    private
    def unzip(source_file, dest_dir)
      #1. extract MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.ipa
      Zip::File.open(source_file) do |zip_file|
        zip_file.each do |entry|
          if entry.name == zipped_file
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #2. unzip MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.ipa
      if File.exist?(Rails.root.join(@temp_dir, zipped_file))
        Zip::File.open(Rails.root.join(@temp_dir, zipped_file)) do |zip_file|
          zip_file.each do |entry|
            entry.extract("#{dest_dir}/#{entry.name}")
          end
        end
      end

      #3. drop MiMedia.zip
      FileUtils.rm(Rails.root.join(@temp_dir, zipped_file))
    end

    def clean_remote_dir
      SshEndpoint.execute("rm -rf #{Node.appstore_node.upload_dir}", Node.appstore_node)
    end

    def create_temp_dir
      temp_dir_ = File.join('/tmp', Tempfile.new('').path.split('/').last)
      FileUtils.remove_entry(temp_dir_) if Dir.exist?(temp_dir_) or File.exist?(temp_dir_)
      Dir.mkdir(temp_dir_, 0700)
      temp_dir_
    end

    def clean_on_failure
      FileUtils.remove_entry(@temp_dir)
    end

    def zipped_file
      #remove extension by last dot -> remain only name of file
      "%s.ipa" % @step_file.original_name.split('.')[0..-2].join('.')
    end

  end
end
