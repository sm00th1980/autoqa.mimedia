# -*- encoding : utf-8 -*-
module Steps
  class Step_6 < Base::StepWithDefaults

    private
    def params
      version = @step_file.version rescue nil
      {app: "#{AppstoreCheck.app_path}", version: version}
    end

  end
end
