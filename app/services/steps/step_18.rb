# -*- encoding : utf-8 -*-
module Steps
  class Step_18 < Base::StepWithDefaults

    private
    def params
      {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
    end

  end
end
