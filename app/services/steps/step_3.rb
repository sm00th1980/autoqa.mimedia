# -*- encoding : utf-8 -*-
module Steps
  class Step_3 < Base::StepWithDefaults

    private
    def params
      {path: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
    end

  end
end
