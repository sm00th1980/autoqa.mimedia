# -*- encoding : utf-8 -*-
module Steps
  class Step_2 < Base::StepWithDefaults

    def got
      output.encode("UTF-8", :invalid => :replace, :undef => :replace, :replace => "?").gsub("\t", '        ').gsub("\u0000", '').gsub("\u0002", '').gsub("\u0017", '')
    end

    private
    def params
      {app: AppstoreCheck.app_path, executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
    end

  end
end
