# -*- encoding : utf-8 -*-
module Steps
  class Step_14 < Base::StepIcon

    private
    def icons
      [
          'LaunchImage.png',
          'LaunchImage@2x.png',
          'LaunchImage-568h@2x.png',
          'LaunchImage-800-667h@2x.png',
          'LaunchImage-800-Portrait-736h@3x.png',
          'LaunchImage-800-Landscape-736h@3x.png'
      ]
    end

  end
end
