# -*- encoding : utf-8 -*-
require 'pngdefry'

module Steps
  module Base
    class StepIcon

      attr_reader :got, :expected, :result

      def initialize(check, step_file)
        @check = check
        @result = nil

        @local_path = step_file.local_path
      end

      def id
        @check.id
      end

      def name
        @check.name
      end

      def command
        @check.read_attribute_before_type_cast('detail')
      end

      def run
        begin
          @expected = @check.read_attribute_before_type_cast('expected')
          @got = check_icons
        rescue
        else
          #no exception
          if @expected == @got
            @result = true
          else
            @result = false
          end
        end
      end

      def status
        return 'failed' if result == false
        return 'passed' if result == true
        'unknown'
      end

      private
      def path
        "%s/%s" % [@local_path, ("%s.app" % Rails.configuration.mac[:app_name])]
      end

      def check_icon(icon)
        icon_path = "#{path}/#{icon}"

        if File.exist?(icon_path)
          md5 = Digest::MD5.file(icon_path).to_s

          width = Pngdefry.dimensions(icon_path).first
          height = Pngdefry.dimensions(icon_path).last
        else
          md5 = nil
          width = nil
          height = nil
        end

        {exist: File.exist?(icon_path), md5: md5, width: width, height: height}

      end

      def check_icons
        outputs = []

        icons.each do |icon|
          result = check_icon(icon)
          if result[:exist]
            outputs << "#{icon} (MD5 = #{result[:md5]}, SIZE = #{result[:width]}x#{result[:height]})"
          else
            outputs << "#{icon} not exist"
          end
        end

        outputs.join("\n")
      end

      def icons
        raise "Should be overrided in subclasses"
      end
    end
  end
end
