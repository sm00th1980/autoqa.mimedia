# -*- encoding : utf-8 -*-
module Steps
  class Step_16 < Base::StepIcon

    private
    def check_icons
      outputs = []

      icons.each do |icon|
        result = check_icon(icon)
        if result[:exist]
          outputs << "#{icon} file is present"
        else
          outputs << "#{icon} file is not present"
        end
      end

      outputs.join("\n")
    end

    def icons
      ['iTunesArtwork', 'iTunesArtwork@2x']
    end

  end
end
