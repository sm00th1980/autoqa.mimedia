# -*- encoding : utf-8 -*-
module Steps
  class Step_19 < Base::StepWithDefaults

    def got
      if output.include?('arm64')
        return 'arm64 has been supported'
      end

      'arm64 has not been supported'
    end

    private
    def params
      {executable: "#{AppstoreCheck.app_path}/#{Rails.configuration.mac[:app_name]}"}
    end

  end
end
