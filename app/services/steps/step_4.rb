# -*- encoding : utf-8 -*-
module Steps
  class Step_4 < Base::StepWithDefaults

    def got
      output.gsub("\t", '        ')
    end

    private
    def params
      {provision: "#{AppstoreCheck.app_path}/embedded.mobileprovision"}
    end

  end
end
