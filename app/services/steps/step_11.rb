# -*- encoding : utf-8 -*-
require 'pngdefry'

module Steps
  class Step_11 < Base::StepIcon

    private
    def icons
      ['AppIcon60x60@2x.png', 'AppIcon60x60@3x.png']
    end
  end
end
