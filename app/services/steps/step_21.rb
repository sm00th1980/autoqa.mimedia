# -*- encoding : utf-8 -*-
module Steps
  class Step_21 < Base::StepWithDefaults

    private
    def params
      short_version = @step_file.short_version rescue nil
      {app: "#{AppstoreCheck.app_path}", short_version: short_version}
    end

  end
end
