# -*- encoding : utf-8 -*-
module Steps
  class Step_15 < Base::StepIcon

    private
    def icons
      [
          'LaunchImage-Portrait~ipad.png',
          'LaunchImage-Landscape~ipad.png',
          'LaunchImage-Portrait@2x~ipad.png',
          'LaunchImage-Landscape@2x~ipad.png'
      ]
    end

  end
end
