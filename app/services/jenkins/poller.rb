# -*- encoding : utf-8 -*-
require 'jenkins_api_client'

class Jenkins::Poller
  include Sidekiq::Worker

  def perform
    JenkinsJob.where(status: JobStatus.running).find_each do |running_job|
      client = JenkinsApi::Client.new(server_ip: Rails.configuration.jenkins[:host], username: Rails.configuration.jenkins[:username], password: Rails.configuration.jenkins[:password])
      begin
        status = client.job.status(running_job.current_job_name)
      rescue JenkinsApi::Exceptions::NotFound
        finish_job(running_job)
      rescue
      else
        #no exception
        finish_job(running_job) if status.downcase != 'running'
      end
    end
  end

  private
  def finish_job(job)
    job.finish_failed! if job.reload.status.running?
  end

end

