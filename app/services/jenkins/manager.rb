# -*- encoding : utf-8 -*-
require 'jenkins_api_client'
require 'erb'

class Jenkins::Manager

  attr_reader :current_job_name

  def initialize(job, job_name=nil)
    @job = job
    @client = JenkinsApi::Client.new(server_ip: Rails.configuration.jenkins[:host], username: Rails.configuration.jenkins[:username], password: Rails.configuration.jenkins[:password])
    @current_job_name = job_name.blank? ? new_job_name : job_name
  end

  def perform
    Repo.new.update
    delete
    create
    build
  end

  def config(node=nil)
    ERB.new(File.read('app/views/jenkins/config.xml.erb')).result(get_binding(node))
  end

  def get_binding(node=nil)
    finish_callback = @job.finish_callback
    maven_string = @job.maven_string(node)
    url = "#{Rails.configuration.git[:host]}:#{Rails.configuration.git[:path]}"
    branch = "*/#{Rails.configuration.git[:branch]}"
    
    binding
  end

  def delete
    @client.job.delete(@current_job_name) rescue nil
  end

  private
  def build
    @client.job.build(@current_job_name)
  end

  def create
    @job.current_job_name = @current_job_name
    @job.save!

    @client.job.create(@current_job_name, config)
  end

  def new_job_name
    Tempfile.new("temp_job_name_").path.split('/').last
  end

end
