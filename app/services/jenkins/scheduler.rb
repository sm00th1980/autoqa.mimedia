# -*- encoding : utf-8 -*-

class Jenkins::Scheduler

  def self.run(job_name=nil)
    JenkinsJob.next.run!(job_name) if JenkinsJob.next
  end

end
