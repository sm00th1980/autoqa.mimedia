# -*- encoding : utf-8 -*-

class InfrastructureLoginPoller
  include Sidekiq::Worker

  def perform
    InfrastructureLog.create!(infrastructure: Infrastructure.api_login, available: login_success?, datetime: Time.now)
  end

  private
  def session_api_key
    begin
      response = Typhoeus.post("#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:session_postfix]}",
                               followlocation: true,
                               ssl_verifyhost: 0,
                               ssl_verifypeer: false,
                               headers: {:"X-MiMedia-API-Key" => Rails.configuration.api_login[:api_key]}
      )
    rescue
    end

    JSON.parse(response.body)["session"]["id"] rescue ''
  end

  def login_success?
    begin
      response = Typhoeus.post("#{Rails.configuration.api_login[:host]}/#{Rails.configuration.api_login[:login_postfix]}",
                               followlocation: true,
                               ssl_verifyhost: 0,
                               ssl_verifypeer: false,
                               body: {:"account" => {:"emailAddress" => Rails.configuration.api_login[:login], :"password" => Rails.configuration.api_login[:password]}}.to_json,
                               headers: {:"X-MiMedia-Session-Key" => session_api_key}
      )
    rescue
    end

    if Rails.configuration.api_login[:login] == (JSON.parse(response.body)["account"]["emailAddress"] rescue nil)
      return true
    end

    false
  end

end
