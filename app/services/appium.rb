# -*- encoding : utf-8 -*-
require 'net/ssh/session'

class Appium

  def initialize(node)
    @node = node
  end

  def restart
    via_ssh do |session|
      stop_appium(session)
      sleep 1
      start_appium(session)
    end
  end

  def stop_appium(session)
    session.run("/usr/local/bin/pm2 stop 0")
  end

  def start_appium(session)
    session.run("/usr/local/bin/pm2 start /usr/local/bin/appium -- --session-override --full-reset --log-timestamp --local-timezone")
  end

  def via_ssh
    session = Net::SSH::Session.new(@node.host, @node.login, @node.password, port: 22)
    session.open
    session.export('PATH', '$PATH:$HOME/.rvm/bin:/usr/bin:/usr/local/bin')
    yield(session)
    session.close
  end

end
