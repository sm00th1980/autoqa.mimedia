# -*- encoding : utf-8 -*-
class Report

  def self.send_to_report_team
    Rails.configuration.report_team.each do |email_to|
      MailerReport.send_to_report_team(email_to, subject, message).deliver_now
    end
  end

  def self.subject
    '[AutoQA] iOS daily report'
  end

  def self.message
    {
        testrun_types: TestrunTypeDecorator.decorate_collection([TestrunType.simulator_ios_8_x, TestrunType.simulator_ios_9_x, TestrunType.device_ios_8_x, TestrunType.device_ios_9_x])
    }
  end

end
