# -*- encoding : utf-8 -*-
require 'git'

class Repo

  def initialize
    @git = Git.open(Rails.configuration.git[:path], :log => Logger.new(STDOUT))
  end

  def update
    @git.pull('origin', Rails.configuration.git[:branch])
  end

end
