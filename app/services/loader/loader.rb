# -*- encoding : utf-8 -*-
require 'json'

module Loader::Loader

  def load_data(job_id)
    job = JenkinsJob.find_by(id: job_id)
    xunit = JSON.parse(File.read(Rails.root.join(job.data_fullpath, "xunit.json"))) rescue nil

    if job.present? and job.testrun and xunit.present? and xunit["testSuites"].present?
      if job.scenario.present?
        load_specific_scenario(job, xunit)
      else
        load_all_scenarios(job, xunit)
      end
    end
  end

  def load_testcase(testcase_allure_id, scenario, job)
    data = JSON.parse(File.read(File.join(job.data_fullpath, "#{testcase_allure_id}-testcase.json"))) rescue nil

    if data.present?
      Testcase.create!(
          scenario: scenario,
          data: data,
          allure_id: data["uid"],
          status: Parser.testcase_status(data.to_json)
      )
    end
  end

  def load_all_scenarios(job, data)
    data["testSuites"].each do |testSuite|
      start = DateTime.strptime((testSuite["time"]["start"].to_f / 1000).to_s, "%s").to_time
      stop = DateTime.strptime((testSuite["time"]["stop"].to_f / 1000).to_s, "%s").to_time

      passed = testSuite["statistic"]["passed"].to_i
      total = testSuite["statistic"]["total"].to_i

      failed_by_appium = total != passed ? true : false

      testcase_allure_ids = testSuite["testCases"].map { |testcase| testcase["uid"] rescue nil }.compact

      new_scenario = Scenario.create!(
          allure_id: testSuite["uid"],
          name: testSuite["name"],
          start: start,
          stop: stop,
          testrun: job.testrun,
          failed_by_appium: failed_by_appium,
          job: job
      )

      testcase_allure_ids.each do |testcase_allure_id|
        load_testcase(testcase_allure_id, new_scenario, job)
      end
    end
  end

  def load_specific_scenario(job, data)
    data["testSuites"].each do |testSuite|
      start = job.start
      stop = Time.now

      passed = testSuite["statistic"]["passed"].to_i
      total = testSuite["statistic"]["total"].to_i

      failed_by_appium = total != passed ? true : false

      testcase_allure_ids = testSuite["testCases"].map { |testcase| testcase["uid"] rescue nil }.compact

      new_scenario = Scenario.create!(
          allure_id: testSuite["uid"],
          name: testSuite["name"],
          start: start,
          stop: stop,
          testrun: job.testrun,
          failed_by_appium: failed_by_appium,
          job: job
      )

      testcase_allure_ids.each do |testcase_allure_id|
        load_testcase(testcase_allure_id, new_scenario, job)
      end
    end
  end

end
