# -*- encoding : utf-8 -*-
class NightRun

  BEGIN_HOUR, BEGIN_MIN, BEGIN_SEC = 9, 0, 0 #start of day
  END_HOUR, END_MIN, END_SEC = 21, 0, 0 #end of day

  def self.perform
    if night_time?
      Testrun.where(disabled: false).find_each do |testrun|
        JenkinsJob.schedule(testrun) if testrun.type.ios?
      end
    end
  end

  def self.night_time?
    now = Time.now
    !now.between?(now.change(hour: BEGIN_HOUR, min: BEGIN_MIN, sec: BEGIN_SEC), now.change(hour: END_HOUR, min: END_MIN, sec: END_SEC))
  end

end
