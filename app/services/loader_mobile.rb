# -*- encoding : utf-8 -*-
class LoaderMobile
  include Sidekiq::Worker
  include Loader::Loader

  def perform(job_id)
    running_job = JenkinsJob.find_by(id: job_id)
    if running_job and running_job.status.running?
      load_data(running_job.id)

      #drop job into jenkins
      Jenkins::Manager.new(running_job, running_job.current_job_name).delete

      #save results to testrun
      testrun = running_job.testrun.reload

      if running_job.scenario.blank?
        testrun.start = running_job.start
        testrun.stop = Time.now
      end

      running_job.stop = Time.now
      running_job.status = JobStatus.finished_with_success
      running_job.save!

      testrun.failed_scenarios_count = testrun.scenarios.select { |scenario| scenario.failed? }.count
      testrun.passed_scenarios_count = testrun.scenarios.select { |scenario| scenario.passed? }.count
      testrun.pending_scenarios_count = testrun.scenarios.select { |scenario| scenario.pending? }.count
      testrun.save!
    end
  end

end
