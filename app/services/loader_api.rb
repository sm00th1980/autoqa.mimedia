# -*- encoding : utf-8 -*-
require 'json'
require 'net/ssh'
require 'tempfile'
require 'net/scp'
require 'zip'

class LoaderApi

  def self.load
    clean

    #copy from remote
    local_zip_file = copy_from_remote

    #unzipping
    unzip(local_zip_file, "#{Rails.root}/tmp")
    parse_into_db
    clean
  end

  def self.copy_from_remote
    host = Rails.configuration.api[:host]
    username = Rails.configuration.api[:username]

    remote_zip_file = remote_zip_filename
    local_zip_file = local_zip_filename
    Net::SSH.start(host, username) do |ssh|
      puts "Zipping on remote:#{api_path}/*.json"
      ssh.exec!("/usr/bin/zip -j #{remote_zip_file} #{api_path}/*.json")
      puts "Zipped on remote"

      #coping zip
      Net::SCP.download!(host, username, remote_zip_file, local_zip_file)
      puts "Downloaded from remote"

      #remove remote zip file
      ssh.exec!("rm #{remote_zip_file}")
    end

    local_zip_file
  end

  def self.parse_into_db
    #stats
    ['behavior', 'defects', 'environment', 'graph', 'report', 'xunit'].each do |report|
      Testrun.api.find_each do |testrun|
        filepath = Rails.root.join(testrun.data_fullpath, "#{report}.json")
        data = JSON.parse(File.read(filepath)) rescue nil
        if data.present?
          testrun.send("#{report}=", data)
          testrun.save!
        end
      end
    end

    #testcases
    Testrun.api.find_each do |testrun|
      testcases = []
      for filepath in Dir.glob(File.join(testrun.data_fullpath, '*-testcase.json'))
        data = JSON.parse(File.read(filepath)) rescue nil
        testcases << data if data.present?
      end

      Testcase.where(testrun: testrun).delete_all
      testcases.each do |testcase|
        Testcase.create!(testrun: testrun, data: testcase, allure_id: testcase["uid"])
      end
    end

    puts "Parsed into DB"
  end

  def self.api_path
    host = Rails.configuration.api[:host]
    username = Rails.configuration.api[:username]

    last_build = nil
    Net::SSH.start(host, username) do |ssh|
      last_build = ssh.exec!("ls #{Rails.configuration.api[:start_path]}").split.map { |build| Integer(build) rescue nil }.compact.max
    end

    "#{Rails.configuration.api[:start_path]}/#{last_build}/#{Rails.configuration.api[:end_path]}"
  end

  def self.remote_zip_filename
    "/tmp/#{Tempfile.new("allure_data_json_").path.split('/').last}.zip"
  end

  def self.local_zip_filename
    "#{Rails.root}/tmp/#{Tempfile.new("allure_data_json_").path.split('/').last}.zip"
  end

  def self.clean
    #clean tmp dir
    for filename in Dir.glob(File.join("#{Rails.root}/tmp", '*.zip'))
      FileUtils.rm(filename)
    end

    for filename in Dir.glob(File.join("#{Rails.root}/tmp", '*.json'))
      FileUtils.rm(filename)
    end
  end

  def self.unzip(source_file, dest_dir)
    Zip::File.open(source_file) do |zip_file|
      zip_file.each do |entry|
        puts "Extracting #{entry.name}"
        entry.extract("#{dest_dir}/#{entry.name}")
      end
    end

    puts "Unzipped on local"
  end
end

