var QueueRow = React.createClass({

    _status: function () {
        switch (this.props.job.status) {
            case 'running':
                return (<span className="label label-primary">RUNNING</span>);
                break;
            case 'scheduled':
                return (<span className="label label-default">SCHEDULED</span>);
                break;
            case 'finished_with_success':
                return (<span className="label label-success">FINISHED WITH SUCCESS</span>);
                break;
            case 'finished_with_fail':
                return (<span className="label label-danger">FINISHED WITH FAIL</span>);
                break;
            default:
                return (<span className="label label-warning">UNKNOWN</span>);
        }
    },

    _style: function () {
        return {verticalAlign: 'middle'};
    },

    _showRow: function () {
        switch (this.props.job.status) {
            case 'running':
                return true;
                break;
            case 'scheduled':
                return true;
                break;
            default:
                return false;
        }
    },

    _onClick: function () {
        this.props.onUnschedule(this.props.job.id);
    },

    _button: function () {
        if (this.props.job.status === 'scheduled') {
            return ( <button className="btn btn-warning btn-sm" onClick={this._onClick}>Unschedule</button> )
        }
        return null;
    },

    render: function () {
        if (this._showRow()) {
            return (
                <tr>
                    <td style={this._style()}>
                        {this.props.job.name}
                    </td>
                    <td style={this._style()}>
                        {this.props.job.current_job_name}
                    </td>
                    <td style={this._style()}>
                        {this._status()}
                    </td>
                    <td style={this._style()}>
                        {this.props.job.start_at}
                    </td>
                    <td style={this._style()}>
                        {this.props.job.duration}
                    </td>
                    <td style={this._style()}>
                        {this.props.job.scenario_name}
                    </td>
                    <td style={this._style()}>
                        {this.props.job.node_name}
                    </td>
                    <td>
                        {this._button()}
                    </td>
                </tr>
            )
        } else {
            return null;
        }
    }

});