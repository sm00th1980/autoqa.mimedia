var CheckResult = React.createClass({

    getInitialState: function () {
        return {
            expected: null,
            got: null
        };
    },

    componentDidMount: function () {
        EventSystem.subscribe('check.selected', this._updateResult);
    },

    _updateResult: function (obj) {
        this.setState({expected: obj.check.expected, got: obj.check.got});
    },

    render: function () {
        return (
            <div className="row">

                <div className="col-md-6">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Expected</h3>
                        </div>
                        <div className="panel-body">
                            <div className="highlight">
                                <pre><code className="xml hljs">{this.state.expected}</code></pre>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Got</h3>
                        </div>
                        <div className="panel-body">
                            <div className="highlight">
                                <pre><code className="xml">{this.state.got}</code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});