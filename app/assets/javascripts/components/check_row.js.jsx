var CheckRow = React.createClass({

    _status: function () {
        switch (this.props.check.status) {
            case 'running':
                return (<span className="label label-primary">RUNNING</span>);
                break;
            case 'passed':
                return (<span className="label label-success">PASSED</span>);
                break;
            case 'failed':
                return (<span className="label label-danger">FAILED</span>);
                break;
            default:
                return (<span className="label label-default">UNKNOWN</span>);
        }
    },

    _style: function () {
        return {verticalAlign: 'middle'};
    },

    _trClass: function () {
        if (this.props.checked) {
            return "success";
        }
        return null;
    },

    render: function () {

        return (
            <tr onClick={this.props.onClick} className={this._trClass()}>
                <td style={this._style()}>
                    {this.props.index + 1}
                </td>
                <td style={this._style()}>
                    {this.props.check.name}
                </td>
                <td style={this._style()}>
                    {this.props.check.command}
                </td>
                <td style={this._style()}>
                    {this._status()}
                </td>
            </tr>
        );
    }

});