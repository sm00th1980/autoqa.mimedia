var CheckTable = React.createClass({

    getInitialState: function () {
        return {
            checks: this.props.checks,
            reset_checks: $.extend(true, [], this.props.checks), //need a clone for resetting checks
            checked_index: null
        };
    },

    componentDidMount: function () {
        EventSystem.subscribe('checking.start', this._startChecking);
        EventSystem.subscribe('file.selected', this._checksReset);
    },

    _checksReset: function () {
        this.setState({checks: this.state.reset_checks}); //reset old results
    },

    _startChecking: function (obj) {
        var self = this;

        $.eventsource({
            label: "event-source-label",
            url: self.props.live_url,
            data: {id: obj.file_id},
            dataType: "json",
            message: function (response) {
                if (response.finished) {
                    Notification.notice('Checking completed.');

                    //closing stream
                    $.eventsource("close", "event-source-label");
                } else {
                    self._updateChecks(response)
                }
            }

        });
    },

    _updateChecks: function (new_check) {
        var checks = $.extend(true, [], this.state.checks);
        var index = _.findIndex(checks, function (check) {
            return check.id === new_check.id;
        });

        checks[index] = new_check;

        this.setState({checks: checks});
    },

    _checkSelected: function (check, index) {
        var self = this;
        return function () {
            self.setState({checked_index: index});
            EventSystem.publish('check.selected', {check: check});
        }
    },

    _newRow: function (data, index) {
        return (
            <CheckRow
                index={index}
                check={this.state.checks[index]}
                onClick={this._checkSelected(this.state.checks[index], index)}
                checked={this.state.checked_index === index}
                />
        )
    },

    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-heading">Steps</div>
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Command</th>
                        <th style={{width: 150 + 'px'}}>Status</th>
                    </tr>
                    </thead>

                    <tbody>
                    {this.state.checks.map(this._newRow, this)}
                    </tbody>
                </table>
            </div>
        );
    }

});