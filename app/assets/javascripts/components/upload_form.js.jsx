var UploadForm = React.createClass({

    getInitialState: function () {
        return {
            version: this.props.version,
            file: null,
            disabled: true, //disable button for start checking
            file_id: null
        };
    },

    _onFileSelect: function (e) {
        var file = e.target.value.replace(/\\/g, '/').replace(/.*\//, '');
        this.setState({file: file, disabled: false});

        EventSystem.publish('file.selected');
    },

    _onVersionChange: function (e) {
        this.setState({version: e.target.value});
    },

    _version: function () {
        return (
            <input type="text" className="form-control"
                   placeholder="Version" onChange={this._onVersionChange} value={this.state.version}/>
        )
    },

    _submitButton: function () {
        if (this.state.disabled) {
            return (
                <button type="submit" className="btn btn-primary" disabled="disabled">Start check</button>
            )
        } else {
            return (
                <button type="submit" className="btn btn-primary">Start check</button>
            )
        }
    },

    handleSubmit: function (event) {
        event.preventDefault();

        var file = $('input[id="file"]')[0].files[0];
        var self = this;

        var data = new FormData();
        data.append('build', file);
        data.append('version', self.state.version);

        var notify = Notification.upload('Uploading started.', function () {
            Notification.notice('Start checking...');
            EventSystem.publish('checking.start', {file_id: self.state.file_id});
        });

        $.ajax({
            url: this.props.upload_url,
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            xhr: function () {
                jqxhr = $.ajaxSettings.xhr();
                jqxhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        notify.update({'progress': percentComplete * 100.0});
                    }
                }, false);

                return jqxhr;
            },
            success: function (response) {
                if (response.success) {
                    self.setState({file_id: response.file_id});

                    notify.update({'message': 'Uploading completed.'});
                    notify.close();
                } else {
                    if ('message' in response) {
                        Notification.error(response.message);
                    } else {
                        Notification.error('Error upload file to backend. Possible backend failure.');
                    }
                }
            },
            error: function () {
                Notification.error('Error upload file to backend. Possible network failure.');
            }
        });
    },

    render: function () {
        return (
            <form className="form-inline" method="post" encType="multipart/form-data" onSubmit={this.handleSubmit}>

                <div className="input-group" style={{width: 25 + '%'}}>
                    <div>
                        <input type="text" className="form-control"
                               placeholder="MiMedia-iOS-AppStore-1.0.27-1-2015-10-22.zip"
                               value={this.state.file}
                               style={{width: 90 + '%'}}
                               readOnly/>

                    <span className="input-group-btn">
                        <span className="btn btn-default btn-file">
                            <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                            <input type="file" accept=".zip" id="file" onChange={this._onFileSelect}/>
                        </span>
                    </span>
                    </div>
                </div>

                <div className="form-group" style={{marginRight: 8 + 'px'}}>
                    {this._version()}
                </div>

                {this._submitButton()}

            </form>
        )
    }

});